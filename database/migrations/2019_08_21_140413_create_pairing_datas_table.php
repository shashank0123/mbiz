<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairingDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairing_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->string('member_id');
            $table->string('leftcount')->default('0');
            $table->string('rightcount')->default('0');
            $table->string('leftdirectcount')->default('0');
            $table->string('rightdirectcount')->default('0');
            $table->string('leftactivecount')->default('0');
            $table->string('rightactivecount')->default('0');
            $table->string('leftwashoutcount')->default('0');
            $table->string('rightwashountcount')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairing_datas');
    }
}
