<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\MemberDetail;
use App\Order;
use StdClass;
use Mail;


class PurchaseHistoryController extends Controller
{
	public function getAll()
	{

		$user = \Sentinel::getUser();
        $member = $user->member;

        $user_order = Order::where('user_id',$member->id)->where('status','SUCCESS')->orWhere('status','RETURN')->orWhere('status','CANCEL')->orWhere('status','delivered')->orderBy('created_at','DESC')->get();
        
        
		return view('front.history.purchase-history',compact('user_order'));
	}

	public function actionOrder(Request $request)
	{
		$order = new Order;
		$response = new StdClass;
		$response->msg = "success";
		$response->status = '200';
		$user_id = $request->userId;
		$order_id = $request->orderId;
		$txn_id = $request->txnId;
		$action = $request->orderAction;

		$user = Member::where('id',$user_id)->first();
		$member = MemberDetail::where('member_id',$user_id)->first();
		$order = Order::where('transaction_id',$txn_id)->where('user_id',$user_id)->first();



		$username = $user->username;
		$phone = $member->member_phone;
		$payment_method = $order->payment_method;
		$transaction_id = $order->transaction_id;

		if($action == 'return'){
			$order->status = 'RETURN';
			
		}
		else{
			$order->status = 'CANCEL';
		}	
		$order->update();

		$data = array('Name' => $user->username,'Contact' => $member->member_phone, 'TransactionID' => $order->transaction_id, 'Payment Method' => $order->payment_method, 'Action' => $action);
		Mail::send('front.history.message', $data, function($message) use ( $username,$phone,$payment_method,$transaction_id,$action)
		{   
			$message->from('no-reply@m-biz.in', 'Mailer');
			$message->to('Admin@m-biz.in', 'Admin')->subject('About An Order');
		});	

		$response->msg ="Your request to ".$action." the order is sent" ;
		return response()->json($response);
	}

	public function getSearchedOrders(Request $request){

		$type = $request->product_keyword;
       $type;
       if($type=='cancel'){
       	$type='CANCEL';
       }if($type=='return'){
       	$type='RETURN';
       }if($type=='success'){
       	$type='Success';
       }if($type=='delivered'){
       	$type='delivered';
       }
       $user = \Sentinel::getUser();
        $member = $user->member;

      
$user_order = Order::where('user_id',$member->id)->where('transaction_id','LIKE','%'.$type.'%')->orWhere('status','LIKE','%'.$type.'%')->orWhere('payment_method','LIKE','%'.$type.'%')->get();


        return view('front.history.searched_orders',compact('user_order'));
	}


    	// return response()->json($response);
    	// return view('front.history.action',compact());



}
