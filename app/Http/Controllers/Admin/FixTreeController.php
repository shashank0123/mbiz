<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Newsletter;
use App\Models\Member;
use App\Models\BonusPairing;

ini_set('max_execution_time', 300);
class FixTreeController extends Controller
{

	// ***************************For Fixing Tree****************************
	public function fixtreeall(Request $request)
	{
		echo "<pre>";
		$memberlists = Member::select('id', 'position', 'parent_id', 'username', 'package_id')->orderBy('id', 'DESC')->get();
		$members = array();
		foreach ($memberlists as $key => $value) {
			$members[$value->id] = $value;
		}
		foreach ($members as $key => $value) {
			$name = $value->position;
			if (isset($members[$value->parent_id]))
			$members[$value->parent_id]->$name = $value;
		}

		foreach ($members as $key => $value) {
			$left = array();
			$right = array();
			if (isset($value->left )){
				if ($value->left->left){
						array_push($left, ($value->left->left));
						array_push($left, ($value->left->right));
				}
				array_push($left, $value->left->id);
				$left = array_filter($left);
				$right = array_filter($right);
				$left = implode(',', $left);
				$value->left = $left;
			}

			if (isset($value->right )){
				if ($value->right->right){
					array_push($right, $value->right->left);
					array_push($right, $value->right->right);
				}
				array_push($right, $value->right->id);
				$right = implode(',', $right);
				$value->right = $right;
			}
			$count = 0;
			$member = Member::where('id', $value->id)->first();
			$member->left_children = implode(',', array_filter(explode(',', $value->left)));
			$member->right_children = implode(',', array_filter(explode(',', $value->right)));
			$member->update();
			echo $count.'<br>' ;
		}
	}


	public function fixtreeallV2(Request $request)
	{
		$memberlists = Member::select('id', 'position', 'parent_id', 'username', 'package_id')->orderBy('id', 'DESC')->get();
		$members = array();
		foreach ($memberlists as $key => $value) {
			$members[$value->id] = $value;
		}
		foreach ($members as $key => $value) {

			$members[$value->parent_id]->$value->position = $value;
		}

		foreach ($members as $key => $value) {
			$left = array();
			$right = array();
			if (isset($value->left )){
				if ($value->left->left){
						array_push($left, $value->left->left);
						array_push($left, $value->left->right);
				}
				array_push($left, $value->left->id);
				$left = implode(',', $left);
				$value->left = $left;
			}

			if (isset($value->right )){
				if ($value->right->right){
					array_push($right, $value->right->left);
					array_push($right, $value->right->right);
				}
				array_push($right, $value->right->id);
				$right = implode(',', $right);
				$value->right = $right;
			}
				}
			echo '<br><br><br>';
			$members = array_filter($members);
			foreach ($members as $key => $value) {
				if (isset($value->id))
				if (isset($value->left)){
					$leftarray = explode(',', $value->left);
					$count = 0;
					foreach ($leftarray as $key => $value) {
						if ($members[$value]->package_id == 2){
							$count = $count+1;
						}
					}
					$value->left  = $count;
				}
				if (isset($value->id))
				if (isset($value->right)){
					$rightarray = explode(',', $value->right);
					$count = 0;
					foreach ($rightarray as $key => $value) {
						if ($members[$value]->package_id == 2){
							$count = $count+1;
						}
					}
					$value->right  = $count;
				}
				foreach ($members as $key => $value) {
					if (isset($value->id))
					echo $value->id." ".$value->left." ".$value->right.'<br>';
				}
			}
		}
		




	public function calcallbinary(Request $request)
	{
		$memberlists = Member::select('id', 'position', 'parent_id', 'username', 'package_id', 'left_children', 'right_children')->orderBy('id', 'DESC')->get();
		$members = array();
		foreach ($memberlists as $key => $value) {
			$members[$value->id] = $value;
		}
			$count = 1;
		foreach ($members as $key => $value) {
			// var_dump($value);
			if ($value->package_id > 1){
				echo $value->left_children.'<br>';
				$leftarray = explode(',', $value->left_children);
				$leftcount = 0;
				foreach ($leftarray as $key1 => $value1) {
					if (trim($value1)!=null && $members[trim($value1)]->package_id > 1){
						$leftcount = $leftcount + 1;
					}
				}
				$update = Member::where('id', $value->id)->first();
				$update->left_total = $leftcount*100;
				$rightarray = explode(',', $value->right_children);
				$rightcount = 0;
				foreach ($rightarray as $key1 => $value1) {
					if (trim($value1)!=null &&  $members[trim($value1)]->package_id > 1){
						$rightcount = $rightcount + 1;
					}
				}
				$update->right_total = $rightcount*100;
				$update->update();
				echo $count++;
			}
		}

	}


	public function calcpairing(Request $request)
	{
		$members = Member::where('package_id', '>', '1')->get();
		foreach ($members as $key => $value) {
			$checkactive = Member::where('parent_id', $value->id)->where('package_id', '>', 1)->get();
			if (count($checkactive) == 2){
				if ($value->left_total > $value->right_total || $value->left_total < $value->right_total){
					$pairincome = min($value->left_total, $value->right_total);
					$pairincomesave = $pairincome;

					$maxpair = min(1000, $pairincome);
					$pairincome  = new BonusPairing;
					$pairincome->member_id = $value->id;
					$pairincome->username = $value->username;
					$pairincome->amount_cash = $maxpair;
					$pairincome->amount_promotion = 0;
					$pairincome->total = $maxpair;
					$pairincome->save();

					if ($pairincome){
						$updatevalue = Member::where('id', $value->id)->first();
						$updatevalue->left_total = $value->left_total-$pairincomesave;
						$updatevalue->right_total = $value->right_total-$pairincomesave;
						$updatevalue->update();
					}
				}
			}
		}
	}



	public function fixleftright(Request $request)
	{
		$members = Member::all();
		foreach ($members as $key => $value) {
			$onemember = Member::where('id', $value->id)->first();
			$left = $onemember->left_children;
			$left = explode(',', $left);
			$left = array_values($left);
			$left = implode(',', $left);
			$right = $onemember->right_children;
			$right = explode(',', $right);
			$right = array_values($right);
			$right = implode(',', $right);
			$onemember->left_children = $left;
			$onemember->right_children = $right;
			$onemember->update();
			// dd($left);
			// die();
			// echo $left;
		}
	}

	// To view all Newsletters Email
    
}
