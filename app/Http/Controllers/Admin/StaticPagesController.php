<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faqs;
use App\StaticPage;

class StaticPagesController extends Controller
{
    public function viewFAQList(){
    	$faq = Faqs::orderBy('created_at','DESC')->get();
    	return view('back.ecommerce.faq',compact('faq'));
    }

    public function createFAQ()
    {
        return view('back.ecommerce.add-faq');
    }

    public function storeFAQ(Request $request){
    	$faq = new Faqs;
    	$faq->questions = $request->question;
    	$faq->answers = $request->answer;
    	$faq->status = $request->status;
    	$faq->save();
    	return redirect()->back()->with('message','Data Inserted Successfully');
    }

    public function editFAQ($id)
    {
        $faq = Faqs::find($id);
        return view('back.ecommerce.edit-faq',compact('faq'));
    }

    public function updateFAQ(Request $request, $id)
    {
        $faq = Faqs::find($id);        
        $faq->questions = $request->question;
        $faq->answers = $request->answer;
        $faq->status = $request->status;
        $faq->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }

    public function destroyFAQ($id)
    {
        $faq = Faqs::find($id);
        $faq->delete();
        return redirect()->back()->with('message','Data Successfully Deleted');
    }



    public function viewPageList(){
        $pages = StaticPage::all();
        return view('back.ecommerce.static-pages',compact('pages'));
    }

    public function editPage($id)
    {
        $page = StaticPage::find($id);
        return view('back.ecommerce.edit-static-pages',compact('page'));
    }

    public function updatePage(Request $request, $id)
    {
        $page = StaticPage::find($id); 
        $page->page_heading = $request->page_heading;
        $page->description = $request->description;
        $page->update();
        return redirect()->back()->with('message','Data Successfully Updated');
    }
}
