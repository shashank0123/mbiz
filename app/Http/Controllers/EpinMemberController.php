<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EpinMemberController extends Controller
{
    public function showaddepinpage()
	{
		 return view('front.epin.addEpin');
	}

	public function createEpin(Request $request)
	{
		$data = $request->data;
		$member_id = $data['member_id'];
		$package_id = $data['package_id'];
		$no_of_epins = $data['no_of_epins'];
		$createepins = new TransferEpin;
		$createepins->sender_id = 'admin';
		$createepins->receiver_id = $member_id;
		$createepins->no_of_epins = $no_of_epins;
		$createepins->save();

		for ($i=1; $i < $no_of_epins; $i++) { 
			$generateepins = new Epin;
			$generateepins->member_id = $member_id;
			$generateepins->epin = time().$member_id.$i;
			$generateepins->package_id = $package_id;
			$generateepins->save();
		}
		return \Response::json([
            'type'  => 'success',
            'message' => \Lang::get('message.epincreation'),
        ]);
		//epin created by admin will also be saved in the transfer epin table.
		var_dump($member_id);
		die();
	}
}
