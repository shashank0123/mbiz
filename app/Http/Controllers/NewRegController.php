<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\Leads;
use App\RegistrationSponsorId;
use App\MainCategory;
use Mail;
use StdClass;
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

class NewRegController extends BaseController
{
    public function Register(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';
        $user = new Leads;
        $checkemail = Leads::where('email',$request->email)->first();
        
        
        

        if($checkemail)
        { 
            if($checkemail->verify_otp == 'verified'){
                $response->message = "Already Registered"; 
            }        
            else{
                $mobile = $checkemail->phone;
                $message = "Your%20OTP%20for%20MBiz%20is%20$checkemail->otp";
                $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
           
                $c = curl_init();
                curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($c,CURLOPT_HTTPGET ,1);

                curl_setopt($c, CURLOPT_URL, $url);
                $contents = curl_exec($c);
                if (curl_errno($c)) { 
                    echo 'Curl error: ' . curl_error($c);
                }
                else{
                    curl_close($c);
                }
                if($user){
                    $response->message ="Data Inserted Successfully";
                    return response()->json($response);
                }

                else{
                    echo " Something Went Wrong";
                }
            }  
            
            return response()->json($response);
            exit();             
        }

        else
        {
            if($request->username == null  ||  $request->email == null  ||  $request->mobile == null  ||  $request->adhaar == null)
            {
                $response->message = 'Please fill All the Fields';
                $response->username = $request->username;
                $response->email = $request->email;
                $response->mobile = $request->mobile;
                $response->adhaar = $request->adhaar;
                return response()->json($response);
                exit();
            }

            else{

                $user->username = $request->username;
                $user->phone    = $request->mobile;
                $user->email    = $request->email;
                $user->adhaar   = $request->adhaar;
                $user->otp      = rand(100000, 999999);
                $user->otp_time = date('Y-m-d H:m:s') ;

                $user->save();
            }

            $mobile = $user->phone;
            $message = "Your%20OTP%20for%20MBiz%20is%20$user->otp";
            $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
            // die;
            // $url ="http://103.247.98.91/API/SendMsg.aspx?uname=backstage023&pass=backstage023&send=MBIZTL&dest=$mobile&msg=$message";  
            // $url = "";
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c,CURLOPT_HTTPGET ,1);

            curl_setopt($c, CURLOPT_URL, $url);
            $contents = curl_exec($c);
            if (curl_errno($c)) {
                echo 'Curl error: ' . curl_error($c);
            }
            else{
                curl_close($c);
            }
            if($user){
                $response->message ="Data Inserted Successfully";
                return response()->json($response);
            }

            else{
                echo " Something Went Wrong";
            }
        }        
    }

    public function verifyotp(Request $request)
    {
        $sponsored = new RegistrationSponsorId;
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';
        
        $findUser = Leads::where('email',$request->email)->first();
        $sponsoredId = $request->sponsoredId;
        $sponsoredId = strtoupper($sponsoredId);

        if($findUser->verify_otp != 'verified')
        {
            if($findUser->otp == $request->otp) {                
                $response->message = 'Registered Successfully. Please Wait we will call you shortly.';
                $response->user_id = $findUser->id;
                $findUser->verify_otp ='verified';
                $findUser->update();
                $sponsored->user_id = $findUser->id;
                $sponsored->sponsor_id = $sponsoredId;
                $sponsored->save();

                return response()->json($response);

            }
            else {                

                $response->message = "Wrong OTP";
                return response()->json($response);
            }
        }
        else
        {
            $response->message = "Already Verified";
            return response()->json($response);
        }        
    }

    public function checkoutForm(Request $request,$id)
    {      
        $lead = Leads::where('id',$id)->first();
        return view('membership-payment',compact('lead'));
    }

    public function checkoutForm1(Request $request,$id){
        $success = "";
        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
            if(strcasecmp($contentType, 'application/json') == 0){
                $data = json_decode(file_get_contents('php://input'));
                $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
                $json=array();          
                $json['success'] = $hash;
                echo json_encode($json);        
            }
            exit(0);
        }

    }

    public function getSuccess(Request $request){

        $lead = new Leads;

        $post_data = $_POST;  


        $user_email = $post_data['email'];
        $status = $post_data['txnStatus'];
        $txnid = $post_data['txnid'];
        // echo $lead = Leads::where('email',$user_email)->get();
        // die;
        

        if($status == 'SUCCESS'){
            
          $lead = Leads::where('email',$user_email)->get();
            $lead->payment_status = 'success';
            // echo $lead; 
            // die;
            // $lead->update();
            Leads::where('email',$user_email)->update([
            'payment_status' => $lead->payment_status
            ]);
        }

        

        $data = array('Email'=>$user_email, 'TransactionID'=>$txnid, 'Payment_Status'=>$status);
        Mail::send('mail', $data, function($message) use ($user_email, $txnid, $status)
        {   
            $message->from('jyotikasethi3007@gmail.com', 'Jyotika');
            $message->to('jyotika@backstagesupporters.com', 'Admin')->subject('Successfull Registration');
        });



        return view('register');
    }


}
