<?php

namespace App\Http\Controllers;
use App\MainCategory;
use App\Product;
use App\Blog;
use App\Review;
use App\Testimonial;
use App\Wishlist;
use App\Models\Member;
use App\Models\Epoint;
use App\Cart;
use App\City;
use App\Banner;
use App\State;
use App\Country;
use App\Address;
use App\Order;
use App\OrderItem;
use App\User;
use App\Contact;
use StdClass;
use Mail;
use Hash;

use Illuminate\Http\Request;

class EcommerceMbizController extends Controller
{
	public function index()
	{
		$cartproducts = array();
		$prod = new Product;
		$products = array();
		$quantity = array();
		$pro_cnt = 0;
		$i = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		
		// die;
		$banners = Banner::where('image_type','banner')->where('status','active')->get(); 
		// echo $banners;
		// exit();
		$category = MainCategory::all();
		foreach($category as $cat){
			$prod = Product::where('category_id',$cat->id)->where('status','Active')->orderBy('updated_at','desc')->limit(1)->get();
			// echo $prod;
			if($prod != null){
				foreach($prod as $pro)
					$products[$pro_cnt++] = $pro->id;
			}
			
		}

		$blogs = Blog::where('status','active')->get();    	
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
		}		

		return view('ecommerce2.index',compact('maincategory','products','blogs','cartproducts','quantity','banners','pro_cnt'));
	}
	

    // For Search Page
	public function getSearchProduct(Request $request)
	{
		$subcat = $request->subcategory;
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");
		$color = array();

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		
		$total = array();
		$i=0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();        	
			$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->where('status','Active')->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();       			
						}
					}
				}
				if($count==0)
					$total[$i++] = 0;
				else					
					$total[$i++] = $count;   
			}
		}

		$cnt_pro = 0;

		if($subcat!="" and $keyword!="" )
		{
			$sub = MainCategory::where('slug',$subcat)->first();
			$searched_products = Product::where('category_id',$sub->id)->where('status','Active')->where('name','LIKE','%'.$keyword.'%')->orderby('name','ASC')->get();
			foreach($searched_products as $pro){
				$color[$cnt_pro++] = $pro->product_color;
			}
		}
		else if($subcat!="" && $keyword=="")
		{
			$sub = MainCategory::where('slug',$subcat)->first();
			$searched_products = Product::where('category_id',$sub->id)->where('status','Active')->orderby('name','ASC')->get();   
			foreach($searched_products as $pro){
				$color[$cnt_pro++] = $pro->product_color;
			}       
		}

		else if($keyword != "" && $subcat=="")
		{
			$searched_products = Product::where('name','LIKE','%'.$keyword.'%')->where('status','Active')->orderby('name','ASC')->get();     
			foreach($searched_products as $pro){
				$color[$cnt_pro++] = $pro->product_color;
			}     
		}
		else if($subcat==0)
		{
			$searched_products = Product::where('status','Active')->orderby('name','ASC')->get();           
			foreach($searched_products as $pro){
				$color[$cnt_pro++] = $pro->product_color;
			}
		}       

		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		$color = array_filter($color);
		$color = array_unique($color);

		// for($var=0 ; $var<$i ; $var++){
		// 	echo $total[$var]."  ";
		// }
		// die;

		return view('ecommerce2.search-product',compact('maincategory','searched_products','total','lastproduct','max_price','cartproducts','quantity','color','keyword','subcat'));
	}

    // For Product Detail
	public function getProductDetail($id)
	{
		$bill = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$getproduct = Product::where('slug',$id)->where('status','Active')->first();

		$products = Product::where('category_id',$getproduct->category_id)->where('status','Active')->get();

		$product_category = MainCategory::where('id',$getproduct->category_id)->where('status','Active')->first();
		$count_review = Review::where('product_id',$getproduct->id)->count();
		$reviews = Review::where('product_id',$id)->orderBy('created_at','desc')->get();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $pro){
				$product = Product::where('id',$pro->product_id)->first();
				$price = $product->sell_price;
				$bill = $bill + $pro->quantity*$price;
			}
		}		
		
		return view('ecommerce2.product-detail',compact('maincategory','getproduct','product_category','count_review','reviews','products','bill','id'));
	}

	public function submitReview($id,Request $request)
	{
		$review = new Review;
		$review->name = $request->name;
		$review->email = $request->email;
		$review->rating = $request->rating;
		$review->review = $request->review;
		$review->product_id = $id;        
		$review->save();

		return redirect()->back()->with('message','Thank You For Your Review');
	}

	public function aboutUs()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::where('status','Active')->get();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				echo $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				echo $quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		exit();
		
		return view('ecommerce2.about-us',compact('maincategory','testimonials','cartproducts','quantity'));
	}	

	public function getWishList()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('ecommerce2.wishlist',compact('maincategory','cartproducts','quantity'));
	}	

	public function getCheckout($id)
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$countries = Country::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}
		$address = Address::where('user_id',$id)->first();

		
		return view('ecommerce2.checkout',compact('maincategory','countries','cartproducts','quantity','address'));
	}
	 //Dependent DropDown Of Countries , States & Cities
	public function getStateList(Request $request)
	{
		$request->country_id;
		$states = State::where("country_id",$request->country_id)->get();
		return response()->json($states);
	}

	public function getCityList(Request $request)
	{
		$request->state_id;
		$cities = City::where("state_id",$request->state_id)->get();
		echo $cities;
		die;
		return response()->json($cities);
	}


	// To return cart view
	public function getCart(Request $request)
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();	
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;

		$allproducts = array();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('ecommerce2.cart',compact('maincategory','cartproducts','quantity'));
	}

	//Category Wise Products Selection
	public function getCategoryProducts($id,Request $request){
		$checkId = MainCategory::where('slug',$id)->first();
		$subcat = $checkId->id;
		
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");

		$cartproducts = array();
		$color = array();
		$size = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		
		
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();        	
			$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->where('status','Active')->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();       			
						}
					}
				}

			}
		}

		$searched_products = array();
		$cat_ids = array();

		$total = array();
		$i=0;
		$sec = MainCategory::where('slug',$id)->where('status','Active')->first();
		$cat_ids[0] = MainCategory::where('id',$sec->id)->where('status','Active')->first();

		$cid = 1;
		$cnt_pro = 0;
		$count_catpro = 0;
		$idss = MainCategory::where('category_id',$sec->id)->where('status','Active')->get();
		if($idss != null){
			foreach($idss as $ids){
				$count_catpro = 0;
				$cat_ids[$cid++] = $ids;
				$idsss = MainCategory::where('category_id',$ids->id)->where('status','Active')->get();
				$count_catpro+=Product::where('category_id',$ids->id)->where('status','Active')->count(); 
				if($idsss != null){
					foreach($idsss as $ds){
						$cat_ids[$cid++] = $ds;		
						$count_catpro+=Product::where('category_id',$ds->id)->where('status','Active')->count();				
					}
					if($count==0)
						$total[$i++] = 0;
					else					
						$total[$i++] = $count_catpro;
				}
			}
		}
		
		for($j=0 ; $j<$cid ; $j++){
			$products = Product::where('category_id',$cat_ids[$j]->id)->where('status','Active')->get();
			if($products != null){
				foreach($products as $pro){
					$searched_products[$cnt_pro] = $pro;
					$color[$cnt_pro] = $pro->product_color;
					$cnt_pro++;
				}
			}			
		}
		
		$color = array_filter($color);
		$color = array_unique($color);
		shuffle($searched_products);

		return view('ecommerce2.categorywise-products',compact('maincategory','searched_products','total','lastproduct','max_price','cartproducts','quantity','id','color'));
	}

	// For Ajax Call (Adding product to cart)
	public function addToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = 1;
			$cart[$product_id] = $item;
			$count++;
		}

		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);		

		if(!$cart)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved']); 
	}

    // For Ajax Call (Adding product to cart)
	public function deleteSessionData(Request $request,$id)
	{	
		$flag = 1 ;
		$count = session()->get('count');
		$count--;
		$bill = 0;
		$showcount = $count;
		$cart = session()->get('cart');
		$cart[$request->id] = null;
		$cart = array_filter($cart);
		$request->session()->put('cart',$cart);
		$request->session()->put('count',$showcount);
		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$price = $product->sell_price;
			$bill = $bill + $pro->quantity*$price;
		}
		if($flag == 1)		
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully', 'showcount' => $showcount,'bill' => $bill]);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	public function increment(Request $request,$id,$quant)
	{
		$cart = session()->get('cart');
		$ids = $request->id ;		
		if (array_key_exists($ids,$cart)) {
			$cart[$ids]->quantity = $cart[$ids]->quantity + 1;
			echo "data updated ".$cart[$ids]->quantity;
		}                    
		else {
			echo "New inserted quantity"; 
			$cart[$ids]->quantity = 1;            
		}  
		$request->session()->put('cart', $cart);
		return response()->json(['status' => 200, 'message' => 'Data Successfully Updated']);
	}	

	public function decrement(Request $request,$id,$quant)
	{
		$cart = session()->get('cart');
		$ids = $request->id ;		
		if ($cart[$ids]->quantity >1) {
			$cart[$ids]->quantity = $cart[$ids]->quantity - 1;
			echo "data updated";
		}                    
		else {
			echo "No quantity in Cart"; 
			$cart[$ids]->quantity = 1;            
		}  
		$request->session()->put('cart', $cart);
		return response()->json(['status' => 200, 'message' => 'Data Successfully Updated']);
	}

	public function addToWishList(Request $request)
	{
		$wishlistItem = new Wishlist;
		$wishlist = new Cart;
		$product_id = $request->id;
		$wishlistItem->product_id = $request->id;
		$wishlistItem->user_id = $request->user_id;
		if (session()->get('wishlist') != null ){
			$wishlist = session()->get('wishlist');					
		}
		else {
			$wishlist = array();			
		}
		if (isset($wishlist[$product_id]->quantity)){
			$wishlist[$product_id]->quantity = $wishlist[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = 1;
			$wishlist[$product_id] = $item;		
		}

		$request->session()->put('wishlist',$wishlist);		
		$wishlistItem->save();
		if(!$wishlist)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved']); 
	}

	
	public function deleteWishList(Request $request,$id)
	{			
		$user_id = $request->user_id;
		$product_id = $request->id;
		$wishlistItem = Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
		$wishlistItem->delete();

		$flag = 1;
		$wishlist = session()->get('wishlist');
		$wishlist[$product_id] = null;
		$wishlist = array_filter($wishlist);
		$request->session()->put('wishlist',$wishlist);
		
		if($flag == 1)		
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	//Delete All Items From WishList
	public function clearWishList(Request $request)
	{	
		$wish = new Wishlist;		
		$wish = Wishlist::where('user_id',$request->user_id)->get();
		
		foreach($wish as $del){
			$del->delete();
		}
		
		$wishlist = session()->forget('wishlist');

		if($wishlist == null)
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	//Search All
	public function searchAll(){
		$product = Product::where('status','Active')->get();
		return response()->json(['status' => 200, 'message' => 'Data Retrieved']);
	}

	// Add All To Cart
	public function addAllToCart(Request $request)
	{
		$wishlist = new Cart;
		if(session()->get('wishlist') != null){
			$wishlist = session()->get('wishlist');				

			foreach($wishlist as $list){
				$cart = new Cart;
				$product_id = $list->product_id;
				if (session()->get('cart') != null){
					$cart = session()->get('cart');
				}
				else 
					$cart = array();
				if (isset($cart[$product_id]->quantity)){
					$cart[$product_id]->quantity = $list->quantity ;
				}
				else{
					$item = new StdClass;
					$item->product_id = $product_id;
					$item->quantity =$list[$product_id]->quantity;
					$cart[$product_id] = $item;
				}
				$request->session()->put('cart',$cart);
				if(!$cart)
				{
					$message = "Something Went Worng";        
				}
				else{
					$message = "Data Added To Cart";
				}
			}
		}
		session()->forget('wishlist');
		return response()->json(['status' => 200, 'message' => $message]);
	}

	public function productsCat(Request $request){
		$searched_products = array();
		$count = 0 ;
		$id = array();
		$i=1;
		
		$checkId = MainCategory::where('id',$request->cat_id)->first();
		$send_id = $checkId->id;
		$id[0] = $checkId->id;
		if($id[0] == 0)
		{
			$products = Product::where('status','Active')->get();
			foreach($products as $pro){
				$searched_products[$count++] = $pro;
			}
		}
		else{
			$cat_id = MainCategory::where('category_id',$id[0])->where('status','Active')->get();
			if($cat_id != null){
				foreach($cat_id as $cat){
					$id[$i] = $cat->id;
					$sub_id =MainCategory::where('category_id',$id[$i])->where('status','Active')->get();
					$i++;
					if($sub_id != null){
						foreach($sub_id as $sub){
							$id[$i] = $sub->id;
							$i++;
						}
					}
				}
			}
			for($j=0 ; $j<$i ; $j++){
				$products= Product::where('category_id',$id[$j])->where('status','Active')->get();
				foreach($products as $pro){
					$searched_products[$count++] = $pro;
				}
			}	
		}		
		shuffle($searched_products);
		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}

	//Filter products on the basis of there prices
	public function priceFilter(Request $request){
		echo $max = $request->max_cost;
		echo $min = $request->min_cost;
		echo $color = $request->color;
		echo $size = $request->size;
		$checkId = MainCategory::where('slug',$request->cat_id)->first();
		echo $send_id = $checkId->id;

		$category_ids = array();
		$category_ids[0] = $send_id;
		$j = 1;

		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		for($f=0;$f<$j;$f++){
			echo $category_ids[$f]."  /  ";
		}

		// $searched_products = Product::where('product_size','LIKE',$size.'%,')->orWhere('product_size','LIKE','%,'.$size)->orWhere('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size)->where('status','Active')->get();

		// echo $searched_products;
		// die;

		if($size=="" && $color==""){
			echo "Size & color Empty";
			for($k=0 ; $k<$j ; $k++){
				// echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($size==""){
			echo "Size Empty";
			for($k=0 ; $k<$j ; $k++){

				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('status','Active')->get();

				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else if($color==""){
			echo "Color Empty";
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."    /     ";
				$products=Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->where('status','Active')->get();
				// $products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE',$size.'%,')->orWhere('product_size','LIKE','%,'.$size)->orWhere('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size)->where('status','Active')->get();
				echo $products;
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else{
			echo "Nothing Empty";
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products=Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->where('product_color',$color)->where('status','Active')->get();
				// $products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.'%,')->orWhere('product_size','LIKE','%,'.$size)->orWhere('product_size','LIKE',$size)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}	
		// $var =1;
		// foreach($searched_products as $pro ){
		// 	echo $var++."    ".$pro."<br><br>";
		// }	
		// die;

		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}


	//Filter products on the basis of there prices
	public function priceFilterSearch(Request $request){
		echo $max = $request->max_cost;
		echo $min = $request->min_cost;
		echo $color = $request->color;
		echo $keyword = $request->keyword;
		echo $size = $request->size;
		
		echo $send_id = $request->cat_id;
		

		$category_ids = array();
		$category_ids[0] = $send_id;
		$j = 1;

		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		for($f=0;$f<$j;$f++){
			echo $category_ids[$f]."  /  ";
		}


		if($send_id != ""){
			if($size=="" && $color==""){
				echo "Size & color Empty";
				for($k=0 ; $k<$j ; $k++){
				// echo $category_ids[$k]."   ";
					$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}
			else if($size==""){
				echo "Size Empty";
				for($k=0 ; $k<$j ; $k++){

					$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('status','Active')->get();

					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}
			else if($color==""){
				echo "Color Empty";
				for($k=0 ; $k<$j ; $k++){
					echo $category_ids[$k]."    /     ";
					$products=Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->where('status','Active')->get();

					echo $products;
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}
			else{
				echo "Nothing Empty";
				for($k=0 ; $k<$j ; $k++){
					echo $category_ids[$k]."   ";
					$products=Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->where('product_color',$color)->where('status','Active')->get();

					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}
		}

		else{
			if($size=="" && $color==""){
				$searched_products = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->orderby('name','ASC')->get();
			}
			else if($size==""){
				$searched_products = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('product_color',$color)->orderby('name','ASC')->get();
			}
			else if($color == ""){
				$searched_products = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('product_size','LIKE','%'.$size.'%')->orderby('name','ASC')->get();
			}
			else {
				$searched_products = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%'.$size.'%')->orderby('name','ASC')->get();
			}
		}	

		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}


	//For Category Wise Product Filter

	public function categoryWiseProduct(Request $request){
		$searched_products = array();
		$count = 0 ;
		$id = array();
		$i=1;
		$checkId = MainCategory::where('slug',$request->cat_id)->first();
		echo $send_id = $checkId->id;
		
		$id[0] = $send_id;
		if($id[0] == 0)
		{
			$products = Product::where('status','Active')->get();

			foreach($products as $pro){
				$searched_products[$count++] = $pro;
			}

		}
		else{
			$cat_id = MainCategory::where('category_id',$id[0])->where('status','Active')->get();
			if($cat_id != null){
				foreach($cat_id as $cat){
					$id[$i] = $cat->id;
					$sub_id =MainCategory::where('category_id',$id[$i])->where('status','Active')->get();
					$i++;
					if($sub_id != null){
						foreach($sub_id as $sub){
							$id[$i] = $sub->id;
							$i++;
						}
					}
				}
			}

			for($j=0 ; $j<$i ; $j++){
				$products= Product::where('category_id',$id[$j])->where('status','Active')->get();
				foreach($products as $pro){
					$searched_products[$count++] = $pro;
				}
			}	
		}
		shuffle($searched_products);

		// return response()->json($searched_products);
		return view('ecommerce2.cat-wise-product',compact('searched_products','send_id'));

	}

	public function checkoutForm(Request $request,$id){
		$cart = new Cart;
		$order = new Order;
		$address = new Address; 
		$check = new Address; 

		$checkSessionID = Cart::where('session_id',session()->getId())->first();
		
		if($checkSessionID != null){

		}
		else{
			$cart->session_id = session()->getId();
			$cart->user_id = $request->member_id;
			$cart->cart = json_encode(session()->get('cart'));
			$cart->date = date('Y-m-d H:m:s');
			$cart->save();
		}		

		$check = Address::where('user_id',$request->member_id)->first();
		
		if($check != null){
			
			$check->address = $request->address;
			$check->country_id = $request->country_id;
			$check->state_id = $request->state_id;
			$check->city_id = $request->city_id;
			$check->pin_code = $request->pin_code;
			$check->phone = $request->phone;
			$check->update();
		}
		else{
			$address->user_id = $request->member_id;
			$address->address = $request->address;
			$address->country_id = $request->country_id;
			$address->state_id = $request->state_id;
			$address->city_id = $request->city_id;
			$address->pin_code = $request->pin_code;
			$address->phone = $request->phone;
			$address->save();
		}		

		$address_id = Address::where('user_id',$request->member_id)->first();
		$order->user_id = $request->member_id;
		$order->address_id =  $address_id->id;
		$order->price =  $request->total_bill;
		$order->payment_method =  $request->payment_method;
		$order->date = date('Y-m-d H:m:s');
		$order->save();

		$order_id = Order::where('user_id',$request->member_id)->first();
		$i=0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$order_item = new OrderItem;
				$order_item->order_id = $order_id->id;
				$order_item->item_id = $cart->product_id;
				$price = Product::where('id',$cart->product_id)->first();
				$order_item->quantity	= $cart->quantity;
				$order_item->price = $cart->quantity * $price->sell_price;
				$order_item->save();
			}	
		}

		$checkorder = Order::where('user_id',$id)->orderBy('created_at','DESC')->first();
		
		$user_info = Member::where('Member.id',$id)->leftjoin('users','Member.user_id','=','users.id')->select('Member.username','users.phone','users.email')->first();

		if($request->payment_method == "direct-bank"){
			return redirect()->back()->with('message',"This Payment method is not availble this time Please choose another.");
		}
		elseif($request->payment_method == 'Biz_Wallet'){
			return view('ecommerce2.ebiz-wallet',compact('user_info','checkorder'));
		}
		elseif($request->payment_method == 'payumoney'){
			return view('ecommerce2.payment',compact('user_info','checkorder'));
		}
	}

	public function checkoutForm1(Request $request,$id){
		$success = "";
		if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
	//Request hash
			$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';	
			if(strcasecmp($contentType, 'application/json') == 0){
				$data = json_decode(file_get_contents('php://input'));
				$hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
				$json=array();			
			// $request->session()->put('success',$hash);
				$json['success'] = $hash;
				echo json_encode($json);		
			}
			exit(0);
		}
	}  
	

	public function getSuccess(Request $request){
		$postdata = $_POST;
		
		$user = User::where('email',$postdata['email'])->first();
		$member = Member::where('user_id',$user->id)->first();	

		$updateorder = new Order;

		$updateorder = Order::where('user_id',$member->id)->orderBy('created_at','DESC')->first();
		$updateorder->transaction_id = $postdata['txnid'];
		$updateorder->encrypted_payment_id = $postdata['encryptedPaymentId'];
		$updateorder->status = $postdata['txnStatus'];

		$updateorder->update();

		$transaction_details = Order::where('user_id',$updateorder->user_id)->whereNotNull('transaction_id')->orderBy('updated_at','DESC')->get();


		return view('ecommerce2.result',compact('transaction_details'));
	}

	//To show session data on Mini Cart
	public function showMiniCart(){
		$cartproducts = array();
		$quantity = array();
		$i = 0;
		$bill = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
			$j=0;
			foreach(session()->get('cart') as $cart){
				echo $quantity[$j];
				$j++;
			}

			foreach(session()->get('cart') as $pro){
				$product = Product::where('id',$pro->product_id)->first();
				$price = $product->sell_price;
				$bill = $bill + $pro->quantity*$price;
			}
		}	

		return view('ecommerce2.minicart',compact('cartproducts','quantity','bill'))->render();
	}

	//Show Sorted Products on Search Category Page
	public function sorting(Request $request){
		$searched_products = array();
		$i=0;
		$sort_type = $request->sort_type;
		$checkId = MainCategory::where('slug',$request->cat_id)->first();
		echo $send_id = $checkId->id;
		// $send_id = $request->cat_id;
		$min = $request->min_cost;
		$max = $request->max_cost;
		$size = $request->size;
		$color = $request->color;

		$category_ids = array();
		$j = 1;
		$category_ids[0] = $send_id;

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		if($color=="" && $size=="" && $min=="" && $max==""){
			for($k=0 ; $k<$j ; $k++){			
				$products = Product::where('category_id',$category_ids[$k])->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}

		elseif($size=="" && $color==""){
			for($k=0 ; $k<$j ; $k++){			
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}

		elseif($size==""  && $min=="" && $max==""){
			for($k=0 ; $k<$j ; $k++){			
				$products = Product::where('category_id',$category_ids[$k])->where('product_color',$color)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}

		elseif($color=="" && $min=="" && $max==""){
			for($k=0 ; $k<$j ; $k++){			
				$products = Product::where('category_id',$category_ids[$k])->where('product_size','like','%'.$size.'%')->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}

		else{
			for($k=0 ; $k<$j ; $k++){			
				$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','like','%'.$size.'%')->where('product_color',$color)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}		
		
		if($sort_type == 'name'){
			$name = array_column($searched_products, 'name');
			array_multisort($name, SORT_ASC, $searched_products);
		}
		else if($sort_type == 'new'){
			$created_at = array_column($searched_products, 'created_at');
			array_multisort($created_at, SORT_ASC, $searched_products);
		}
		
		else if($sort_type == 'low-high'){
			$sell_price = array_column($searched_products, 'sell_price');
			array_multisort($sell_price, SORT_ASC, $searched_products);
		}
		
		else if($sort_type == 'high-low'){
			$sell_price = array_column($searched_products, 'sell_price');
			array_multisort($sell_price, SORT_DESC, $searched_products);
		}
		

		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}

	//Show Sorted Products on Search Category Page
	public function sortingSearch(Request $request){
		$searched_products = array();
		$i=0;
		echo $sort_type = $request->sort_type;
		$checkId = MainCategory::where('slug',$request->cat_id)->first();
		echo $send_id = $checkId->id;
		// echo $send_id = $request->cat_id;
		echo $keyword = $request->keyword;
		echo $min = $request->min_cost;
		echo $max = $request->max_cost;
		echo $size = $request->size;
		echo $color = $request->color;

		$category_ids = array();
		$j = 1;
		$category_ids[0] = $request->cat_id;

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		if($send_id != null){
			if($color=="" && $size=="" && $min=="" && $max==""){
				for($k=0 ; $k<$j ; $k++){			
					$products = Product::where('category_id',$category_ids[$k])->where('status','Active')->get();
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}

			elseif($size=="" && $color==""){
				for($k=0 ; $k<$j ; $k++){			
					$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}

			elseif($size==""  && $min=="" && $max==""){
				for($k=0 ; $k<$j ; $k++){			
					$products = Product::where('category_id',$category_ids[$k])->where('product_color',$color)->where('status','Active')->get();
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}

			elseif($color=="" && $min=="" && $max==""){
				for($k=0 ; $k<$j ; $k++){			
					$products = Product::where('category_id',$category_ids[$k])->where('product_size','like','%'.$size.'%')->where('status','Active')->get();
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}

			else{
				for($k=0 ; $k<$j ; $k++){			
					$products = Product::where('category_id',$category_ids[$k])->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','like','%'.$size.'%')->where('product_color',$color)->where('status','Active')->get();
					if($products != null){
						foreach($products as $product){
							$searched_products[$i++] = $product;
						}				
					} 
				}
			}	
		}

		else {
			$min=0;
			$pro_cnt =0;
			if($send_id=="" && $keyword!=""){
				if($size=="" && $color==""){
					$pdt = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else if($size==""){
					$pdt = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('product_color',$color)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else if($color == ""){
					$pdt = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else {
					$pdt = Product::where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%'.$size.'%')->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
			}
			else if($send_id != "" && $keyword != ""){
				if($size=="" && $color==""){
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else if($size==""){
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('product_color',$color)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else if($color == ""){
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else {
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('name','LIKE','%'.$keyword.'%')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%'.$size.'%')->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
			}

			else{
				if($size=="" && $color==""){
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else if($size==""){
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('product_color',$color)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else if($color == ""){
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
				else {
					$pdt = Product::where('category_id',$send_id)->where('status','Active')->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%'.$size.'%')->get();
					if($pdt != null){
						foreach($pdt as $p){
							$searched_products[$pro_cnt++] = $p;
						}
					}
				}
			}			
		}
		// die;

		
		if($sort_type == 'name'){
			
			if(!empty($searched_products)){
				$name = array_column($searched_products, 'name');
				array_multisort($name, SORT_ASC, $searched_products);				
			}
		}
		else if($sort_type == 'new'){
			
			if(!empty($searched_products)){
				$created_at = array_column($searched_products, 'created_at');
				array_multisort($created_at, SORT_ASC, $searched_products);
			}
		}
		
		else if($sort_type == 'low-high'){
			
			if(!empty($searched_products)){
				$sell_price = array_column($searched_products, 'sell_price');
				array_multisort($sell_price, SORT_ASC, $searched_products);
			}
		}
		
		else if($sort_type == 'high-low'){
			
			if(!empty($searched_products)){
				$sell_price = array_column($searched_products, 'sell_price');
				array_multisort($sell_price, SORT_DESC, $searched_products);
			}
		}
		

		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}

	public function addProductToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		$product_quantity = $request->quantity;

		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = $product_quantity;
			$cart[$product_id] = $item;
			$count++;
		}

		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);		

		if(!$cart)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved']); 
	}

	public function sendContactUsForm(Request $request)
	{
		$response = new StdClass;
		$response->status = 200;
		$response->msg = 'Something went wrong';
		$contact_user = new Contact;

		if($request->contact_name == null  ||  $request->contact_email == null  ||  $request->contact_subject == null  ||  $request->contact_message == null)
		{
			if($request->contact_name == null)
				$response->msg = 'Please Enter Your Name';
			if($request->contact_email == null)
				$response->msg = 'Please Enter Your Email';
			if($request->contact_subject)
				$response->msg = 'Please Enter Subject';
			if($request->contact_message == null)
				$response->msg = 'Message must not be empty';

			return response()->json($response);
			exit();
		}
		else{
			$name = $request->contact_name;
			$email = $request->contact_email;
			$subject = $request->contact_subject;
			$content = $request->contact_message;

			$contact_user->name = $name;
			$contact_user->email = $email;
			$contact_user->subject = $subject;
			$contact_user->message_content = $content;

			$contact_user->save();
			if($contact_user){

				$data = array('Name'=>$contact_user->name,'Email'=>$contact_user->email, 'Subject'=>$contact_user->subject, 'Message'=>$contact_user->message_content);
				Mail::send('ecommerce2.contact-form', $data, function($message) use ( $name,$email,$subject,$content)
				{   
					$message->from('no-reply@m-biz.in', 'Mailer');
					$message->to('info@m-biz.in', 'Admin')->subject('Query Mail');
				});		

				$response->msg ="Thank you .We will reply you soon";
				return response()->json($response);
			}
			else{
				echo " Something Went Wrong";
				$response->msg =" Something Went Wrong";
			}
		}

		return response()->json($response);
		exit();             
	}


	public function getColorProduct(Request $request){
		$searched_products = array();
		$i=0;
		$color = $request->color;
		$size = $request->size;
		$min = $request->min_cost;
		$max = $request->max_cost;
		$category_ids = array();
		
		$j = 1;
		$checkId = MainCategory::where('slug',$request->cat_id)->first();
		echo $send_id = $checkId->id;
		$category_ids[0] = $send_id;
		// $send_id = $request->cat_id;

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}


		if($size==""  && $max==""  && $min==""){
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('product_color',$color)->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		elseif($size==""){
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('product_color',$color)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('product_color',$color)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%'.$size.'%')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}

		
		shuffle($searched_products);
		
		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}

	//Filter products on the basis of there prices
	public function getSizedProduct(Request $request){
		echo $size = $request->size;
		echo $color = $request->color;
		
		$send_id = $request->cat_id;
		echo $min = $request->min_cost;
		echo $max = $request->max_cost;

		$category_ids = array();
		$category_ids[0] = $send_id;
		$j = 1;

		$searched_products = array();
		$i=0;	

		$cat = MainCategory::where('category_id',$send_id)->get();
		if($cat != null){
			foreach($cat as $cid){
				$category_ids[$j++] = $cid->id;
				$subcat = MainCategory::where('category_id',$cid->id)->get();
				if($subcat != null){
					foreach($subcat as $sid){
						$category_ids[$j++] = $sid->id;
					}
				}
			}
		}

		
		if($color=="" && $min=="" && $max=""){
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('product_size','LIKE','%'.$size."%")->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		elseif($color==""){
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('product_size','LIKE','%'.$size."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}
		else{
			for($k=0 ; $k<$j ; $k++){
				echo $category_ids[$k]."   ";
				$products = Product::where('category_id',$category_ids[$k])->where('product_color',$color)->where('product_size','LIKE','%'.$size."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				if($products != null){
					foreach($products as $product){
						$searched_products[$i++] = $product;
					}				
				} 
			}
		}

		shuffle($searched_products);
		return view('ecommerce2.product_cat',compact('searched_products','send_id'));
	}

	//Filter products on the basis of there prices
	public function getSizedSearchProduct(Request $request){
		$size = $request->size;		

		$searched_products = Product::where('product_size',$size)->where('status','Active')->get();

		shuffle($searched_products);

		return view('ecommerce2.product_cat',compact('searched_products'));
	}

	public function getColoredSearchProduct(Request $request){
		
		$color = $request->color;		

		$searched_products = Product::where('product_color',$color)->where('status','Active')->get();

		shuffle($searched_products);

		return view('ecommerce2.product_cat',compact('searched_products'));
	}

	public function getTransaction(){
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}
		return view('ecommerce2.transaction',compact('maincategory','cartproducts','quantity'));
	}
	

	public function checkWalletBalance(Request $request)
	{
		$response = new StdClass;
		$response->message = "Something went wrong";
		$wallet = Epoint::find($request->member_id);
		$bill = $request->bill;
		if($wallet->epoint<$bill){
			$response->message = "Not enough ecoins in your wallet to purchase. Please select another method.";
		}
		else{
			$response->message = "Ready to Pay";
		}
		return response()->json($response);
	}

	public function getPaymentEpoint(Request $request){

		$postdata = $_POST;
		
		$user = User::where('email',$postdata['email'])->first();
		$member = Member::where('user_id',$user->id)->first();	
		$wallet = Epoint::find($member->id);
		
		$encrypt = Hash::make($postdata['txnid']);
		
		$wallet->epoint = $wallet->epoint - $postdata['amount'];
		
		$wallet->update();

		$mobile = $postdata['mobile'];
		$message = "Your%20payment%20(Id-".$postdata['txnid'].")%20of%20Rs.".$postdata['amount']."%20for%20M-Biz%20Trading%20Pvt.%20Ltd.%20is%20successful.";
		$url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
            // die;
            // $url ="http://103.247.98.91/API/SendMsg.aspx?uname=backstage023&pass=backstage023&send=MBIZTL&dest=$mobile&msg=$message";  
            // $url = "";
		$c = curl_init();
		curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($c,CURLOPT_HTTPGET ,1);

		curl_setopt($c, CURLOPT_URL, $url);
		$contents = curl_exec($c);
		if (curl_errno($c)) {
			echo 'Curl error: ' . curl_error($c);
		}
		else{
			curl_close($c);
		}
		
		$updateorder = new Order;

		$updateorder = Order::where('user_id',$member->id)->orderBy('created_at','DESC')->first();
		$updateorder->transaction_id = $postdata['txnid'];
		$updateorder->encrypted_payment_id = $encrypt;
		$updateorder->status = 'SUCCESS';

		$updateorder->update();

		$transaction_details = Order::where('user_id',$updateorder->user_id)->whereNotNull('transaction_id')->orderBy('updated_at','DESC')->get();

		return view('ecommerce2.result',compact('transaction_details'));
	}
}
