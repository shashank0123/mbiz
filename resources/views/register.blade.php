@extends('front.app')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important;
  }
  .pink-text {  font-size: 24px;  }

  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .card.bordered .card-action {
     border-top: none !important; 
}


  .pull-right button { margin-bottom: 20px !important }
  @media screen and (max-width: 991px)
  {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }

  @media screen and (max-width: 580px)
  {
    .card { max-width:90%;margin-left: 5%; }
  }

  #login-head { width: 100% !important; background-color: orange }
  #otpcontainer { display: none; }
</style>

@section('title')
Register - {{ config('app.name') }}
@stop

@section('content')
<div class="clearfix"></div>
<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: orange; position: absolute; top: 0;">
    <div style="background-color: orange;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
    <ul>
      <li style="list-style: none">
        <a href="#" style="color: #fff; font-size: 14px">Help Line:  1800-419-8447</a><br>
      </li>

    </ul>
  </div>
</div>

<div class="card bordered z-depth-2" style="margin-top: 70px">
  <div class="card-header">
    <div class="brand-logo" style="text-align: center;">
      <img src="{{ asset('asset/images/menu/logo/mbiz.png') }}" width="100">
    </div>
  </div>
  <div id="formcontainer" class="container m-b-30" style="max-width: 100%;">
    <form class="form-floating action-form" id="register" http-type="post" data-url="{{ route('registerforuse', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong pink-text">Registeration Form - Become a member</div>
        </div>

        <div class="form-group">
          <label for="username">Sponsor ID</label>
          <input type="text" style="text-transform: uppercase;" name="sponsorId" class="form-control" id="sponsorId" required="required">
        </div>

        <div class="form-group">
          <label for="username">Name</label>
          <input type="text" name="username" class="form-control" id="username" required="required">
        </div>

        <div class="form-group">
          <label for="username">Email</label>
          <input type="text" name="email" class="form-control" id="email" required="required">
        </div>

        <div class="form-group">
          <label for="username">Mobile</label>

          <input type="text" name="mobile" class="form-control" id="phone" pattern="[789][0-9]{9}" required="required">
        </div>

        <div class="form-group">
          <label for="username">Adhaar</label>
          <input type="text" name="adhaar" class="form-control" id="username" pattern="[0-9]{12}" required="required">
        </div>
      </div>

      <div class="card-action clearfix">
        <div class="pull-left">
          <a href="/en/login" class="btn btn-link black-text">              
            <span style="color:red">Login</span>
          </a>
        </div>

        <div class="pull-right">
          <span id="submit" onclick="showDiv()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span>@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
    </form>
    <br>
  </div>

  <div id="otpcontainer" class="container m-b-30" style="max-width: 100%;">
    <form id="otpform" class="form-floating action-form" http-type="post" data-url="{{ route('verifyotp', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong pink-text">Verify Your OTP</div>
        </div>
        <input type="hidden" name="email" id="emailvalue">
        <input type="hidden" name="sponsoredId" id="sponsoredId">

        <div class="form-group">
          <label for="username">Enter OTP</label>
          <input type="text" name="otp" class="form-control" id="otp" required="">
        </div>
      </div>
      <input type="hidden" name="verify_otp" class="form-control" id="verify_otp" required="">

      <div class="card-action clearfix"> 
        <div class="pull-right" >
          <span id="verify" onclick="verifyOTP()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span>@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

@stop

<script>     

  function showDiv(){    
   var request = $.ajax({
    type: "POST",
    url: "/register",
    data: $('#register').serialize(),

  });
   request.done(function(msg) {
    if (msg.message == 'Data Inserted Successfully'){

      $('#formcontainer').hide();
      $('#emailvalue').val($('#email').val())
      $('#sponsoredId').val($('#sponsorId').val())
      console.log(msg);
      $("#otpcontainer").show();
    }
    else
      Swal(msg.message);                  
  });

   request.fail(function(msg) {
    Swal(msg.message);
  });
 }


 function verifyOTP(){    
  var request = $.ajax({
    type: "GET",
    url: "verifyotp",
    data: $('#otpform').serialize(),      
  });
  request.done(function(msg) {
    if (msg.message == 'Registered Successfully. Please Wait we will call you shortly.'){
      console.log(msg);
      // Swal(msg.message);
       window.location.href = "/register/membership-fee/"+msg.user_id;      
    }
    else{
      Swal(msg.message);
    }
  });

  request.fail(function(msg) {
    Swal(msg.message);
  });
}  
</script>
