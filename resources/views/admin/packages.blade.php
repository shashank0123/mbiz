@extends('layouts.admin')

@section('content')
  <div class="container-fluid">
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-wallet-giftcard"></i> Package</h1>
        </div>
        <div class="card">
          <div>
            <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
              <thead style="text-align: center;">
                <tr>
                  <th><b>Amount</b></th>                
                  <th><b>Group Level</b></th>
                  <th><b>Max Pair Daily</b></th>
                  <th><b>Max Pair Bonus</b></th>
                  <th><b>Shares Limit</b></th>                  
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="input-group">
                      <input type="number" class="form-control" name="pairing_percent" value="" required="" min="0">
                      <span class="input-group-addon" style="padding-left: 10px;padding-top: 5px">%</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="number" name="group_level" class="form-control" value="" required="" min="0">
                      <span class="input-group-addon" style="padding-left: 10px;padding-top: 5px">level(s)</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="number" class="form-control" name="max_pair" value="" required="" min="0">
                      <span class="input-group-addon" style="padding-left: 10px;padding-top: 5px">pair(s)</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <span class="input-group-addon" style="padding-right: 10px;padding-top: 5px">$</span>
                      <input type="number" class="form-control" name="max_pairing_bonus" value="" required="" min="0">
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="number" class="form-control" name="share_limit" value="" required="" min="0">
                      <span class="input-group-addon" style="padding-left: 10px;padding-top: 5px">shares</span>
                    </div>
                  </td>
                  <td>
                    <button class="btn btn-warning btn-flat-border btn-update" data-url="" type="submit">
                      <i class="md md-mode-edit"></i> Update
                    </button>
                  </td>
                </tr>
           
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
@endsection