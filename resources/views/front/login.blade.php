@extends('front.app')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important;
  }
  .pink-text {  font-size: 24px;  }
  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .pull-right button { margin-bottom: 20px !important }

  @media screen and (max-width: 991px) {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }
  @media screen and (max-width: 580px) {
    .card { max-width:90%;margin-left: 5%; }
  }
  #login-head { width: 100% !important; background-color: orange }
</style>

<div class="clearfix"></div>
                  
<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: orange; margin-top: -13px;">
                        <div style="background-color: orange;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
                            <ul>
                                <li style="list-style: none">
                                    <a href="#" style="color: #fff; font-size: 14px">Help Line:  1800-419-8447</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

  <div class="card bordered z-depth-2" >
    <div class="card-header">
      <div class="brand-logo" style="text-align: center;">
        <img src="{{ asset('/asset/images/menu/logo/mbiz.png') }}" width="100">
      </div>
    </div>

    <div class="container m-b-30" style="max-width: 100%;">
    <form class="form-floating action-form" http-type="post" data-url="{{ route('login.post', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong pink-text">@lang('login.title')</div>
          <!-- <p><a href="{{ route('login', ['lang' => 'en']) }}">English</a> | <a href="{{ route('login', ['lang' => 'chs']) }}">简体中文</a> | <a href="{{ route('login', ['lang' => 'cht']) }}">繁體中文</a></p> -->
          <p class="card-title-desc"> @lang('login.subtitle')</p>
        </div>
        <div class="form-group">
          <label for="username">@lang('login.username')</label>
          <input type="text" style="text-transform: uppercase;" name="username" class="form-control" id="username" required="">
        </div>
        <div class="form-group">
          <label for="password">@lang('login.password')</label>
          <input type="password" name="password" class="form-control" id="password" required="">
        </div>
        <div class="form-group">
          <div class="checkbox">
          <label><input type="checkbox" name="remember"> @lang('login.remember') </label>
          

          </div>
        </div>

        <div class="card-action clearfix">

          <div class="pull-left">
            <a href="/register" class="btn btn-link black-text">

              <span style="color:red">Register Here</span>
            </a>
          </div>


          <div class="pull-right">
            <button type="submit" class="btn btn-link black-text">
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
              <span>@lang('login.title')</span>
            </button>
          </div>
        </div>

        <div class="row" style="text-align: center; margin-top: -20px">
          <label><a href="/passwordReset" id="forget"> @lang('login.forgotPassword') ?</a> </label>
        </div>
      </form>
    </div>
  </div>
</div>

@stop
