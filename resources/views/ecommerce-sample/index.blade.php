<?php
use App\MainCategory;
use App\Product;
use App\Review;
?>

@extends('layouts/ecommerce')
<style>
    .logo-img{ width: 100%; height: auto; }
    .fa-star-of-david { color: #ccc !important; }                                        
    .quick-view-btn { background-color: #333333; }
    .quick-view-btn:hover { background-color: orange; }

    .front-image { height: 250px; }
    .front-image-new { height: 200px; }

    
</style>
@section('content')
<!-- Hiraola's Header Main Area End Here -->
<div class="slider-with-category_menu"  id="response-hide" >
    <div class="container-fluid">
        <div class="row">
            <div class="col grid-half order-md-2 order-lg-1">
                <div class="category-menu">
                    <div class="category-heading">
                        <h2 class="categories-toggle"><span>Shop by categories</span></h2>
                    </div>
                    <div id="cate-toggle" class="category-menu-list">
                        <ul>
                            @foreach($maincategory as $row)
                            <li class="right-menu rx-parent"><a href="#" style="color: orange">{{$row->Mcategory_name}}</a>
                                <?php $category=MainCategory::where('category_id',$row->id)->get(); ?>
                                <ul class="cat-mega-menu">
                                    @foreach($category as $col)
                                    <li class="right-menu cat-mega-title">               
                                        <a>{{ $col->Mcategory_name }}</a>
                                        <?php $subproduct=MainCategory::where('category_id',$col->id)->get(); ?>
                                        <ul>
                                            @foreach($subproduct as $sets)
                                            <li><a href="/product-list/{{$sets->id}}">{{$sets->Mcategory_name}}</a></li>
                                            @endforeach                                
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>                                    
                            </li>                            
                            @endforeach                                        

                            <li class="rx-parent">
                                <a class="rx-default" style="color: orange">More Categories</a>
                                <a class="rx-show" style="color: orange">Collapse</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col grid-full order-md-1 order-lg-2">
                <div class="hiraola-slider_area">
                    <div class="main-slider">
                        <!-- Begin Single Slide Area -->
                        <div class="single-slide animation-style-01 bg-1">
                            <div class="container">
                                <div class="slider-content">
                                        {{-- <h5><span>Black Friday</span> This Week</h5>
                                        <h4>Estrogen Protein</h4>
                                        <!-- <h3>Surface Studio 2019</h3> -->
                                        <h4>Starting at <span>₹12500</span></h4>
                                        <div class="hiraola-btn-ps_left slide-btn">
                                            <a class="hiraola-btn" href="#">Shopping Now</a>
                                        </div> --}}
                                    </div>
                                    {{-- <div class="slider-progress"></div> --}}
                                </div>
                            </div>
                            <!-- Single Slide Area End Here -->
                            <!-- Begin Single Slide Area -->
                            <div class="single-slide animation-style-02 bg-2">
                                <div class="container">
                                    <div class="slider-content">
                                        {{-- <h5><span>-10% Off</span> This Week</h5>
                                        <h4>100% Why protein</h4>
                                        <!-- <h3>Pro+ Obsidian</h3> -->
                                        <h4>Starting at <span>₹12500</span></h4>
                                        <div class="hiraola-btn-ps_left slide-btn">
                                            <a class="hiraola-btn" href="#">Shopping Now</a>
                                        </div> --}}
                                    </div>
                                    {{-- <div class="slider-progress"></div> --}}
                                </div>
                            </div>
                            <div class="single-slide animation-style-02 bg-3">
                                <div class="container">
                                    <div class="slider-content">
                                        {{-- <h5><span>Black Friday</span> This Week</h5>
                                        <h4>Egg Protein</h4>
                                        <!-- <h3>Surface Studio 2019</h3> -->
                                        <h4>Starting at <span>₹12500</span></h4>
                                        <div class="hiraola-btn-ps_left slide-btn">
                                            <a class="hiraola-btn" href="#">Shopping Now</a>
                                        </div> --}}
                                    </div>
                                    {{-- <div class="slider-progress"></div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col grid-half grid-md_half order-md-2 order-lg-3">
                    <div class="banner-item img-hover_effect">
                        <a href="#">
                            <img class="img-full" src="asset\images\trend\banner1.jpg" alt="Hiraola's Banner">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .hiraola-product_area { margin-top: -50px; margin-bottom: -50px }
        .hiraola-section_title h4 a:hover{ color: orange !important; }
    </style>
    <!-- Begin Hiraola's Product Area -->
    <div class="hiraola-product_area" style="width: 90%;margin:0 5%">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hiraola-section_title">
                        <h4><a href="#">New Arrival</a></h4>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="hiraola-product_slider">
                        <!-- Begin Hiraola's Slide Item Area -->
                        @foreach($product as $row)
                        <div class="slide-item">
                            <div class="single_product">
                                <div class="product-img">                                   
                                    <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image1 }} " class="front-image-new"/>
                                    {{-- <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image_url }} " class="logo-img" /> --}}
                                    <?php $discount=(($row->mrp - $row->sell_price)*100)/$row->mrp;

                                    $rate = Review::where('product_id',$row->id)->avg('rating');
                                    $final_rate = floor($rate);
                                    ?>
                                    
                                    <div class="add-actions">
                                        <ul>
                                            <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                <button  onclick="addCart({{$row->id}})" id="">
                                                    <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag" style="color: #fff"></i></a>
                                                </button>
                                            </li>

                                            <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">
                                                <button  onclick="openview({{ $row->id }},'{{ $row->name }}',{{$row->sell_price}},{{ $row->mrp }},'{{$row->short_descriptions}}','{{$row->image1}}','{{$row->image2}}','{{$row->image3}}',<?php echo $final_rate; ?>)">
                                                    <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                        <i
                                                        class="ion-eye" style="color: #fff; "></i>
                                                    </a>
                                                </button></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="product-detail/{{$row->id}}">{{strtoupper($row->name)}}</a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs.{{$row->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>
                                                    <li style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                        <button  onclick="" id="addWishList">
                                                            <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="ion-android-favorite-outline"></i></a>
                                                        </button>

                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                                <ul>
                                                    <li> 
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- Hiraola's Slide Item Area End Here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hiraola's Product Area End Here -->

        {{-- Static Banner --}}
        <div class="static-banner_area" style="margin-top: -40px">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="static-banner-image"></div>
                        <div class="static-banner-content">
                           
                            <h2>Featured Product</h2>
                            <h3>Electronics</h3>
                            <p class="schedule">
                                Starting at
                                <span> ₹12500  </span>
                            </p>
                            <div class="hiraola-btn-ps_left">
                                <a href="#" class="hiraola-btn">Shopping Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        





        <!-- Begin Hiraola's Product Tab Area Three -->
        <!-- Begin Hiraola's Product Tab Area -->
        @foreach($maincategory as $row)
        <div class="hiraola-product-tab_area-2" style="width: 90%;margin:0 5%">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product-tab">
                            <div class="hiraola-tab_title">
                                <h4><a href="#">{{ $row->Mcategory_name }}</a></h4>
                            </div>
                            


                            <ul class="nav product-menu">
                                <li><a class="active" data-toggle="tab" href="#necklaces"><span>{{ $row->Mcategory_name }} Products</span></a></li>
                               <!--  <li><a data-toggle="tab" href="#Ring"><span>Ring</span></a></li>
                                <li><a data-toggle="tab" href="#bracelet"><span>Bracelet</span></a></li>
                                <li><a data-toggle="tab" href="#anklet"><span>Anklet</span></a></li> -->
                            </ul>
                        </div>

                        <div class="tab-content hiraola-tab_content">
                            <div id="necklaces" class="tab-pane active show" role="tabpanel">
                                <div class="hiraola-product-tab_slider-2">

                                    
                                    <?php
                                    $productmain = Product::where('category_id',$row->id)->get();
                                    ?>

                                    @foreach($productmain as $rowe)
                                    <div class="slide-item">
                                        <div class="single_product">
                                            <div class="product-img">
                                                <a href="#">
                                                    <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $rowe->image1 }} " class="front-image"  />
                                                </a>
                                                <div class="add-actions">
                                                    <ul>
                                                        <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                            <button  onclick="addCart({{$rowe->id}})" id="">
                                                                <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag" style="color: #fff"></i></a>
                                                            </button>
                                                        </li>

                                                        <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">
                                                            <button  onclick="openview({{ $rowe->id }},'{{ $rowe->name }}',{{$rowe->sell_price}},{{ $rowe->mrp }},'{{$rowe->short_descriptions}}','{{$rowe->image1}}','{{$rowe->image2}}','{{$rowe->image3}}',<?php echo $final_rate; ?>)">
                                                                <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                                    <i
                                                                    class="ion-eye" style="color: #fff; "></i>
                                                                </a>
                                                            </button></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="hiraola-product_content">
                                                    <div class="product-desc_info">
                                                        <h6><a class="product-name" href="product-detail/{{$rowe->id}}">{{$rowe->name}}</a></h6>
                                                        <div class="price-box">
                                                            <span class="new-price">Rs.{{$rowe->sell_price}}</span>
                                                        </div>
                                                        <div class="additional-add_action">
                                                            <ul>

                                                                <li  style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                    <button  onclick="" id="addWishList">
                                                                        <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="ion-android-favorite-outline"></i></a>
                                                                    </button>

                                                                </li>                          
                                                            </ul>
                                                        </div>
                                                        <div class="rating-box">
                                                            <ul>
                                                                <li> 
                                                                    <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                    <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                    <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                    <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                    <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach



                                        
                                        <?php $category = MainCategory::where('category_id',$row->id)->get(); ?>
                                        @foreach($category as $rows)
                                        
                                        <?php
                                        $catproduct = Product::where('category_id',$rows->id)->get();
                                        ?>

                                        @foreach($catproduct as $cols)
                                        <div class="slide-item">
                                            <div class="single_product">
                                                <div class="product-img">
                                                    <a href="#">
                                                        <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $cols->image1 }} " class="front-image"  />
                                                    </a>
                                                    <div class="add-actions">
                                                        <ul>
                                                            <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                <button  onclick="addCart({{$cols->id}})" id="">
                                                                    <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag" style="color: #fff"></i></a>
                                                                </button>
                                                            </li>

                                                            <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">
                                                                <button  onclick="openview({{ $cols->id }},'{{ $cols->name }}',{{$cols->sell_price}},{{ $cols->mrp }},'{{$cols->short_descriptions}}','{{$cols->image1}}','{{$cols->image2}}','{{$cols->image3}}',<?php echo $final_rate; ?>)">
                                                                    <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                                        <i
                                                                        class="ion-eye" style="color: #fff; "></i>
                                                                    </a>
                                                                </button></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="hiraola-product_content">
                                                        <div class="product-desc_info">
                                                            <h6><a class="product-name" href="product-detail/{{$cols->id}}">{{$cols->name}}</a></h6>
                                                            <div class="price-box">
                                                                <span class="new-price">Rs.{{$cols->sell_price}}</span>
                                                            </div>
                                                            <div class="additional-add_action">
                                                                <ul>

                                                                    <li  style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                        <button  onclick="" id="addWishList">
                                                                            <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="ion-android-favorite-outline"></i></a>
                                                                        </button>

                                                                    </li>                          
                                                                </ul>
                                                            </div>
                                                            <div class="rating-box">
                                                                <ul>
                                                                    <li> 
                                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach

                                            
                                            <?php
                                            $subproduct = MainCategory::where('category_id',$rows->id)->get();
                                            ?>
                                            @foreach($subproduct as $set)
                                            
                                            <?php
                                            $subcatproduct = Product::where('category_id',$set->id)->get();
                                            ?>

                                            @foreach($subcatproduct as $sets)
                                            <div class="slide-item">
                                                <div class="single_product">
                                                    <div class="product-img">
                                                        <a href="#">
                                                            <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $sets->image1 }} " class="front-image"  />
                                                        </a>
                                                        <div class="add-actions">
                                                            <ul>
                                                                <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                    <button  onclick="addCart({{$sets->id}})" id="">
                                                                        <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag" style="color: #fff"></i></a>
                                                                    </button>
                                                                </li>

                                                                <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">
                                                                    <button  onclick="openview({{ $sets->id }},'{{ $sets->name }}',{{$sets->sell_price}},{{ $sets->mrp }},'{{$sets->short_descriptions}}','{{$sets->image1}}','{{$sets->image2}}','{{$sets->image3}}',<?php echo $final_rate; ?>)">
                                                                        <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                                            <i
                                                                            class="ion-eye" style="color: #fff; "></i>
                                                                        </a>
                                                                    </button></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="hiraola-product_content">
                                                            <div class="product-desc_info">
                                                                <h6><a class="product-name" href="product-detail/{{$sets->id}}">{{$sets->name}}</a></h6>
                                                                <div class="price-box">
                                                                    <span class="new-price">Rs.{{$sets->sell_price}}</span>
                                                                </div>
                                                                <div class="additional-add_action">
                                                                    <ul>
                                                                        <li  style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                            <button  onclick="" id="addWishList">
                                                                                <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To WishList"><i class="ion-android-favorite-outline"></i></a>
                                                                            </button>
                                                                        </li>                   
                                                                    </ul>
                                                                </div>
                                                                <div class="rating-box">
                                                                    <ul>
                                                                        <li> 
                                                                            <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                            <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                            <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                            <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                            <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach                                      
                                                @endforeach                                    
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach

                    <!-- Hiraola's Product Tab Area End Here -->

                    <?php $trend = Product::where('trending','Active')->get();?>

                    <!-- Begin Hiraola's Product Tab Area Three -->
                    <div class="hiraola-product-tab_area-4" style="width: 90%;margin:0 5%">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="product-tab">
                                        <div class="hiraola-tab_title">
                                            <h4><a href="#">Trending Products</a></h4>
                                        </div>
                                        <ul class="nav product-menu">
                                            <li><a class="active" data-toggle="tab" href="#necklaces-2"><span>TRENDING PRODUCTS
                                            </span></a></li>

                                        </ul>
                                    </div>
                                    <div class="tab-content hiraola-tab_content">
                                        <div id="necklaces-2" class="tab-pane active show" role="tabpanel">
                                            <div class="hiraola-product-tab_slider-2">
                                                <!-- Begin Hiraola's Slide Item Area -->
                                                @foreach($trend as $row)
                                                <?php 
                                                $rate = Review::where('product_id',$row->id)->avg('rating');
                                                $final_rate = floor($rate);
                                                ?>
                                                <div class="slide-item">
                                                    <div class="single_product">
                                                        <div class="product-img">
                                                            <a href="#">
                                                                <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image1 }} " class="front-image"/>
                                                                {{-- <img class="secondary-img" src="asset\images\trend\1.jpg" alt="Hiraola's Product Image"> --}}
                                                            </a>
                                                            <div class="add-actions">
                                                                <ul>
                                                                    <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                        <button  onclick="addCart({{$row->id}})" id="">
                                                                            <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag" style="color: #fff"></i></a>
                                                                        </button>
                                                                    </li>

                                                                    <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">
                                                                        <button  onclick="openview({{ $row->id }},'{{ $row->name }}',{{$row->sell_price}},{{ $row->mrp }},'{{$row->short_descriptions}}','{{$row->image1}}','{{$row->image2}}','{{$row->image3}}',<?php echo $final_rate; ?>)">
                                                                            <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                                                <i
                                                                                class="ion-eye" style="color: #fff; "></i>
                                                                            </a>
                                                                        </button></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="hiraola-product_content">
                                                                <div class="product-desc_info">
                                                                    <h6><a class="product-name" href="product-detail/{{$row->id}}">{{$row->name}}</a></h6>
                                                                    <div class="price-box">
                                                                        <span class="new-price">Rs.{{$row->sell_price}}</span>
                                                                    </div>
                                                                    <div class="additional-add_action">
                                                                        <ul>

                                                                            <li style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                                                <button  onclick="" id="addWishList">
                                                                                    <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i class="ion-android-favorite-outline" ></i></a>
                                                                                </button>

                                                                            </li>




                                                                        </ul>
                                                                    </div>
                                                                    <div class="rating-box">
                                                                        <ul>
                                                                            <li> 
                                                                                <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                                <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                                <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                                <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                                <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach      
                                                    <!-- Hiraola's Slide Item Area End Here -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Hiraola's Product Tab Area Three End Here -->

                        {{-- <style type="text/css"> 
                            .shipping-content h6{color: white;}
                            .shipping-content p{color: white;}

                        </style> --}}
                        @endsection

                        <div class="modal fade modal-wrapper" id="viewproduct" tabindex="-1" role="dialog" aria-labelledby="viewproductLabel" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">                                   

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="modal-inner-area sp-area row">
                                        <div class="col-lg-5 col-md-5">
                                            <div class="sp-img_area">
                                                <div class="sp-img_slider-2 slick-img-slider hiraola-slick-slider arrow-type-two" data-slick-options='{
                                                "slidesToShow": 1,
                                                "arrows": false,
                                                "fade": true,
                                                "draggable": false,
                                                "swipe": false,
                                                "asNavFor": ".sp-img_slider-nav"
                                            }'>
                                            <div class="single-slide " >
                                                <img src=""  id="img1" style="width: 100%; height: 350px"/>
                                            </div>
                                            <div class="single-slide " >
                                                <img src=""  id="img2" style="width: 100%; height: 350px"/>
                                            </div>
                                            <div class="single-slide ">
                                                <img src=""  id="img3" style="width: 100%; height: 350px"/>
                                            </div>
                                            {{-- <div class="single-slide umber">
                                                <img src="asset/images/single-product/large-size/4.jpg" alt="Hiraola's Product Image">
                                            </div> --}}
                                        </div>
                                        <div class="sp-img_slider-nav slick-slider-nav hiraola-slick-slider arrow-type-two" data-slick-options='{
                                        "slidesToShow": 3,
                                        "asNavFor": ".sp-img_slider-2",
                                        "focusOnSelect": true
                                    }' data-slick-responsive='[
                                    {"breakpoint":768, "settings": {"slidesToShow": 3}},
                                    {"breakpoint":577, "settings": {"slidesToShow": 3}},
                                    {"breakpoint":481, "settings": {"slidesToShow": 2}},
                                    {"breakpoint":321, "settings": {"slidesToShow": 2}}
                                    ]'>
                                    <div class="single-slide red">
                                        <img src="" class="logo-img" id="imgt1"/>
                                    </div>
                                    <div class="single-slide orange">
                                        <img src="" class="logo-img" id="imgt2"/>
                                    </div>
                                    <div class="single-slide brown">
                                        <img src="" class="logo-img" id="imgt3"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <div class="sp-content">
                                <div class="sp-heading">
                                    <h5 id="viewname"></h5>
                                </div>
                                {{-- <input type="text" id="viewrate" name="rate"> --}}
                                <style>
                                    .fa-star-of-david { color: #ccc !important; }
                                </style>
                                <div class="rating-box">
                                    <ul>
                                        <li> 
                                            <i class="fa <?php if(isset($final_rate) && $final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                            <i class="fa <?php if(isset($final_rate) && $final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                            <i class="fa <?php if(isset($final_rate) && $final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                            <i class="fa <?php if(isset($final_rate) && $final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                            <i class="fa <?php if(isset($final_rate) && $final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                        </li>
                                    </ul>
                                </div>
                                <div class="price-box">
                                    <h3>Rs.
                                        <span class="new-price" id="viewprice"></span></h3>
                                        {{--  <span class="old-price">£93.68</span> --}}
                                    </div>
                                    <div class="essential_stuff">
                                        <ul>
                                            <li>Discount : <span id='viewdiscount'></span><span>%</span></li>
                                        </ul>
                                    </div>
                                    <div class="list-item">
                                        <p id="viewdescription"></p>
                                    {{-- <ul>
                                        <li>10 or more £81.03</li>
                                        <li>20 or more £71.09</li>
                                        <li>30 or more £61.15</li>
                                    </ul> --}}
                                </div>
                                {{-- <div class="list-item last-child">
                                    <p id="viewdescription"></p>
                                </div> --}}
                                <br>
                                <div class="quantity">
                                    <label>Quantity</label>
                                    <div class="cart-plus-minus">
                                        <input class="cart-plus-minus-box" value="1" type="text">
                                        <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                        <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                    </div>
                                </div>
                                <div class="hiraola-group_btn">
                                    <ul>
                                        <li><a href="#" class="add-to_cart">Cart To Cart</a></li>
                                        <li style="margin-top: -30px;"><a href="#" ><i class="ion-android-favorite-outline"></i></a></li>
                                        <li style="margin-top: -30px;"><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                    </ul>
                                </div>
                                <div class="hiraola-tag-line">
                                    <h6>Tags:</h6>
                                    <a href="javascript:void(0)">Ring</a>,
                                    <a href="javascript:void(0)">Necklaces</a>,
                                    <a href="javascript:void(0)">Braid</a>
                                </div>
                                <div class="hiraola-social_link">
                                    <ul>
                                        <li class="facebook">
                                            <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                                <i class="fab fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery JS -->
    <script src="asset/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="asset/js/plugins/jquery-ui.min.js"></script>

    <script>
        function openview(id,name,price,mrp,description,img1,img2,img3,rate){
         // alert(img1+img2+img3);
    // $('#viewid').val(id);
    $('#viewname').text(name);

    // alert(description);
    $('#viewdescription').text(description);
    $('#viewprice').text(price);
    $("#img1").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewproduct').modal('show');
    $('#viewrate').val(rate);

} 


function addCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        /* the route pointing to the post function */
        url: '/index/add-to-cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) { 
            $("#ajaxdata").html(data);
        }
    }); 


    // function showCart(){
    //     $.ajax({
    //         url: '/index/show-cart',
    //         type: 'POST',

    //         success: function (data) { 
    //         alert(data);

    //         $("#ajaxdata").append(html);
    //     }
    //     });
    // }

}    
</script>

