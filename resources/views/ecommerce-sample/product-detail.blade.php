<?php 
use App\MainCategory;
use App\Product;
use App\Review;

$products = Product::all();
$rate = Review::where('product_id',$product->id)->avg('rating');
$final_rate = floor($rate);
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>MBiz</title>
  <meta name="robots" content="noindex, follow" />
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="/asset/images/favicon.ico">
    <!-- CSS
     ============================================ -->
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="/asset/css/vendor/bootstrap.min.css">
     <!-- Fontawesome -->
     <link rel="stylesheet" href="/asset/css/vendor/font-awesome.css">
     <!-- Fontawesome Star -->
     <link rel="stylesheet" href="/asset/css/vendor/fontawesome-stars.css">
     <!-- Ion Icon -->
     <link rel="stylesheet" href="/asset/css/vendor/ion-fonts.css">
     <!-- Slick CSS -->
     <link rel="stylesheet" href="/asset/css/plugins/slick.css">
     <!-- Animation -->
     <link rel="stylesheet" href="/asset/css/plugins/animate.css">
     <!-- jQuery Ui -->
     <link rel="stylesheet" href="/asset/css/plugins/jquery-ui.min.css">
     <!-- Lightgallery -->
     <link rel="stylesheet" href="/asset/css/plugins/lightgallery.min.css">
     <!-- Nice Select -->
     <link rel="stylesheet" href="/asset/css/plugins/nice-select.css">
     <!-- Main Style CSS (Please use minify version for better website load performance) -->
     <link rel="stylesheet" href="/asset/css/style.css">
     <!--<link rel="stylesheet" href="/asset/css/style.min.css">-->
     <style>
       .header-bottom_area .main-menu_area > nav > ul > li { padding-right: 30px !important; }
       .header-middle_area .hm-searchbox { margin-top: 20px !important; }
       .header-middle_area { padding:  8px 30px !important; }

       #logo-img { width: 100px !important; height: 100px !important; }
       #response-show { display: none !important; }
       @media screen and (max-width: 991px) {
        .header-bottom_area .header-logo { padding: 10px 0 0px !important;}   
      }
      .header-right_area ul li a { padding: 3px 10px 3px 10px !important }
      .fa-star-of-david { color: #ccc !important; }


      @media screen and (max-width : 768px)
      {
        #response-hide { display: none; }
        #response-show { display: block !important; }
      }
    </style>
  </head>

  <body class="template-color-1">
    <div class="main-wrapper">
      <div class="header-middle_area d-none d-lg-block" style="background-color: #fff">
        <div class="container-fluid" style="background-color: orange;margin-top: -10px !important;margin-left: -3%;margin-right: -3% padding: 0 !important; width: 105.5%;">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="header-right_area" >
              <ul>
                <li>
                  <a href="#" style="color: #fff; font-size: 14px">Help Line:  1800-419-8447</a>
                </li>
                <li>
                  <a href="#user-view" class="wishlist-btn toolbar-btn" title="User" data-toggle="tooltip" data-placement="top">
                    <!-- <i class="ion-android-favorite-outline"></i> -->
                    <i class="fa fa-user" style="color: #fff;font-size: 14px"></i> 
                  </a>
                </li>
                <li>
                  <a href="#mobileMenu" class="mobile-menu_btn toolbar-btn color--white d-lg-none d-block">
                    <i class="ion-navicon" style="color: #fff"></i>
                  </a>
                </li>
                <li><a class="hiraola-add_compare toolbar-btn" href="/wishlist" data-toggle="tooltip" data-placement="top" title="Wishlist">
                  <i
                  class="ion-android-favorite-outline" style="color: #fff;font-size: 14px"></i></a>
                </li>
                <li>
                  <button onclick="showCart()"> <a href="#miniCart" class="minicart-btn toolbar-btn" data-toggle="tooltip" data-placement="top" title="MiniCart">
                    <i class="ion-bag" style="color: #fff;font-size: 14px" ></i>
                  </a></button>
                </li>
              </ul>
            </div>
          </div>                    
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-3">
              <div class="header-logo">
                <a href="/index">
                  <img src="/asset/images/menu/logo/mbiz.png" alt="Hiraola's Header Logo" style="width: 150px; height: 80px; margin-top: 5px">
                </a>
              </div>
            </div>
            <div class="col-lg-9">
              <div class="hm-form_area">
                <form action="/search-product" method="POST" class="hm-searchbox">
                  <select class="nice-select select-search-category" name="subcategory">
                    <option value="0">All</option>;
                    @foreach($maincategory as $row)
                    <?php $cat = MainCategory::where('category_id',$row->id)->get();
                    ?>
                    @foreach($cat as $col)
                    <?php $subcat = MainCategory::where('category_id',$col->id)->get();
                    ?>
                    @foreach($subcat as $cols )
                    <option value="{{ $cols->id }}">{{ $cols->Mcategory_name }}</option>                    
                    @endforeach           
                    @endforeach           
                    @endforeach
                  </select>
                  <input type="text" placeholder="Enter your search key ..." name="product_keyword">
                  <button type="submit" class="li-btn" name="submit"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="header-bottom_area header-sticky stick">
        <style type="text/css">
          #boxy {
            max-width: 1322px !important;
          }
          .header-bottom_area .main-menu_area > nav > ul > li > a { padding: 10px 0 !important; }                    
        </style>
        <div class="container" id="boxy">
          <div class="row">
            <div class="col-md-4 col-sm-4 d-lg-none d-block">
              <div class="header-logo">
                <a href="index">
                  <img src="/asset/images/menu/logo/mbiz.png" style="width: 90px; height: 50px" alt="Hiraola's Header Logo">
                </a>
              </div>
            </div>
            <div class="col-lg-12 d-none d-lg-block position-static" style="font-size: 13px;">
              <div class="main-menu_area">
                <nav>
                  <ul>                                       
                    <li class="active"><a href="/index">Home</a></li>     
                    @foreach($maincategory as $row)
                    <li class="megamenu-holder"><a href="#">{{$row->Mcategory_name}}</a>
                      <ul class="hm-megamenu">
                        <?php $category=MainCategory::where('category_id',$row->id)->get(); ?>
                        @foreach($category as $col)
                        <li><span class="megamenu-title">{{$col->Mcategory_name}}</span>
                          <?php $subproduct=MainCategory::where('category_id',$col->id)->get(); ?>
                          <ul>
                            @foreach($subproduct as $cols)
                            <li><a href="/product-list/{{$cols->id}}">{{$cols->Mcategory_name}}</a>
                            </li>
                            @endforeach                            
                          </ul>
                        </li>
                        @endforeach                                 
                      </ul>
                    </li>
                    @endforeach
                    <li><a href="/about">About Us</a></li>
                    <li><a href="/contact-us">Contact</a></li>
                  </ul>
                </nav>
              </div>
            </div>              
            <br>
            <div class="col-lg-12 d-none d-lg-block position-static" id="response-show" style="font-size: 13px;">
              <div class="main-menu_area">
                <style>
                  .container {
                    display: inline-block;
                    cursor: pointer;
                  }

                  .bar1, .bar2, .bar3 {
                    width: 35px;
                    border-radius: 2px;
                    height: 4px;
                    background-color: #fff;
                    margin: 5px 5px;
                    transition: 0.4s;
                  }

                  /* Rotate first bar */
                  .change .bar1 {
                    -webkit-transform: rotate(-45deg) translate(-9px, 6px) ;
                    transform: rotate(-45deg) translate(-9px, 6px) ;
                  }

                  /* Fade out the second bar */
                  .change .bar2 {
                    opacity: 0;
                  }

                  /* Rotate last bar */
                  .change .bar3 {
                    -webkit-transform: rotate(45deg) translate(-8px, -8px) ;
                    transform: rotate(45deg) translate(-8px, -8px) ;
                  }
                  .response-menu { display: none; }
                  .response-menu ul { padding:10px 5px }
                  .response-menu ul li a{ color: #fff; padding:20px 5px; font-size: 16px;font-weight: bold !important; }
                  .response-menu ul li a:hover { color: #fff !important;  text-decoration: underline !important; }
                  .response-menu ul li { padding-bottom: 10px  }
                  .response-menu ul li .hm-megamenu { display: none; }
                  .response-menu .megamenu-holder:hover  .hm-megamenu { display: block !important; background-color: #fff}
                  #subcat li a { color: orange !important; }
                  .response-menu .megamenu-title  { color: #333; font-size: 16px !important }
                </style> 

                <div class="container" onclick="myFunction(this)">
                  <br> 
                  <div class="bar1"></div>
                  <div class="bar2"></div>
                  <div class="bar3"></div><br> 


                  <div class="response-menu">

                    <nav>
                      <ul>                                       
                        <li class="active"><a href="/index">Home</a></li>     
                        @foreach($maincategory as $row)
                        <li class="megamenu-holder"><a href="#">{{$row->Mcategory_name}}</a>
                          <ul class="hm-megamenu">
                            <?php $category=MainCategory::where('category_id',$row->id)->get(); ?>
                            @foreach($category as $col)
                            <li><span class="megamenu-title">{{$col->Mcategory_name}}</span>
                              <?php $subproduct=MainCategory::where('category_id',$col->id)->get(); ?>
                              <ul id="subcat">
                                @foreach($subproduct as $cols)
                                <li><a href="/product-list/{{$cols->id}}">{{$cols->Mcategory_name}}</a>
                                </li>
                                @endforeach                            
                              </ul>
                            </li>
                            @endforeach                                 
                          </ul>
                        </li>
                        @endforeach
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/contact-us">Contact</a></li>
                      </ul>
                    </nav>
                  </div>   
                </div>                     
              </div>
            </div> 

            <script>
              function  myFunction(){
                $('.response-menu').toggle();
              }

            </script>

          </div>
        </div>
      </div>
      <div class="offcanvas-minicart_wrapper" id="miniCart">
        <div class="offcanvas-menu-inner">
          <a href="#" class="btn-close"><i class="ion-android-close"></i></a>
          <div class="minicart-content">
            <div class="minicart-heading">
              <h4>Shopping Cart</h4>
            </div>
            <ul class="minicart-list">
              @if(session()->get('cart') == null)
              <?php echo "No products in the cart"; ?>
              @else
              <?php
              echo "Something in cart";
              ?>
              @endif
              <li class="minicart-product">
                <a class="product-item_remove" href="javascript:void(0)"><i class="ion-android-close"></i></a>
                <div class="product-item_img">
                  <img src="/asset/images/product/small-size/2-2.jpg" alt="Hiraola's Product Image">
                </div>
                <div class="product-item_content">
                  <a class="product-item_title" href="#">Egg White Protien</a>
                  <span class="product-item_quantity">1 x ₹12500</span>
                </div>
              </li>
              <li class="minicart-product">
                <a class="product-item_remove" href="javascript:void(0)"><i class="ion-android-close"></i></a>
                <div class="product-item_img">
                  <img src="/asset/images/product/small-size/2-3.jpg" alt="Hiraola's Product Image">
                </div>
                <div class="product-item_content">
                  <a class="product-item_title" href="#">100% whey Protein Gold</a>
                  <span class="product-item_quantity">1 x ₹12500</span>
                </div>
              </li>
            </ul>
          </div>
          <div class="minicart-item_total">
            <span>Subtotal</span>
            <span class="ammount">₹37500</span>
          </div>
          <div class="minicart-btn_area">
            <a href="/cart" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Cart</a>
          </div>
          <div class="minicart-btn_area">
            <a href="/checkout" class="hiraola-btn hiraola-btn_dark hiraola-btn_fullwidth">Checkout</a>
          </div>
        </div>
      </div>            
    </header>

    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </div>
    @endif

    @if($message = Session::get('message'))
    <div class="alert alert-primary" style="text-align: center;">
      <p>{{ $message }}</p>
    </div>
    @endif
    <!-- Begin Hiraola's Modal Area -->
    {{-- <div class="modal fade modal-wrapper" id="exampleModalCenter"> --}}
	{{-- <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body"> --}}
				{{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button> --}}
				<div class="container-fluid">
					<div class="modal-inner-area sp-area row">
						<div class="col-lg-5 col-md-5">
							<div class="sp-img_area">
								<div class="sp-img_slider-2 slick-img-slider hiraola-slick-slider arrow-type-two" data-slick-options='{
								"slidesToShow": 1,
								"arrows": false,
								"fade": true,
								"draggable": false,
								"swipe": false,
								"asNavFor": ".sp-img_slider-nav"
							}'>
							<div class="single-slide " style="text-align: center;" >
								<img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $product->image1 }} " style="height: 400px" />
							</div>
							<div class="single-slide " style="text-align: center;">
								<img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $product->image2 }} " style="height: 400px"/>
							</div>
							<div class="single-slide "  style="text-align: center;">
								<img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $product->image3 }} " style="height: 400px" />
							</div>                          
           </div>
           <div class="sp-img_slider-nav slick-slider-nav hiraola-slick-slider arrow-type-two" data-slick-options='{
           "slidesToShow": 3,
           "asNavFor": ".sp-img_slider-2",
           "focusOnSelect": true
         }' data-slick-responsive='[
         {"breakpoint":768, "settings": {"slidesToShow": 3}},
         {"breakpoint":577, "settings": {"slidesToShow": 3}},
         {"breakpoint":481, "settings": {"slidesToShow": 2}},
         {"breakpoint":321, "settings": {"slidesToShow": 2}}
         ]'>
         <div class="single-slide" id="logo-img" >
           <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $product->image1 }} "  />
         </div>
         <div class="single-slide " id="logo-img" >
           <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $product->image2 }} "  />
         </div>
         <div class="single-slide" id="logo-img" >
           <img src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $product->image3 }} "  />
         </div>
       </div>
     </div>
   </div>
   <div class="col-xl-7 col-lg-6 col-md-6">
    <div class="sp-content">
     <div class="sp-heading">
      <h5><a href="#">{{$product->name}}</a></h5>
    </div>
    <style>
      .fa-star-of-david { color: #ccc !important; }
    </style>
    <div class="rating-box">
      <ul>
       <li> 
        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
      </li>
      
    </ul>
  </div><br>
  <?php $discount = (($product->mrp - $product->sell_price)*100)/$product->mrp;

  ?>
  <div class="price-box">
    <span class="new-price"> Rs. {{ $product->sell_price }}</span><br>

  </div>
  <div class="essential_stuff">
    <ul>
     <li>Discount : <span><?php echo round($discount,2)."%"?></span></li><br>
     {{-- <li>Price in reward points:<span>400</span></li> --}}
   </ul>
 </div>
 <div class="list-item">
  <h5>Description</h5>
  <p><?php echo htmlspecialchars_decode($product->short_descriptions) ?></p>
</div><br>
<div class="list-item last-child">
  <ul>
   <li>Brand<a href="javascript:void(0)">Buxton</a></li>
   <li>Product Code: Product 15</li>
   <li>Reward Points: 100</li>
   <li>Availability: In Stock</li>
 </ul>
</div><br>
     {{-- <div class="color-list_area">
        <div class="color-list_heading">
         <h4>Available Options</h4>
     </div>
     <span class="sub-title">Color</span>
     <div class="color-list">
         <a href="javascript:void(0)" class="single-color active" data-swatch-color="red">
          <span class="bg-red_color"></span>
          <span class="color-text">Red (+£3.61)</span>
      </a>
      <a href="javascript:void(0)" class="single-color" data-swatch-color="orange">
          <span class="burnt-orange_color"></span>
          <span class="color-text">Orange (+£2.71)</span>
      </a>
      <a href="javascript:void(0)" class="single-color" data-swatch-color="brown">
          <span class="brown_color"></span>
          <span class="color-text">Brown (+£0.90)</span>
      </a>
      <a href="javascript:void(0)" class="single-color" data-swatch-color="umber">
          <span class="raw-umber_color"></span>
          <span class="color-text">Umber (+£1.81)</span>
      </a>
  </div>
</div> --}}
<div class="quantity">
  <label>Quantity</label>
  <div class="cart-plus-minus">
   <input class="cart-plus-minus-box" value="1" type="text">
   <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
   <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
 </div>
</div>
<div class="hiraola-group_btn">
  <ul>
   <li><button  onclick="addCart({{$product->id}})" class="btn btn-primary" style="background-color: orange; border: none; border-radius: 4px">Add To Cart<button></li>
     <li><a href="#"><i class="ion-android-favorite-outline"></i></a></li>
     <li><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
   </ul>
 </div>
 <div class="hiraola-tag-line">
  <h6>Tags:</h6>
  <a href="javascript:void(0)">Ring</a>,
  <a href="javascript:void(0)">Necklaces</a>,
  <a href="javascript:void(0)">Braid</a>
</div>
<div class="hiraola-social_link">
  <ul>
   <li class="facebook">
    <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
     <i class="fab fa-facebook"></i>
   </a>
 </li>
 <li class="twitter">
  <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
   <i class="fab fa-twitter-square"></i>
 </a>
</li>
<li class="youtube">
  <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
   <i class="fab fa-youtube"></i>
 </a>
</li>
<li class="google-plus">
  <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
   <i class="fab fa-google-plus"></i>
 </a>
</li>
<li class="instagram">
  <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
   <i class="fab fa-instagram"></i>
 </a>
</li>
</ul><br>
</div>
</div>
</div>
</div>
</div>
<br><br>
<hr style="width: 90%; color: #ccc">
{{-- </div>
</div>
</div> --}}
{{-- </div> --}}
<!-- Hiraola's Modal Area End Here -->

<style> 
  .nav-pills li a{ padding: 10px; font-size: 16px}
  /*.nav-pills li a:active { background-color: orange !important; color: #fff !important; }*/
  /*#desc p { padding : 20px; }*/
  #desc, #add, #rev  { background-color: #f1f3f4 }
  #add .table { width: 90%; margin: 1% 5%; background-color: #fff}
  .review-content { margin: 1% 3%; }
  #add { margin-top: 2% }
  .nav-pills .active { display: block;background-color: #f1f3f4; padding: 0 10px }
  #exTab1 { margin-top: 30px; }
</style>


{{-- DEscription Area --}}
<div id="exTab1" class="container"> 
  <ul  class="nav nav-pills">
    <li class="active">
      <a  href="#desc" data-toggle="tab">Description</a>
    </li>
    <li><a href="#add" data-toggle="tab">Additional Information</a>
    </li>
    <li><a href="#rev" data-toggle="tab">Reviews(0)</a>
    </li>
    
  </ul>

  <div class="tab-content clearfix">
    <div class="tab-pane active" id="desc">
      <div style="margin: 2% 3%">
        <?php echo htmlspecialchars_decode($product->long_descriptions) ?><br>
      </div>

    </div>
    <div class="tab-pane" id="add"><br>
      <table class="table">
        <tr>
          <th style="width: 20%">Weight</th>
          <td style="width: 80%">5 kg</td>
        </tr>
        <tr>
          <th style="width: 20%">Dimensions</th>
          <td style="width: 80%">122 × 121 × 451 cm</td>
        </tr>
        <tr>
          <th style="width: 20%">Flavor</th>
          <td style="width: 80%">Chocolate, Vanilla</td>
        </tr>
      </table><br>
    </div><br>
    <div class="tab-pane" id="rev">
      <div class="review-content"><br>
        <style>
          #review { display: block;background-color: #fff; border-radius: 5px }
          #review h6 ,#review p { padding:10px 20px 0px 20px; }
        </style>
        <?php
        $review = Review::where('product_id',$product->id)->count();  
        $i=1;        
        ?>
        @if($review>0)
        <?php
        $review = Review::where('product_id',$product->id)->get();

        ?>
        <h4>&nbsp;&nbsp;&nbsp;&nbsp;Reviews</h4><br>
        @foreach($review as $row)
        <div class="container" > 

          
          <div id="review">
            <h6>{{ $i++ }}.&nbsp;&nbsp;{{ $row->name }}</h6>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;{{ $row->review }}</p>
          </div>
        </div><br><br>
        
        @endforeach


        @else
        <p>
        There are no reviews yet.</p>

        <h6>Be the first to review “100% WHEY PROTEIN GOLD Net Wt. 2.5 KG”</h6>
        <p>Your email address will not be published. Required fields are marked *</p>
        @endif

        <h5>Add Your Review</h5>
        <form action="/product-detail/{{ $product->id }}" method="POST">
          <div class="form-group">
            <label>Name *</label>
            <input type="text" name="name" id="name" class="form-control"> 
          </div>

          <div class="form-group">
            <label>Email *</label>
            <input type="text" name="email" id="email" class="form-control"> 
          </div><br>
          <div class="checkbox">
            <label><input type="checkbox" id="save" name="save" value="yes">&nbsp;&nbsp;&nbsp;&nbsp;Save my name, email, and website in this browser for the next time I comment.</label>
          </div><br>
          <div class="form-group">
            <style>
              .show-result {
                margin: 10px;
                padding: 10px;
                color: green;
                font-size: 20px;
              }

              .star s:hover,
              .star s.active {
                color: orange;
              }
              .star-rtl s:hover,
              .star-rtl s.active {
                color: orange;
              }

              .star s,
              .star-rtl s {
                color: black;
                font-size: 50px;
                cursor: default;
                text-decoration: none;
                line-height: 50px;
              }
              .star {
                padding: 2px;
              }
              .star-rtl {
                background: #555;
                display: inline-block;
                border: 2px solid #444;
              }
              .star-rtl s {
                color: yellow;
              }
              .star s:hover:before,
              .star s.rated:before,
              .star s.active:before {
                content: "\2605";
                font-size: 32px;
              }
              .star s:before {
                content: "\2606";
                font-size: 32px;
              }
              .star-rtl s:hover:after,
              .star-rtl s.rated:after,
              .star-rtl s.active:after {
                content: "\2605";
                font-size: 32px;
              }

              .star-rtl s:after {
                content: "\2606";
              }
            </style>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

            <script>
              $(function() {
                $("div.star > s").on("click", function(e) {

    // remove all active classes first, needed if user clicks multiple times
    $(this).closest('div').find('.active').removeClass('active');

    $(e.target).parentsUntil("div").addClass('active'); // all elements up from the clicked one excluding self
    $(e.target).addClass('active');  // the element user has clicked on


    var numStars = $(e.target).parentsUntil("div").length+1;
    $('.show-result input').val(numStars );
  });
              });
            </script>
            <label for="rating">Rating</label><br>
            <div class="star"><s><s><s><s><s></s></s></s></s></s></div>
            <div class="show-result">
              <input type="hidden" name="rating" id="rating" >
            </div>


{{-- <div class="show-result">No stars selected yet.</div>
 <label for="comment">Rating</label><br>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span> --}}
</div>
<br>
<style>
  .checked {
    color: orange;
  }
</style>
<div class="form-group">
  <label for="comment">Comment:</label>
  <textarea class="form-control" rows="5" id="comment" name="review"></textarea>
</div><br>

<button type="submit" class="btn btn-success" style="background-color: orange; border:none;" name="submit" value="submit">Submit</button>
</form>
<br><br>
</div>
</div>

</div>
</div>



{{-- End of Description Area --}}


<style type="text/css"> 
  .shipping-content h6{color: white;}
  .shipping-content p{color: white;}

</style>

<!-- footer here -->

<!-- Begin Hiraola's Shipping Area -->
<div class="hiraola-shipping_area" style="background-color: #333">
  <div class="container">
    <div class="shipping-nav">
      <div class="row">
        <div class="col-lg-3 col-md-6">
          <div class="shipping-item">
            <div class="shipping-icon">
              <img src="/asset/images/shipping-icon/1.png" alt="Hiraola's Shipping Icon">
            </div>
            <div class="shipping-content">
              <h6>30 DAYS RETURN</h6>
              <p>money back</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="shipping-item">
            <div class="shipping-icon">
              <img src="/asset/images/shipping-icon/2.png" alt="Hiraola's Shipping Icon">
            </div>
            <div class="shipping-content">
              <h6>FREE SHIPPING</h6>
              <p>on all orders over 12500</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="shipping-item">
            <div class="shipping-icon">
              <img src="/asset/images/shipping-icon/3.png" alt="Hiraola's Shipping Icon">
            </div>
            <div class="shipping-content">
              <h6>LOWEST PRICE</h6>
              <p>guarantee</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6">
          <div class="shipping-item">
            <div class="shipping-icon">
              <img src="/asset/images/shipping-icon/4.png" alt="Hiraola's Shipping Icon">
            </div>
            <div class="shipping-content">
              <h6>SAFE SHOPPING</h6>
              <p>guarantee 100%</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Hiraola's Shipping Area End Here -->


<!-- Begin Hiraola's Footer Area -->
<div class="hiraola-footer_area">
  <div class="footer-top_area">
    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="footer-widgets_info">

            <div class="footer-widgets_logo">
              <a href="/index">
                <img src="/asset/images/menu/logo/mbiz.png" alt="Hiraola's Footer Logo" style="width: 180px; height: 80px">
              </a>
            </div>


                               <!--  <div class="widget-short_desc">
                                    <p>We are a team of designers and developers that create high quality HTML Template & Woocommerce, Shopify Theme.
                                    </p>
                                  </div> -->
                                  <div class="hiraola-social_link">
                                    <ul>
                                      <li class="facebook">
                                        <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                          <i class="fab fa-facebook"></i>
                                        </a>
                                      </li>
                                      <li class="twitter">
                                        <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                          <i class="fab fa-twitter-square"></i>
                                        </a>
                                      </li>
                                      <li class="google-plus">
                                        <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                          <i class="fab fa-google-plus"></i>
                                        </a>
                                      </li>
                                      <li class="instagram">
                                        <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                          <i class="fab fa-instagram"></i>
                                        </a>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                              <div class="col-lg-8">
                                <div class="footer-widgets_area">
                                  <div class="row">
                                    <div class="col-lg-3">
                                      <div class="footer-widgets_title">
                                        <h6>Product</h6>
                                      </div>
                                      <div class="footer-widgets">
                                        <ul>
                                          <li><a href="#">Prices drop</a></li>
                                          <li><a href="#">New products</a></li>
                                          <li><a href="#">Best sales</a></li>
                                          <li><a href="/contact-us">Contact us</a></li>
                                        </ul>
                                      </div>
                                    </div>
                                    <div class="col-lg-5">
                                      <div class="footer-widgets_info">
                                        <div class="footer-widgets_title">
                                          <h6>About Us</h6>
                                        </div>
                                        <div class="widgets-essential_stuff">
                                          <ul>
                                            <li class="hiraola-address"><i class="ion-ios-location"></i><span>Address:</span> Delhi 59</li>
                                            <li class="hiraola-phone"><i class="ion-ios-telephone"></i><span>Call Us:</span> <a href="tel://180012345566">1800-1234-5566</a>
                                            </li>
                                            <li class="hiraola-email"><i class="ion-android-mail"></i><span>Email:</span> <a href="mailto://info@m-biz.in">info@m-biz.in</a></li>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-4">
                                      <div class="instagram-container footer-widgets_area">
                                        <div class="footer-widgets_title">
                                          <h6>Sign Up For Newslatter</h6>
                                        </div>
                                        <div class="widget-short_desc">
                                          <p>Subscribe to our newsletters now and stay up-to-date with new collections</p>
                                        </div>
                                        <div class="newsletter-form_wrap">
                                          <form action="http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="newsletters-form validate" target="_blank" novalidate>
                                            <div id="mc_embed_signup_scroll">
                                              <div id="mc-form" class="mc-form subscribe-form">
                                                <input id="mc-email" class="newsletter-input" type="email" autocomplete="off" placeholder="Enter your email" />
                                                <button class="newsletter-btn" id="mc-submit">
                                                  <i class="ion-android-mail" aria-hidden="true"></i>
                                                </button>
                                              </div>
                                            </div>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="footer-bottom_area">
                          <div class="container">
                            <div class="footer-bottom_nav">
                              <div class="row">
                                <div class="col-lg-12">
                                  <div class="footer-links">
                                    <ul>
                                        <!-- <li><a href="#">Online Shopping</a></li>
                                        <li><a href="#">Promotions</a></li>
                                        <li><a href="#">My Orders</a></li>
                                        <li><a href="#">Help</a></li>
                                        <li><a href="#">Customer Service</a></li>
                                        <li><a href="#">Support</a></li>
                                        <li><a href="#">Most Populars</a></li>
                                        <li><a href="#">New Arrivals</a></li>
                                        <li><a href="#">Special Products</a></li>
                                        <li><a href="#">Manufacturers</a></li>
                                        <li><a href="#">Our Stores</a></li>
                                        <li><a href="#">Shipping</a></li>
                                        <li><a href="#">Payments</a></li>
                                        <li><a href="#">Warantee</a></li>
                                        <li><a href="#">Refunds</a></li>
                                        <li><a href="#">Checkout</a></li>
                                        <li><a href="#">Discount</a></li> -->
                                        <li><a href="returnpolicy">Return Policy</a></li>
                                        <li><a href="privacypolicy">Privacy Policy</a></li>
                                        <li><a href="tnc">Terms and Conditions</a></li>
                                      </ul>
                                    </div>
                                  </div>
                                  <div class="col-lg-12">
                                    <div class="payment">
                                      <a href="#">
                                        <img src="/asset/images/footer/payment/1.png" alt="Hiraola's Payment Method">
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-lg-12">
                                    <div class="copyright">
                                      <span>Copyright &copy; 2019 <a href="#">m-Biz.</a> All rights reserved.</span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
    <!-- JS
      ============================================ -->

      <!-- jQuery JS -->
      <script src="/asset/js/vendor/jquery-1.12.4.min.js"></script>
      <!-- Modernizer JS -->
      <script src="/asset/js/vendor/modernizr-2.8.3.min.js"></script>
      <!-- Popper JS -->
      <script src="/asset/js/vendor/popper.min.js"></script>
      <!-- Bootstrap JS -->
      <script src="/asset/js/vendor/bootstrap.min.js"></script>
      <!-- Slick Slider JS -->
      <script src="/asset/js/plugins/slick.min.js"></script>
      <!-- Countdown JS -->
      <script src="/asset/js/plugins/countdown.js"></script>
      <!-- Barrating JS -->
      <script src="/asset/js/plugins/jquery.barrating.min.js"></script>
      <!-- Counterup JS -->
      <script src="/asset/js/plugins/jquery.counterup.js"></script>
      <!-- Nice Select JS -->
      <script src="/asset/js/plugins/jquery.nice-select.js"></script>
      <!-- Sticky Sidebar JS -->
      <script src="/asset/js/plugins/jquery.sticky-sidebar.js"></script>
      <!-- Jquery-ui JS -->
      <script src="/asset/js/plugins/jquery-ui.min.js"></script>
      <script src="/asset/js/plugins/jquery.ui.touch-punch.min.js"></script>
      <!-- Lightgallery JS -->
      <script src="/asset/js/plugins/lightgallery.min.js"></script>
      <!-- Scroll Top JS -->
      <script src="/asset/js/plugins/scroll-top.js"></script>
      <!-- Theia Sticky Sidebar JS -->
      <script src="/asset/js/plugins/theia-sticky-sidebar.min.js"></script>
      <!-- Waypoints JS -->
      <script src="/asset/js/plugins/waypoints.min.js"></script>
      <!-- Instafeed JS -->
      <script src="/asset/js/plugins/instafeed.min.js"></script>
      <!-- Instafeed JS -->
      <script src="/asset/js/plugins/jquery.elevateZoom-3.0.8.min.js"></script>

      <!-- Vendor & Plugins JS (Please remove the comment from below vendor.min.js & plugins.min.js for better website load performance and remove js files from above) -->
    <!--
<script src="/asset/js/vendor/vendor.min.js"></script>
<script src="/asset/js/plugins/plugins.min.js"></script>
-->

<!-- Main JS -->
<script src="/asset/js/main.js"></script>



<script>
  function addCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
      /* the route pointing to the post function */
      url: '/product-detail/{id}/add-to-cart',
      type: 'POST',
      /* send the csrf-token and the input to the controller */
      dataType: 'JSON',
      data: {_token: CSRF_TOKEN, id: id},
      success: function (data) { 
        $("#ajaxdata").append(html);
      }
    }); 
  }

</script>




</body>

<!-- Mirrored from demo.hasthemes.com/hiraola-preview/hiraola/index by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Apr 2019 05:41:04 GMT -->
</html>