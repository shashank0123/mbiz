@extends('layouts/ecommerce')

@section('content')


<?php 
use App\MainCategory;
use App\Product;
use App\Review;

$products = Product::all();
$count = count($product);



?>


<style>
    .quick-view-btn { background-color: #333333; }
    .quick-view-btn:hover { background-color: orange; }
    .fa-star-of-david { color: #ccc !important; 
    </style>


    <!-- Begin Hiraola's Content Wrapper Area -->
    <div class="hiraola-content_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 order-2 order-lg-1">
                    <div class="hiraola-sidebar-catagories_area">
                        <div class="hiraola-sidebar_categories">
                            <div class="hiraola-categories_title">
                                <h5>Price </h5>                            
                            </div>
                            <div class="price">
                                <input type="text" id="sell_price" name="sell_price" class="form-control" placeholder="Add Your Price" /><br>
                                <button type="button" class="btn btn-primary: none" style="background-color: orange; border; color: #fff">Filter</button>
                            {{-- <div id="slider-range"></div>
                            <div class="price-slider-amount">
                                <div class="label-input">
                                    <label>price : </label>
                                    <input type="text" id="amount" name="price" placeholder="Add Your Price" />
                                </div>
                                <button type="button">Filter</button>
                            </div> --}}
                        </div>
                    </div>
                    <style>
                        .checkbox label  { font-size: 16px !important }
                    </style>

                    {{-- <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Category</h5>
                        </div>
                        <style>
                           .postbutton { margin-top: 10px }
                        </style>
                        <div class="sidebar-checkbox_list"  style="height: 200px; overflow: auto;">
                            
                            @foreach($product as $row)
                            <div style="display: block;" class="postbutton" onclick="callAjax('{{ $row->id }}')">{{ $row->name }}</div>
                                @endforeach  
                                                       
                        </div >
                    </div> --}}
                    <div class="hiraola-sidebar_categories">
                        <div class="hiraola-categories_title">
                            <h5>Rating</h5>
                        </div>
                        <ul class="sidebar-checkbox_list">
                            <select name="rating" class="form-control" size="5">
                                <option value="1" id="rate1">1</option>
                                <option value="2" id="rate2">2</option>
                                <option value="3" id="rate3">3</option>
                                <option value="4" id="rate4">4</option>
                                <option value="5" id="rate5">5</option>
                            </select>
                        </ul>
                    </div>
                    
                    
                </div>
                <div class="sidebar-banner_area">
                    <div class="banner-item img-hover_effect">
                        <a href="javascript:void(0)">
                            <img src="/asset/images/banner/1_1.jpg" alt="Hiraola's Shop Banner Image">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 order-1 order-lg-2">
                <div class="shop-toolbar">
                    <div class="product-view-mode">
                        <a class="active grid-3" data-target="gridview-3" data-toggle="tooltip" data-placement="top" title="Grid View"><i class="fa fa-th"></i></a>
                        <a class="list" data-target="listview" data-toggle="tooltip" data-placement="top" title="List View"><i class="fa fa-th-list"></i></a>
                    </div>
                    <div class="product-item-selection_area">
                        <div class="product-short">
                            <label class="select-label">Sort By:</label>
                            <select class="nice-select">
                                <option value="1">Relevance</option>
                                <option value="2">Name, A to Z</option>
                                <option value="3">Name, Z to A</option>
                                <option value="4">Price, low to high</option>
                                <option value="5">Price, high to low</option>
                                <option value="6">Rating (Highest)</option>
                                <option value="7">Rating (Lowest)</option>                      
                            </select>
                        </div>
                    </div>
                </div>
                <div class="shop-product-wrap grid gridview-3 row">
                    @if($count>0)
                    @foreach($product as $row)
                    <?php 
                    $rate = Review::where('product_id',$row->id)->avg('rating');
                    $final_rate = floor($rate);
                    $fnl_rt = $final_rate;
                    ?>
                    <div class="col-lg-4">
                        <div class="slide-item">
                            <div class="single_product">
                                <div class="product-img">
                                    <a>
                                        <img class="primary-img" src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image1 }}" alt="Hiraola's Product Image">
                                        <img class="secondary-img" src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image2 }}" alt="Hiraola's Product Image">
                                    </a>
                                    <div class="add-actions">
                                        <ul>
                                            <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                <button  onclick="addCart({{$row->id}})" id="">
                                                    <a class="hiraola-add_cart" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-bag" style="color: #fff"></i></a>
                                                </button>
                                            </li>

                                            <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">
                                                <button  onclick="openview({{ $row->id }},'{{ $row->name }}',{{$row->sell_price}},{{ $row->mrp }},'{{$row->short_descriptions}}','{{$row->image1}}','{{$row->image2}}','{{$row->image3}}',<?php echo $final_rate; ?>)">
                                                    <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                        <i
                                                        class="ion-eye" style="color: #fff; "></i>
                                                    </a>
                                                </button></li>
                                            </ul>
                                        </div>
                                    </div></a>
                                    <div class="hiraola-product_content">
                                        <div class="product-desc_info">
                                            <h6><a class="product-name" href="/product-detail/{{$row->id}}">{{$row->name}}</a></h6>
                                            <div class="price-box">
                                                <span class="new-price">Rs.{{$row->sell_price}}</span>
                                            </div>
                                            <div class="additional-add_action">
                                                <ul>

                                                    <li class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center; ">

                                                        <button  onclick="" id="addWishList">
                                                            <a class="hiraola-add_cart" href="#" data-toggle="tooltip" data-placement="top" title="Add To Cart"><i class="ion-android-favorite-outline" style="color: #fff"></i></a>
                                                        </button>

                                                    </li>


                                                </ul>
                                            </div>
                                            <div class="rating-box">
                                                <ul>
                                                    <li> 
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                        <i class="fa <?php if($final_rate>0){ echo "fa-star"; $final_rate--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-slide_item">
                                <div class="single_product">
                                    <div class="product-img">
                                        <a href="#">
                                           <img class="primary-img" src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image1 }}" alt="Hiraola's Product Image">
                                           <img class="secondary-img" src="{{ URL::to('/') }}/asset/images/AdminProduct/{{ $row->image2 }}" alt="Hiraola's Product Image">
                                       </a>
                                   </div>
                                   <div class="hiraola-product_content">
                                    <div class="product-desc_info">
                                        <h6><a class="product-name" href="/product-detail/{{$row->id}}">{{$row->name}}</a></h6>
                                        <div class="rating-box">
                                            <ul>
                                                <li> 
                                                    <i class="fa <?php if($fnl_rt>0){ echo "fa-star"; $fnl_rt--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                    <i class="fa <?php if($fnl_rt>0){ echo "fa-star"; $fnl_rt--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                    <i class="fa <?php if($fnl_rt>0){ echo "fa-star"; $fnl_rt--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                    <i class="fa <?php if($fnl_rt>0){ echo "fa-star"; $fnl_rt--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                    <i class="fa <?php if($fnl_rt>0){ echo "fa-star"; $fnl_rt--;  } else { echo "fa-star-of-david"; } ?>"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="price-box">
                                            <span class="new-price">Rs. {{ $row->sell_price }}</span>
                                        </div>
                                        <div class="product-short_desc">
                                            <p><?php echo htmlspecialchars_decode($row->long_descriptions)?></p>
                                        </div>
                                    </div>
                                    <div class="add-actions">
                                        {{-- <ul>
                                            <li><a class="hiraola-add_cart" href="/cart" data-toggle="tooltip" data-placement="top" title="Add To Cart">Add To Cart</a></li>                            
                                            <li  class="quick-view-btn" style="display:block; width: 30px ; height: 30px; text-align: center;">
                                                <button  onclick="openview({{ $row->id }},'{{ $row->name }}',{{$row->sell_price}},{{ $row->mrp }},'{{$row->short_descriptions}}','{{$row->image1}}','{{$row->image2}}','{{$row->image3}}')">
                                                    <a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="Quick View" >
                                                        <i
                                                        class="ion-eye" style="color: #fff; "></i>
                                                    </a>
                                                </button></li>
                                                <li><a class="hiraola-add_compare" href="wishlist.html" data-toggle="tooltip" data-placement="top" title="Add To Wishlist"><i
                                                    class="ion-android-favorite-outline"></i></a>
                                                </li>
                                            </ul> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                            {{-- <div class="row">
                                <div class="col-lg-12">
                                    <div class="hiraola-paginatoin-area">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <ul class="hiraola-pagination-box">
                                                    <li class="active"><a href="javascript:void(0)">1</a></li>
                                                    <li><a href="javascript:void(0)">2</a></li>
                                                    <li><a href="javascript:void(0)">3</a></li>
                                                    <li><a class="Next" href="javascript:void(0)"><i
                                                        class="ion-ios-arrow-right"></i></a></li>
                                                        <li><a class="Next" href="javascript:void(0)">>|</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="product-select-box">
                                                        <div class="product-short">
                                                            <p>Showing 1 to 12 of 18 (2 Pages)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}

                                @else
                                <div class="container"  style="text-align: center;">
                                    <div class="row" style="margin-top: 10%; ">
                                        <h3 style="color: orange; margin-left: 15%; font-size: 48px">Sorry, No Product is Available for this Category</h3> 
                                    </div>
                                </div>  
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hiraola's Content Wrapper Area End Here -->
                @endsection



                <div class="modal fade modal-wrapper" id="viewproduct" tabindex="-1" role="dialog" aria-labelledby="viewproductLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            {{-- <input type="text" name="id" id="viewid"> --}}
                            <?php $prod = Product::where('id','id')?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="modal-inner-area sp-area row">
                                <div class="col-lg-5 col-md-5">
                                    <div class="sp-img_area">
                                        <div class="sp-img_slider-2 slick-img-slider hiraola-slick-slider arrow-type-two" data-slick-options='{
                                        "slidesToShow": 1,
                                        "arrows": false,
                                        "fade": true,
                                        "draggable": false,
                                        "swipe": false,
                                        "asNavFor": ".sp-img_slider-nav"
                                    }'>
                                    <div class="single-slide " >
                                        <img src=""  id="img1" style="width: 100%; height: 350px"/>
                                    </div>
                                    <div class="single-slide " >
                                        <img src=""  id="img2" style="width: 100%; height: 350px"/>
                                    </div>
                                    <div class="single-slide ">
                                        <img src=""  id="img3" style="width: 100%; height: 350px"/>
                                    </div>
                                            {{-- <div class="single-slide umber">
                                                <img src="asset/images/single-product/large-size/4.jpg" alt="Hiraola's Product Image">
                                            </div> --}}
                                        </div>
                                        <div class="sp-img_slider-nav slick-slider-nav hiraola-slick-slider arrow-type-two" data-slick-options='{
                                        "slidesToShow": 3,
                                        "asNavFor": ".sp-img_slider-2",
                                        "focusOnSelect": true
                                    }' data-slick-responsive='[
                                    {"breakpoint":768, "settings": {"slidesToShow": 3}},
                                    {"breakpoint":577, "settings": {"slidesToShow": 3}},
                                    {"breakpoint":481, "settings": {"slidesToShow": 2}},
                                    {"breakpoint":321, "settings": {"slidesToShow": 2}}
                                    ]'>
                                    <div class="single-slide red">
                                        <img src="" class="logo-img" id="imgt1"/>
                                    </div>
                                    <div class="single-slide orange">
                                        <img src="" class="logo-img" id="imgt2"/>
                                    </div>
                                    <div class="single-slide brown">
                                        <img src="" class="logo-img" id="imgt3"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <div class="sp-content">
                                <div class="sp-heading">
                                    <h5 id="viewname"></h5>
                                </div>
                                <div class="rating-box">
                                    <ul>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                        <li><i class="fa fa-star-of-david"></i></li>
                                    </ul>
                                </div>
                                <div class="price-box">
                                    <h3>Rs.
                                        <span class="new-price" id="viewprice"></span></h3>
                                        {{--  <span class="old-price">£93.68</span> --}}
                                    </div>
                                    <div class="essential_stuff">
                                        <ul>
                                            <li>Discount : <span id='viewdiscount'></span><span>%</span></li>
                                        </ul>
                                    </div>
                                    <div class="list-item">
                                        <p id="viewdescription"></p>
                                    {{-- <ul>
                                        <li>10 or more £81.03</li>
                                        <li>20 or more £71.09</li>
                                        <li>30 or more £61.15</li>
                                    </ul> --}}
                                </div>
                                {{-- <div class="list-item last-child">
                                    <p id="viewdescription"></p>
                                </div> --}}
                                <div class="color-list_area">
                                    <div class="color-list_heading">
                                        <h4>Available Options</h4>
                                    </div>
                                    <span class="sub-title">Color</span>
                                    <div class="color-list">
                                        <a href="javascript:void(0)" class="single-color active" data-swatch-color="red">
                                            <span class="bg-red_color"></span>
                                            <span class="color-text">Red (+£3.61)</span>
                                        </a>
                                        <a href="javascript:void(0)" class="single-color" data-swatch-color="orange">
                                            <span class="burnt-orange_color"></span>
                                            <span class="color-text">Orange (+£2.71)</span>
                                        </a>
                                        <a href="javascript:void(0)" class="single-color" data-swatch-color="brown">
                                            <span class="brown_color"></span>
                                            <span class="color-text">Brown (+£0.90)</span>
                                        </a>
                                        <a href="javascript:void(0)" class="single-color" data-swatch-color="umber">
                                            <span class="raw-umber_color"></span>
                                            <span class="color-text">Umber (+£1.81)</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="quantity">
                                    <label>Quantity</label>
                                    <div class="cart-plus-minus">
                                        <input class="cart-plus-minus-box" value="1" type="text">
                                        <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                        <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                    </div>
                                </div>
                                <div class="hiraola-group_btn">
                                    <ul>
                                        <li><a href="#" class="add-to_cart">Cart To Cart</a></li>
                                        <li style="margin-top: -30px;"><a href="#" ><i class="ion-android-favorite-outline"></i></a></li>
                                        <li style="margin-top: -30px;"><a href="#"><i class="ion-ios-shuffle-strong"></i></a></li>
                                    </ul>
                                </div>
                                <div class="hiraola-tag-line">
                                    <h6>Tags:</h6>
                                    <a href="javascript:void(0)">Ring</a>,
                                    <a href="javascript:void(0)">Necklaces</a>,
                                    <a href="javascript:void(0)">Braid</a>
                                </div>
                                <div class="hiraola-social_link">
                                    <ul>
                                        <li class="facebook">
                                            <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                                <i class="fab fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery JS -->
    <script src="/asset/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="/asset/js/plugins/jquery-ui.min.js"></script>
    

    <script>
        function openview(id,name,price,mrp,description,img1,img2,img3){
         // alert(img1+img2+img3);
    // $('#viewid').val(id);
    $('#viewname').text(name);

    // alert(description);
    $('#viewdescription').text(description);
    $('#viewprice').text(price);
    $("#img1").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/asset/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewproduct').modal('show');
} 


function addCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        /* the route pointing to the post function */
        url: '/product-detail/{id}/add-to-cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) { 
            $("#ajaxdata").append(html);
        }
    }); 
}



</script>


