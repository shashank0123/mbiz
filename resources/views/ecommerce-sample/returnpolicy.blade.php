@extends('layouts/ecommerce')

@section('content')
     
    <!-- Hiraola's Header Main Area End Here -->
  <!-- policy content -->
  <style type="text/css">
  .textpolicy{ background-color: white; margin-top: 20px; margin-bottom: 100px; }
  .textpolicy p{font-weight: 100px; padding: 0px 30px  0px 30px ; }
 .textpolicy h4{ text-align: center; margin-top: 20px; margin-bottom: 10px; }
.textpolicy h5{padding: 0px 30px  0px 30px ;
}
  
</style>
<div class="container-fluid" style="background-color:#F1F3F6;">
  <div class="row">
    
 <div class="col-sm-2"></div> 
 <div  class="col-sm-8 textpolicy">
   
   <h4> m-Biz Return Policy </h4>
   <hr>
   <p> Returns is a scheme provided by respective sellers directly under this policy in terms of which the option of exchange, replacement and/ or refund is offered by the respective sellers to you. All products listed under a particular category may not have the same returns policy. For all products, the policy on the product page shall prevail over the general returns policy. Do refer the respective item's applicable return policy on the product page for any exceptions to the table below.
<br><br>
The return policy is divided into three parts; Do read all sections carefully to understand the conditions and cases under which returns will be accepted.</p>

   <div class="container">
  
  <p>Part 1 – Category, Return Window and Actions possible:</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Category</th>
        
        <th>Returns Window, Actions Possible and Conditions (if any)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
       
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
       
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
       
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
</div>
<h5>2. Links to Other Sites</h5>
<ol>
    <li>belongs to another person and to which You does not have any right to;</li>
    <li>is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever; or unlawfully threatening or unlawfully harassing including but not limited to "indecent representation of women" within the meaning of the Indecent Representation of Women (Prohibition) Act, 1986;</li>
    <li> is misleading in any way;</li>
    <li>is patently offensive to the online community, such as sexually explicit content, or content that promotes obscenity, paedophilia, racism, bigotry, hatred or physical harm of any kind against any group or individual;</li>
    <li> harasses or advocates harassment of another person;<li>
    <li>involves the transmission of "junk mail", "chain letters", or unsolicited mass mailing or "spamming";</li>
    </li>involves the transmission of "junk mail", "chain letters", or unsolicited mass mailing or "spamming";</li>

</ol>   







 </div> 


 <div class="col-sm-2"></div>      
</div>
</div>


    @endsection