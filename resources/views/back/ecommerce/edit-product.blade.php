@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
	#getSize input { margin-left: 20px }
</style>

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">


	<a href="/admin/product"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}">
							<label for="name">Product</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($categoryarray as $data)
								<option value="{{ $data->id}}" {{ $product->category_id==$data->id? 'selected': null }}>{{ $data->Mcategory_name  }}</option>
								@endforeach
								
							</select>



							{{-- <select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($categoryarray as $data)
								<option value="{{ $data->id}}" {{ $product->category_id==$data->id? 'selected': null }}>{{ $data->Mcategory_name  }}</option>
								@endforeach
							</select> --}}
							<label for="email">Category</label>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<div class="form-control" id="getSize" style="margin-left: 10% ; width: 90%">

								<?php								
								$productArr = explode(",",$product->product_size); 
								?>

							<input type="checkbox" name="product_size[]" <?php if(in_array('S',$productArr)){ echo 'checked'; }?> value="S">S
							<input type="checkbox" <?php if(in_array('M',$productArr)){ echo 'checked'; }?> name="product_size[]" value="M">M
							<input type="checkbox" <?php if(in_array('L',$productArr)){ echo 'checked'; }?> name="product_size[]" value="L">L
							<input type="checkbox" <?php if(in_array('XL',$productArr)){ echo 'checked'; }?> name="product_size[]" value="XL">XL
							<input type="checkbox" <?php if(in_array('XXL',$productArr  )){ echo 'checked'; }?> name="product_size[]" value="XXL">XXL
						</div>
						<label for="text">Size</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<input type="text" class="form-control" id="product_color" name="product_color" value="{{ $product->product_color }}">
						<label for="text">Color</label>
					</div>
				</div>
			</div>

			{{-- Row 2 --}}
			<div class="form-group row">
				<div class="col-md-6">
					<div class="form-material floating">
						<input type="text" class="form-control" id="short_descriptions" name="short_descriptions" value="{{ $product->short_descriptions }}">
						<label for="text">Short Description</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<input type="number" class="form-control" id="mrp" name="mrp" value="{{ $product->mrp }}">
						<label for="text">MRP</label>
					</div>
				</div>
			</div>
			{{-- Row 3 --}}
			<div class="form-group row">					
				<div class="col-md-6">
					<div class="form-material floating">
						<input type="number" class="form-control" id="sell_price" name="sell_price" value="{{ $product->sell_price }}">
						<label for="text">Selling Price</label>
					</div>
				</div>
				<div class="col-md-6 from-inline">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-material floating">

								<input type="file" class="form-control" id="image1" name="image1" style="margin-left: 25% ; width: 75%">
								<label for="text">Image 1</label>

							</div>
						</div>
						<div class="col-sm-4">
							<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " class="logo-img">
						</div>
					</div>
				</div>
			</div>
			{{-- Row 4 --}}
			<div class="form-group row">					
				<div class="col-md-6">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-material floating">

								<input type="file" class="form-control" id="image2" name="image2" style="margin-left: 25% ; width: 75%">
								<label for="text">Image 2</label>

							</div>
						</div>
						<div class="col-sm-4">
							<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image2 }} " class="logo-img">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-sm-8">
							<div class="form-material floating">

								<input type="file" class="form-control" id="image3" name="image3" style="margin-left: 25% ; width: 75%">
								<label for="text">Image 3</label>

							</div>
						</div>
						<div class="col-sm-4">
							<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image3 }} " class="logo-img">
						</div>
					</div>
				</div>
			</div>
			{{-- Row 5 --}}
			<div class="form-group row">				
				<div class="col-md-6">
					<div class="form-material floating">
						<select name="trending" class="form-control">
							<option value="yes" {{ $product->trending=='yes'? 'selected': null }}>yes</option>
							<option value="no" {{ $product->trending=='no'? 'selected': null }}>no</option>
						</select>
						<label for="text">Featured</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<input type="text" class="form-control" id="page_keywords" name="page_keywords" value="{{ $product->page_keywords }}">
						<label for="text">Page Keywords</label>
					</div>
				</div>
			</div>
			{{-- Row 7 --}}
			<div class="form-group row">
				<div class="col-md-12">
					<div class="form-material floating">
						<div class="col-md-4">
							<label for="text">Long Description</label>
						</div>
						<div class="col-md-12">
							<textarea name="long_descriptions" class="form-control" id="long_descriptions">{{$product->long_descriptions}}</textarea><br>  
							<script>
								CKEDITOR.replace( 'long_descriptions' );
							</script>
						</div>								
					</div>					
				</div>
			</div>

			{{-- Row 7 --}}
			<div class="form-group row">
				<div class="col-md-12">
					<div class="form-material floating">
						<div class="col-md-4">
							<label for="text">Page Description</label>
						</div>
						<div class="col-md-12">
							<textarea name="page_description" class="form-control" id="page_description" >{{$product->page_description}}</textarea><br>  
							<script>
								CKEDITOR.replace( 'page_description' );
							</script>
						</div>								
					</div>					
				</div>
			</div>

			<div class="form-group row">
				<div class="col-md-6">
					<div class="form-material floating">
						<input type="text" class="form-control" id="page_title" name="page_title" value="{{ $product->page_title }}">
						<label for="text">Page Title</label>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-material floating">
						<select name="availability" class="form-control">
							<option value="no" {{ $product->availability=='no'? 'selected': null }}>Out of Stock</option>
							<option value="yes" {{ $product->availability=='yes'? 'selected': null }}>In Stock</option>
						</select>
						<label for="availability">Availability</label>
					</div>
				</div>
			</div>


			<div class="form-group row">				
				<div class="col-md-6">
					<div class="form-material floating">
						<select name="status" class="form-control">
							<option value="Deactive" {{ $product->status=='Deactive'? 'selected': null }}>Deactive</option>
							<option value="Active" {{ $product->status=='Active'? 'selected': null }}>Active</option>
						</select>
						<label for="mobile">Status</label>
					</div>
				</div>
			</div>

                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

