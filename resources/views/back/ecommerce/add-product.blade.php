@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>
<style>
	#getSize input { margin-left: 20px }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="product"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-product" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="name" name="name">
							<label for="name">Product</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($categoryarray as $row)
								<option value="{{$row->id}}">{{$row->Mcategory_name}}</option>
								@endforeach
								
							</select>
							<label for="email">Category</label>
						</div>
					</div>
				</div>
				
				{{-- Row 3 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="number" class="form-control" id="mrp" name="mrp">
							<label for="text">MRP</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="number" class="form-control" id="sell_price" name="sell_price">
							<label for="text">Selling Price</label>
						</div>
					</div>
				</div>
				{{-- Row 4 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image1" name="image1" style="margin-left: 15% ; width: 85%">
							<label for="text">Image 1</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image2" name="image2" style="margin-left: 15% ; width: 85%" >
							<label for="text">Image 2</label>
						</div>
					</div>
				</div>
				{{-- Row 5 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image3" name="image3" style="margin-left: 15% ; width: 85%">
							<label for="text">Image 3</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="trending" class="form-control">
								<option value=""></option>
								<option value="yes">yes</option>
								<option value="no">no</option>
							</select>
							<label for="text">Featured</label>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="product_color" name="product_color">
							<label for="text">Color</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<div class="form-control" id="getSize" style="margin-left: 10% ; width: 90%">
								<input type="checkbox" name="product_size[]" value="S">S
								<input type="checkbox" name="product_size[]" value="M">M
								<input type="checkbox" name="product_size[]" value="L">L
								<input type="checkbox" name="product_size[]" value="XL">XL
								<input type="checkbox" name="product_size[]" value="XXL">XXL
							</div>
							{{-- <select name="product_size" class="form-control">
								<option value=""></option>
								<option value="S">S</option>
								<option value="M">M</option>
								<option value="L">L</option>
								<option value="XL">XL</option>
								<option value="XXL">XXL</option>
							</select> --}}							
							<label for="text">Size</label>
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="short_descriptions" name="short_descriptions">
							<label for="text">Short Description</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="page_keywords" name="page_keywords">
							<label for="text">Page Keywords</label>
						</div>
					</div>
				</div>

				

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Long Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="long_descriptions" class="form-control" id="long_descriptions"></textarea><br>  
								<script>
									CKEDITOR.replace( 'long_descriptions' );
								</script>
							</div>								
						</div>					
					</div>
				</div>

				{{-- Row 7 --}}
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Page Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="page_description" class="form-control" id="page_description"></textarea><br>  
								<script>
									CKEDITOR.replace( 'page_description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>

				{{-- Row 6 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="page_title" name="page_title">
							<label for="text">Page Title</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="availability" class="form-control">
								<option value=""></option>
								<option value="no">Out of Stock</option>
								<option value="yes">In Stock</option>
							</select>
							<label for="availability">Availability</label>
						</div>
					</div>
				</div>

				{{-- Row 7 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
							<label for="mobile">Status</label>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-alt-primary">Submit</button>
				
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

