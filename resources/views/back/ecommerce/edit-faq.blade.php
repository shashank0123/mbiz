@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
</style>

@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="/admin/faq"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="/admin/edit-faq/{{$faq->id}}" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<input type="text" class="form-control" id="question" name="question" value="{{ $faq->questions }}">
							<label for="question">Question</label>
						</div>
					</div>					
				</div>


				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<input type="text" class="form-control" id="answer" name="answer" value="{{ $faq->answers }}">
							<label for="answers">Answer</label>
						</div>
					</div>

					
				</div>

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<select name="status" class="form-control">
								<option value="Deactive" {{ $faq->status=='Deactive'? 'selected': null }}>Deactive</option>
								<option value="Active" {{ $faq->status=='Active'? 'selected': null }}>Active</option>
							</select>
							<label for="status">Status</label>
						</div>
					</div>				
				</div>                   
				<button type="submit" class="btn btn-alt-primary">Submit</button>
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

