@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

	<a href="blog"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-blog" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				
				
				{{-- Row 3 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" id="heading" name="heading">
							<label for="heading">Blog Heading</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 15% ; width: 85%">
							<label for="text">Image</label>
						</div>
					</div>
				</div>
				
				{{-- Row 7 --}}
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<div class="col-md-4">
								<label for="text">Blog Description</label>
							</div>
							<div class="col-md-12">
								<textarea name="description" class="form-control" id="description"></textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>




				{{-- Row 6 --}}
				<div class="form-group row">					
					
					<div class="col-md-6">
						<div class="form-material floating">
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
							<label for="status">Status</label>
						</div>
					</div>

				</div>


				<button type="submit" class="btn btn-alt-primary">Submit</button>

			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

