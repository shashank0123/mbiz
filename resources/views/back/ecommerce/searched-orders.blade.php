<?php
use App\Models\Member;
use App\OrderItem;
use App\Product;
?>

@extends('back.app')

@section('content')

@include('back.include.header')
@include('back.include.sidebar')
<style>
  #show-response{
    display: none; padding: 20px 
  }
  @media screen and (max-width: 580px){
    #table-response { height: 400px; overflow: auto; }
  } 
</style>
<section style="margin-top: 50px;">
  <div class="container">

  {{-- <a href="add-product"><button type="submit" class="btn btn-alt-primary">Add Product</button> </a><br><br>
  --}} 
  <div class=" btn alert-success" id="show-response"></div>
  <div class="row">
    <div class="col-sm-6">
      <h3>Orders List</h3>
    </div>
    <div class="col-sm-6" style="text-align: right;padding-right: 50px">
      <form action="">
        <input type="search" class="form-group" name="search" id="search" style="padding: 5px" placeholder="Search here" />
        <button type="button" onclick="searchResult()" class="btn btn-primary">Search</button>
      </form>
    </div>
  </div>
  <div style="overflow: auto;" id="table-response">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>S.N</th>        
          <th>User Name</th>        
          <th>Order Detail</th>               
          <th>Price</th>
          <th>Payment Method</th>        
          <th>Transaction ID</th>        
          <th>Status</th>           
        </tr>
      </thead>
      <?php $count=1; ?>
      <tbody id="change-records">
        
        @if(!empty($order))
        <?php
        $username = Member::where('id',$order->user_id)->first();
        $order_item = OrderItem::where('created_at',$order->created_at)->get();

        ?>
        <tr>
          <td>{{$count++}}</td>        
          <td>
            @if(!empty($username))
            {{$username->username}}
            @endif
          </td>        
          <td>
            @if(!empty($order_item))
            @foreach($order_item as $item)
            <?php
            $product = Product::where('id',$item->item_id)->first();
            if(!empty($product))
              echo $product->name.",";
            ?>
            @endforeach
            @endif



          </td>         
          <td>Rs. {{$order->price}}</td>        
          <td>{{$order->payment_method}}</td>         
          <td>{{$order->transaction_id}}</td>         
          <td>
            <select name="status" id="status{{$order->id}}" onchange="changeStatus({{$order->id}})">
              <option value="0" <?php if($order->order_status == 0){ echo 'selected';} ?>>Pending</option>
              <option value="delivered" <?php if($order->order_status == 'delivered'){ echo 'selected';} ?>>Delivered</option>
              <option value="returned" <?php if($order->order_status == 'returned'){ echo 'selected';} ?>>Returned</option>
              <option value="cancelled" <?php if($order->order_status == 'cancelled'){ echo 'selected';} ?>>Cancelled</option>

            </select>
          </td>         
          </tr>

          @endif

        </tbody>
      </table>

      @if(empty($order))
      <div class="alert-danger" style="text-align: center; padding: 15px">
              "No result Found"
            </div>
      @endif
    </div>
  </div>
</section>
<script>
  function changeStatus(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var status = $('#status'+id).val();
    alert(id+" / "+status);
    $.ajax({
      type: "POST",
      url: "/update-order-status",
      data: {_token: CSRF_TOKEN, status: status, order_id : id} ,
      success:function(data){
        if(data.message == 'Successfully Updated'){
          $('#show-response').show();
          $('#show-response').html(data.message);
        }
        else{
          Swal('Something Went Wrong. Please Try Again');
        } 

      }
    });

  }
</script>

@endsection
