@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">

  <a href="/admin/faq"><button type="submit" class="btn btn-alt-primary">Back</button> </a><br><br>

  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="add-faq" method="POST" enctype="multipart/form-data">
        {{-- @csrf --}}
        
        
        <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <input type="text" class="form-control" id="question" name="question">
            <label>Question </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <input type="text" class="form-control" id="answer" name="answer">
            <label>Answer </label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <select class="form-control" name="status">
              <option value="Active">Active</option>
              <option value="Deactive">Deactive</option>
            </select><br>
            <label>Status</label>
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-alt-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
</div>
<!-- END Page Content -->
@endsection

