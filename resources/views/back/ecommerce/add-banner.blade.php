@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>


@include('back.include.header')
@include('back.include.sidebar')
<!-- Page Content -->
<div class="content" style="background: white">
	<br>
	<a href="banner" style="padding: 2%"><button type="submit" class="btn btn-alt-primary" style="padding: 0.5% 3%">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="btn btn-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-banner" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">
					<ul>
						<li>{{"Banner Size should be of 1920 * 450 for better Quality"}}</li>
						<li>{{"Other images should be of 1920 * 450 for better Quality"}}</li>
					</ul>
				</div>
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 25% ; width: 75%">
							<label for="keyword">Banner Image</label>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<select name="image_type" class="form-control">
								<option value=""></option>
								<option value="banner">Full Banner</option>
								<option value="small">Small Banner</option>
								<option value="brand">Brand Logo</option>							
							</select>
							<label for="status">Image Type</label>							
						</div>
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						<div class="form-material floating">
							<input type="text" class="form-control" name="keyword" id="keyword">
							<label for="status">Keyword For Banner Images</label>							
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
							<label for="status">Status</label>							
						</div>
					</div>
				</div>
				
				


				{{-- Row 6 --}}
				{{-- <div class="form-group row">					
					
					<div class="col-md-6">
						<div class="form-material floating">
							
						</div>
					</div>

				</div> --}}



                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-alt-primary">Submit</button>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

