<?php 
use App\Models\MemberDetail;
use App\Models\Member;
use App\User;

$getMember = Member::where('id',$member->member_id)->first();
$user = User::where('id',$getMember->user_id)->first();

?>


@extends('back.app')

@section('title')
Update Member Account | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Member Detail</li>
</ul>
@stop

@section('content')
<main>
  @include('back.include.sidebar')
  <div class="main-container" >
    @include('back.include.header')

    <style type="text/css">
      #DataTables_Table_0_wrapper .row{width: 100% !important}
    </style>
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
     @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
      <section style="padding: 1% 5%;">
          <div class="page-header">
            <h1><i class="md md-settings"></i> @lang('settings.title2')</h1>
            <p class="lead">@lang('settings.subTitle2')</p>
          </div>

          <div class="row m-b-40">
            <div class="col-md-6">
              <div class="well white">
                <form method="POST" action="/admin/bank/{{$member->member_id}}">
                {{-- <form data-parsley-validate="" role="form" class="form-floating action-form" id="accountBankForm" http-type="post" data-url="{{ route('account.postUpdate') }}"> --}}
                  <fieldset>
                    <?php $countries = config('misc.countries'); ?>
                    <div class="form-group">
                      <label class="control-label" for="inputBankName">@lang('settings.bank.name')</label>
                      <select class="form-control" name="bank_name" id="inputBankName">
                        @foreach ($countries as $country => $value)
                          <optgroup label="{{ \Lang::get('country.' . $country) }}">
                            @foreach ($value['banks'] as $bank)
                              <option value="{{ $bank }}" @if ($member->bank_name == $bank) selected="" @endif>{{ $bank }}</option>
                            @endforeach
                          </optgroup>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAccNumber">@lang('settings.bank.number')</label>
                      <input type="text" name="bank_account_number" id="inputBankAccNumber" class="form-control" required="" value="{{ $member->bank_account_number }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAccHolder">@lang('settings.bank.holder')</label>
                      <input type="text" name="bank_account_holder" id="inputBankAccHolder" class="form-control" required="" value="{{ $member->bank_account_holder }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankAddress">@lang('settings.bank.address')</label>
                      <textarea class="form-control" id="inputBankAddress" maxlength="1000" name="bank_address">{{ $member->bank_address }}</textarea>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="inputBankBranch">@lang('settings.bank.branch')</label>
                      <input type="text" name="bank_branch" id="inputBankBranch" class="form-control" value="{{ $member->bank_branch }}">
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="ifsc">IFSC Code</label>
                      <input type="text" name="ifsc" id="ifsc" class="form-control" value="{{ $member->bank_branch }}">
                    </div>
                    {{-- <input type="hidden" name="s" class="form-control" required="" value="asd123">
                    <div class="form-group">
                      <label class="control-label">@lang('settings.secret')</label>
                      <input type="password" name="password" class="form-control" required="">
                    </div> --}}

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <a  class="btn btn-secondary" onclick="resetForm()">Reset</a>
                      <a href="/admin/member-list" class="btn btn-danger">@lang('common.cancel')</a>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main>
<script>
  function resetForm(){
      $('input[type=text]').val("");
      $('input[type=number]').val("");
      $('textarea').val("");
      
    }
</script>
@stop
