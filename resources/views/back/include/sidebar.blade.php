<style>
  #withdraw{ display: none; }
  #announcement{ display: none; }
  #income{ display: none; }
  #package{ display: none; }
  span { cursor: pointer; }
</style>

<nav id="sidebar">
  <!-- Sidebar Content -->
  <div class="sidebar-content">
    <!-- Side Header -->
    <div class="content-header content-header-fullrow px-15">
      <!-- Mini Mode -->
      <div class="content-header-section sidebar-mini-visible-b">
        <!-- Logo -->
        <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
          <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
        </span>
        <!-- END Logo -->

      </div>
      <!-- END Mini Mode -->

      <!-- Normal Mode -->
      <div class="content-header-section text-center align-parent sidebar-mini-hidden">
        <!-- Close Sidebar, Visible only on mobile screens -->
        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
        <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
          <i class="fa fa-times text-danger"></i>
        </button>
        <!-- END Close Sidebar -->

        <!-- Logo -->
        {{-- <div class="content-header-item">
          <a class="link-effect font-w700" href="/dashboard">
            <i class="si si-fire text-primary"></i>
            <span class="font-size-xl text-dual-primary-dark">M</span><span class="font-size-xl text-primary">BIZ</span>
          </a>
        </div> --}}
        <!-- END Logo -->
      </div>
      <!-- END Normal Mode -->
    </div>
    <!-- END Side Header -->

    <!-- Side User -->
    <div class="content-side content-side-full content-side-user px-10 align-parent">
      <!-- Visible only in mini mode -->
      <div class="sidebar-mini-visible-b align-v animated fadeIn">
        <img class="img-avatar img-avatar32" src="{{ asset('media/avatars/avatar15.jpg') }}" alt="">
      </div>
      <!-- END Visible only in mini mode -->

      <!-- Visible only in normal mode -->
      <div class="sidebar-mini-hidden-b text-center">
        <a class="img-link" href="javascript:void(0)">
          @if(isset($image->image))
          <img class="img-avatar" src="/assetsss/images/logo.jpeg" alt="">
          @else
          <img class="img-avatar" src="{{asset('assetsss/images/logo.jpeg') }}" alt="">
          @endif
        </a>
        <ul class="list-inline mt-10">
          <li class="list-inline-item">
            <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="javascript:void(0)">ID : SS000{{ '01' }}</a>
          </li>
          <li class="list-inline-item">
            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
            <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
              <i class="si si-drop"></i>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="link-effect text-dual-primary-dark" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"><i class="zmdi zmdi-power"></i>
            <i class="si si-logout"></i>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>                                    
        </li>
      </ul>
      <ul></ul>           
    </div>
    <!-- END Visible only in normal mode -->
  </div>
  <!-- END Side User -->

  <div class="content-side content-side-full">
    <ul class="nav-main">
                            {{-- <li>
                                <a class="" href="">
                                    <i class="si si-cup"></i><span class="sidebar-mini-hide">ID : SS000{{ Auth::user()->id }}</span>
                                </a>
                              </li> --}}

                              <li>
                                <a class="{{ request()->is('dashboard') ? ' active' : '' }}" href="/admin/dashboard">
                                  <i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span>
                                </a>
                              </li>
                              <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden">MLM Features</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bulb"></i><span class="sidebar-mini-hide"> </span></a>
                                {{-- <ul>
                                  <li>
                                    <a class="{{ request()->is('admin/add-member') ? ' active' : '' }}" href="/admin/add-member">Add Member</a>
                                  </li>

                                  <li>
                                    <a class="{{ request()->is('admin/list-admin') ? ' active' : '' }}" href="/admin/list-admin">Direct List</a>
                                  </li>


                                  <li>
                                    <a class="{{ request()->is('admin/tree-admin') ? ' active' : '' }}" href="/admin/tree-admin">admin-tree</a>
                                  </li>
                                  <li>
                                    <a class="{{ request()->is('admin/profile') ? ' active' : '' }}" href="/admin/profile">Profile</a>
                                  </li>
                                  <li>
                                    <a class="{{ request()->is('admin/payment-list') ? ' active' : '' }}" href="/admin/payment-list">Payment Status</a>
                                  </li>
                                </ul> --}}


                                <ul>

                                  <li>
                                    <a class="{{ request()->is('/admin/member-list') ? ' active' : '' }}" href="/admin/member-list">Member </a>
                                  </li>
                                  {{-- <li>
                                    <a class="{{ request()->is('admin/member/all') ? ' active' : '' }}" href="/admin/member/all">Members List</a>
                                  </li>
 --}}
                                  <li>
                                    <a class="{{ request()->is('admin/member/register') ? ' active' : '' }}" href="/admin/member/register">Add New Root Member</a>
                                  </li>

                                  <li>
                                    <a class="{{ request()->is('admin/epoint/all') ? ' active' : '' }}" href="/admin/epoint/allepoint">Epoint List</a>
                                  </li>


                                  <li>
                                    <a class="{{ request()->is('admin/epoint/add') ? ' active' : '' }}" href="/admin/epoint/add">Add Epoint</a>
                                  </li>
                                  <li>
                                    <a class="{{ request()->is('admin/upgrade-package') ? ' active' : '' }}" href="/admin/upgrade-package">Upgrade Package</a>
                                  </li>

                                  {{-- <li>
                                    <a class="{{ request()->is('/admin/member-list') ? ' active' : '' }}" href="/admin/member-list">Update Member Detail</a>
                                  </li>

                                 
                                  <li>
                                    <a class="{{ request()->is('/admin/reserpassword') ? ' active' : '' }}" href="/admin/resetpassword">Reset Password</a>
                                  </li> --}}
                                  <li>
                                    {{-- <a class="{{ request()->is('admin/payment-list') ? ' active' : '' }}" href="/admin/payment-list">Payment Status</a> --}}
                                  </li>
                                </ul>


                              </li>

                              <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden">Shop Products</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bulb"></i><span class="sidebar-mini-hide"> </span></a>
                                <ul>
                                  {{-- <li>
                                    <a class="{{ request()->is('admin/add-main-category') ? ' active' : '' }}" href="/admin/add-main-category">Add Main Category</a>
                                  </li> --}}                                  
                                  <li>
                                    <a class="{{ request()->is('admin/main-category') ? ' active' : '' }}" href="/admin/main-category">Manage Category</a>
                                  </li>


                                  <li>
                                    <a class="{{ request()->is('admin/product') ? ' active' : '' }}" href="/admin/product">Manage Product</a>
                                  </li>

                                  <li>
                                    <a class="{{ request()->is('admin/banner') ? ' active' : '' }}" href="/admin/banner">Manage Home Page Images</a>
                                  </li>

                                  {{-- <li>
                                    <a class="{{ request()->is('admin/blog') ? ' active' : '' }}" href="/admin/blog">Manage Blogs</a>
                                  </li> --}}

                                  <li>
                                    <a class="{{ request()->is('admin/review') ? ' active' : '' }}" href="/admin/review">Manage Reviews</a>
                                  </li>

                                  {{-- <li>
                                    <a class="{{ request()->is('admin/newsletter') ? ' active' : '' }}" href="/admin/newsletter">Manage Newsletters</a>
                                  </li>
                                  --}}
                                  <li>
                                    <a class="{{ request()->is('admin/testimonial') ? ' active' : '' }}" href="/admin/testimonial">Manage Testimonial</a>
                                  </li>
                                  <li>
                                    <a class="{{ request()->is('admin/faq') ? ' active' : '' }}" href="/admin/faq">Manage FAQ</a>
                                  </li>
                                  <li>
                                    <a class="{{ request()->is('admin/static-pages') ? ' active' : '' }}" href="/admin/static-pages">Manage Static Pages</a>
                                  </li>

                                  <li>
                                    <a class="{{ request()->is('admin/orders') ? ' active' : '' }}" href="/admin/orders">Manage Orders</a>
                                  </li>
                                </ul>
                              </li>


                              <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden" onclick="show('package')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Package</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}" id="package">
                                
                                <ul>
                                <li>
                                    <a href="{{ route('admin.settings.package') }}">
                                      <span>&nbsp;&nbsp;&nbsp;Settings</span>
                                    </a>
                                  </li>
                                </ul>
                              </li>


                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#MDPackage" aria-expanded="false" aria-controls="MDPackage" class="collapsible-header waves-effect"><i class="md md-wallet-giftcard"></i>&nbsp;Package</a>
                                <ul id="MDPackage" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.settings.package') }}">
                                      <span>Settings</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}

                              <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden" onclick="show('income')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Income</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}" id="income">
                                
                                <ul>
                                  <li>
                                    <a href="{{ route('admin.income.direct') }}">
                                      <span>&nbsp;&nbsp;&nbsp;Daily Payout Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.income.weekly') }}">
                                      <span>&nbsp;&nbsp;&nbsp;Weekly Income</span>
                                    </a>
                                  </li>
                                  {{-- <li>
                                    <a href="#">
                                      <span>Car Fund Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <span>House Fund Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <span>Reward Income</span>
                                    </a>
                                  </li> --}}
                                </ul>
                              </li>

                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#income" aria-expanded="false" aria-controls="MDMember" class="collapsible-header waves-effect"><i class="md-receipt"></i>&nbsp;Income</a>
                                <ul id="income" class="collapse">
                                  <li>
                                    <a href="#">
                                      <span>Daily Payout Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <span>Allowance Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <span>Car Fund Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <span>House Fund Income</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="#">
                                      <span>Reward Income</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}

                              <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden" onclick="show('announcement')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Announcement</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}" id="announcement">
                                
                                <ul>
                                <li>
                                    <a href="{{ route('admin.announcement.create') }}">
                                      <span>&nbsp;&nbsp;&nbsp;&nbsp;Create</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.announcement.list') }}">
                                      <span>&nbsp;&nbsp;&nbsp;&nbsp;List</span>
                                    </a>
                                  </li>
                                </ul>
                              </li>


                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#MDAnnouncement" aria-expanded="false" aria-controls="MDAnnouncement" class="collapsible-header waves-effect"><i class="md md-new-releases"></i>&nbsp;Announcement</a>
                                <ul id="MDAnnouncement" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.announcement.create') }}">
                                      <span>Create</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.announcement.list') }}">
                                      <span>List</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}

                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#MDShares" aria-expanded="false" aria-controls="MDShares" class="collapsible-header waves-effect"><i class="md md-trending-up"></i>&nbsp;Shares</a>
                                <ul id="MDShares" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.shares.sellAdmin') }}">
                                      <span>Sell</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.settings.shares') }}">
                                      <span>Settings</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.split') }}">
                                      <span>Split</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.buy') }}">
                                      <span>Buy List</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.sell') }}">
                                      <span>Sell List</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.lock') }}">
                                      <span>Freeze List</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}

                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#MDBonus" aria-expanded="false" aria-controls="MDBonus" class="collapsible-header waves-effect"><i class="md md-attach-money"></i>&nbsp;Bonus</a>
                                <ul id="MDBonus" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.bonus.addStatement') }}">
                                      <span>Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=direct' }}">
                                      <span>Direct Statement</span>
                                    </a>
                                  </li>

                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=group' }}">
                                      <span>Group Statement</span>
                                    </a>
                                  </li>

                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=override' }}">
                                      <span>Override Statement</span>
                                    </a>
                                  </li>

                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=pairing' }}">
                                      <span>Pairing Statement</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}

                              <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden" onclick="show('withdraw')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Withdraw</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}" id="withdraw">
                                {{-- <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bulb"></i><span class="sidebar-mini-hide"> </span></a> --}}
                                <ul>
                                <li>
                                    <a href="{{ route('admin.withdraw.addStatement') }}">
                                      <span>&nbsp;&nbsp;&nbsp;&nbsp;Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.withdraw.all') }}">
                                      <span>&nbsp;&nbsp;&nbsp;Statement</span>
                                    </a>
                                  </li>
                                </ul>
                              </li>


                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#MDWithdraw" aria-expanded="false" aria-controls="MDWithdraw" class="collapsible-header waves-effect"><i class="md md-assignment-returned"></i>&nbsp;Withdraw</a>
                                <ul id="MDWithdraw" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.withdraw.addStatement') }}">
                                      <span>Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.withdraw.all') }}">
                                      <span>Statement</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}

                              {{-- <li>
                                <a href="#" data-toggle="collapse" data-target="#MDTransfer" aria-expanded="false" aria-controls="MDTransfer" class="collapsible-header waves-effect"><i class="md md-swap-vert"></i>&nbsp;Transfer</a>
                                <ul id="MDTransfer" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.transfer.addStatement') }}">
                                      <span>Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.transfer.all') }}">
                                      <span>Transfer statement</span>
                                    </a>
                                  </li>
                                </ul>
                              </li>

                              <li>
                                <a href="#" data-toggle="collapse" data-target="#MDCoin" aria-expanded="false" aria-controls="MDCoin" class="collapsible-header waves-effect"><i class="md md-album"></i>&nbsp;Coin</a>
                                <ul id="MDCoin" class="collapse">
                                  <li>
                                    <a href="{{ route('admin.coin.transaction') }}">
                                      <span>Transaction List</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.coin.list') }}">
                                      <span>Wallet List</span>
                                    </a>
                                  </li>
                                </ul>
                              </li> --}}


                              {{-- <li class="nav-main-heading">
                                <span class="sidebar-mini-visible">MLM</span><span class="sidebar-mini-hidden">Settings</span>
                              </li>
                              <li class="{{ request()->is('admin/*') ? ' open' : '' }}">
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-bulb"></i><span class="sidebar-mini-hide"> </span></a>
                                
                                <ul>
                                  <li>
                                    <a class="{{ request()->is('/admin/reserpassword') ? ' active' : '' }}" href="/admin/resetpassword">Reset Password</a>
                                  </li> --}}

                                  {{-- <li>
                                    <a class="{{ request()->is('admin.announcement.list') ? ' active' : '' }}" href="{{ route('admin.announcement.list') }}">List</a>
                                  </li> --}}
                                {{-- </ul>
                                </li> --}}

                                <li icon="md md-settings-power">
                                  <a href="{{ route('admin.logout') }}"><i class="md md-settings-power"></i>&nbsp;<span>Logout</span></a>
                                </li>



                              </ul>
                            </div>
                            <!-- END Side Navigation -->
                          </div>
                          <!-- Sidebar Content -->
                        </nav>
            <!-- END Sidebar -->

            <script>
              function show(data){
                if(data == 'withdraw')
                $('#withdraw').toggle();
              if(data == 'package')
                $('#package').toggle();
              if(data == 'announcement')
                $('#announcement').toggle();
              if(data == 'income')
                $('#income').toggle();
              }
            </script>