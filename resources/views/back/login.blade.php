@extends('back.app')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important;
  }
  .card{margin-top:0% auto; max-width:40%;margin-left: 17%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .pink-text {  font-size: 24px;  }

  .pull-right button { margin-bottom: 20px !important }

  @media screen and (max-width: 991px)
  {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }

  @media screen and (max-width: 580px)
  {
    .card { max-width:90%;margin-left: 5%; }
  }
  
</style>

<div class="clearfix"></div>
                  <div class="container" >
                      <div  class="col-lg-12 col-md-12 col-sm-12">
                        <div style="background-color: orange;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
                            <ul>
                                <li style="list-style: none">
                                    <a href="#" style="color: #fff; font-size: 14px">Help Line:  1800-419-8447</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                  </div>
<div class="container-fluid center"> 

  <div class="card bordered z-depth-2">
    <div class="card-header">
      <div class="brand-logo" style="text-align: center;">
        <img src="{{ asset('/asset/images/menu/logo/mbiz.png') }}" width="100">
      </div>
    </div>
    <div class="container">
    <form class="form-floating action-form" http-type="post" data-url="{{ route('admin.postLogin') }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong pink-text">Login</div>
          <p class="card-title-desc"> Welcome to {{ config('app.name') }} | ADMIN AREA</p>
        </div>
        <div class="form-group">
          <label for="username" class="control-label">Username</label>
          <input type="text" name="username" class="form-control" id="username" required="">
        </div>
        <div class="form-group">
          <label for="password" class="control-label">Password</label>
          <input type="password" name="password" class="form-control" id="password" required="">
        </div>
        <div class="form-group">
          <div class="checkbox">
          <label><input type="checkbox" name="remember"> Remember me </label>
          </div>
        </div>
      </div>
    
      <div class="card-action clearfix">
        <div class="pull-right">
          <button type="submit" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span>Login</span>
          </button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

@stop