<?php

use App\MainCategory;
use App\Product;
use App\Review;
use App\Wishlist;
$total=0;
$count_menu = 0;
$i=0;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>M-Biz Trading Private Limited</title>

	<link rel="stylesheet" href="/assetsss/styles/style.min.css">

	<link rel="shortcut icon" href="{{ asset('assetsss/images/favicon.ico') }}">

	{{-- <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'> --}}
	{{-- <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}"> --}}
	{{-- <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assetsss/images/mbizlogo.png') }}"> --}}
</head>

<style>


	.dropdown {
		position: relative;
		display: inline-block;
	}

	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #f9f9f9;
		min-width: 160px;
		box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		padding: 12px 16px;
		z-index: 1;
	}

	.dropdown:hover .dropdown-content {
		display: block;
	}

	.have-menu .fa {
		color: #999;    
		margin-left: 90%;
		margin-top: -23px;
	}

	.main-menu-wrap .menu>li>a { font-size: 13px !important; }			
	.header .middle .logo { top: 50px !important; }
	.header .middle .logo { position: absolute; left: 0; height: 85px !important; }
	.header .middle .logo img { top: 40%; max-height: 75px;	width: auto;}
	.quotes-wrap { padding: 85px 0 -205px 0 !important; }
	.slick-slider { height: 400px; }
	.main-menu-wrap .menu>li { display: inline-block; position: relative; margin-left: 18px; }

	#drop-down { display: none; }

	#myBtn { display: none; position: fixed; bottom: 20px; right: 30px; z-index: 99; font-size: 18px; border: none; outline: none; background-color: red; color: white; cursor: pointer; padding: 10px 15px; border-radius: 50%; }
	.mannual-navbar { width: 30% ; float: right; z-index: 99; font-size: 14px; line-height: 24px; background: #fff; -webkit-box-shadow: -1px 2px 9px 0 rgba(0,0,0,.85); -moz-box-shadow: -1px 2px 9px 0 rgba(0,0,0,.85); box-shadow: -1px 2px 9px 0 rgba(0,0,0,.85); padding: 44px 29px 30px; position: absolute; right: 0%; top: 150;max-height: 400px; overflow: auto}

	.more-cat { margin-left: 8%; display: none; }

	.more-sub-cat { margin-left: 8% ; display: none;}



	#submenu-navbar { display: none; display: none;
		position: absolute;
		background-color: #ff0000;
		width: 250px;
		height: 250px;
		left: 200px;
		top: 100px;
		z-index: 99;}

		.have-menu span { text-align: right;margin-top: -20px }

		.more-cat {
			display: none;
		}

		.more-sub-cat{ display: none; }
		.mannual-navbar .more-main-cat .have-menu { border-bottom: 1px solid #999 }

		.mannual-navbar .more-main-cat .have-sub-menu { border-bottom: 1px dashed #999 }

		.mannual-navbar .more-main-cat .no-sub-menu { border-top: 1px solid #999 }
		.mannual-navbar .more-main-cat { margin-left: 5% }
		.mannual-navbar ul li a:hover { color: #37bc9b; text-decoration: underline; }
		.all-more-categories { overflow: auto; }
		.mannual-navbar ul li a:hover #submenu-navbar { display: block !important; }

		.mannual-navbar ul li a {display: block;
			font-size: 15px ;
			line-height: 26px ;
			padding: 5px 0  ;
			color:#898c90 !important ;
			text-transform: uppercase ;
			border-bottom: 2px solid transparent }

			#myBtn:hover {
				background-color: #555;
			}

			@media screen and (max-width: 991px){
				.header .middle .logo { top: 10px !important; }
				.header .middle .logo img { margin-left: 10%; }
			}
			.menumobile-navbar { z-index: 25 !important; display: none; }

			.main-menu-wrap .menu .mega-menu-wrap .mega-menu .title { font-size: 14px !important; }

			.main-menu-wrap .menu .sub-menu a { font-size: 14px; }
		</style>
		{{-- <body oncontextmenu="return false" onselectstart="return false" ondragstart="return false"> --}}
			<body>	 
				<button onclick="topFunction()" id="myBtn" title="Go to top"><i class='fa fa-angle-up' style='font-size:30px; color: #fff; font-weight: bold; margin-bottom: 2px'></i></button>
				<div class="menumobile-navbar-wrapper">
					<nav class="menu-navbar menumobile-navbar js__menu_mobile">
						<div class="menumobile-close-btn js__menu_close" onclick="hideNavBar()">
							<span class="fa fa-times"></span> CLOSE
						</div>
						<div id="menu-mobile">
							<ul class="menu">
								<li class="current-menu-item"><a href="/index">Home</a></li>
								@foreach($maincategory as $row)
								<li class="menu-item-has-children"><a href="/products/{{$row->slug}}">{{$row->Mcategory_name}}</a><span class="drop-down-icon js__menu_drop"></span>
									<ul class="sub-menu">
										<?php
										$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
										?>
										@foreach($category as $rows)
										<li class="menu-item-has-children">

											<a href="/products/{{$rows->slug}}">{{$rows->Mcategory_name}}</a>
											<span class="drop-down-icon js__menu_drop"></span>
											<ul class="sub-menu">
												<?php
												$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
												?>
												@foreach($subcategory as $col)
												<li><a href="/products/{{$col->slug}}">{{$col->Mcategory_name}}</a></li>
												@endforeach									
											</ul>
										</li>
										@endforeach							
									</ul>
								</li>
								@endforeach
								<li><a href="/about">About us</a></li>
								{{-- <li><a href="#">Contact us</a></li> --}}
							</ul><!--/.menu -->
						</div><!--/#menu- -navbar -->
					</nav>
				</div><!--/.menu- -navbar-wrapper -->
				<div class="mobile-sticky js__menu_sticky">
					<div class="container">
		{{-- <div class="left-side">
			<a href="/index" class=""><img src="/assetsss/images/mbizlogo.png" alt=""  style="width: 140px; height: 200px"></a>
		</div> --}}
		<div class="right-side">
			<button type="button" class="menumobile-toggle js__menu_toggle" onclick="showBar()">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	</div>
</div>
<style>
	#response-list{ text-align: right !important }
	
	@media screen and (max-width: 768px)
	{
		#response-icon{ text-align: center !important; }
		#response-list{ text-align: center !important }
		.search-form { display: none; }
	}
	
</style>
<div id="wrapper">
	<header class="header">
		@if(!empty($member->id))	
		<input type="hidden" name="user_id" value="{{$member->id}}" id="user_id" >
		@endif

		<div class="top">
			<div class="container">
				<div class="row" id="response-icon">
					<div class="col-sm-1 col-xs-12" >
						<ul class="list-inline" style="margin-top: 5px;margin-bottom: 0">
							<li class="list-inline-item"><a href="#" class="fa fa-twitter" ></a></li>
							<li class="list-inline-item"><a href="https://www.facebook.com/M-Biz-Trading-Private-Limited-2122159934768714/" class="fa fa-facebook" target="blank"></a></li>
							{{-- <li class="list-inline-item"><a href="#" class="fa fa-google-plus"></a></li> --}}
							<li class="list-inline-item"><a href="#" class="fa fa-linkedin"></a></li>		<button type="button" class="menumobile-toggle js__menu_toggle" style="float: right; top: -10px; "  onclick="showBar()">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>				
						</ul>
					</div><!--/.left-side -->

					<div class="col-sm-2 col-xs-12">
						{{-- <a href=" http://iseindia.ltd/" target="blank"><img src="/assetsss/images/s&i.png" title="Stocks & Investments"></a> --}}
					</div>
					
					<div class="col-sm-9 col-xs-12" id="response-list">
						<ul class="menu">
							@if (!empty($member->id))

							{{-- <li><a href="/en/member">Panel</a></li> --}}
							<li><a href="/en/member">My Account</a></li>
							{{-- <li><a href="/wishlist">My Wishlist</a></li> --}}		
							
							{{-- <li><a href="/cart">Checkout</a></li> --}}
							
							<li><a href="/en/logout">Logout</a></li>
							@else
							<li><a href="/en/login">Login</a></li>
							@endif
							<li><strong><a style="color: #f60e23">TOLL FREE&nbsp;&nbsp;: &nbsp;&nbsp;</a><a style="font-size: 20px;color: #f60e23" href="tel:1800-419-8447">1800-419-8447</a></strong></li>
						</ul>

					</div><!--/.right-side -->
				</div>
			</div><!--/.container -->
		</div><!--/.top -->
		
		@foreach($maincategory as $main)
		<?php
		$category = MainCategory::where('category_id',$main->id)->get();
		?>

		@endforeach
		<div class="container">
			<div class="middle">
				<a href="/index" class="logo"><img src="/assetsss/images/mbizlogo.png" alt="" style="width: 140px; height: 200px"></a>
				<form action="/searchproduct" method="GET" class="search-form">
					<div class="select-category">
						<select class="js__select2" data-min-results="Infinity" name="subcategory" style="max-width: 170px;">
							<option value="">All</option>;
							@foreach($maincategory as $row)
							<?php $cat = MainCategory::where('category_id',$row->id)->get();
							?>
							@foreach($cat as $col)
							<option value="{{ $col->slug }}">{{ $col->Mcategory_name }}</option>
							<?php $subcat = MainCategory::where('category_id',$col->id)->get();
							?>
							@foreach($subcat as $cols )
							<option value="{{ $cols->slug }}">{{ $cols->Mcategory_name }}</option>                    
							@endforeach           
							@endforeach           
							@endforeach						
						</select>
					</div>
					<input type="text" placeholder="Search" class="inp-search" name="product_keyword" style="margin-left: 30px;">
					<button type="submit" name="search" class="inp-submit"><i class="fa fa-search"></i></button>
				</form><!--/.search-form -->
				<ul class="right-side">
					<li><a href="/wishlist" class="fa fa-heart" title="Wishlist">
						<?php	
						//$show_count=session()->get('count_wishlist');
						if(!empty($member->id))
						$show_cnt=Wishlist::where('user_id',$member->id)->count();
					else 
						$show_cnt = 0;
						?>
						<span class="num" id="show-ttl">{{$show_cnt}}</span>
						
					</a></li>
					<li class="js__drop_down" >
						<a onclick="showCart()" title="Shopping Cart" class="fa fa-shopping-cart js__drop_down_button">						
							
							<?php
							$show=session()->get('count');
							?>

							@if(session()->get('count') != null)
							<span class="num" id="show-total">{{$show}}</span>
							@else
							<span class="num" id="show-total">0</span>
							@endif
						</a>
						<div class="cart-list-container">
							<div class="cart-list-content" id="showminicart">
								@if($cartproducts != null)
								<?php $i=0; $final_rate = 0; ?>

								<ul class="cart-list" style="max-height: 350px; overflow: auto">
									@foreach($cartproducts as $product)

									<li id="listhide{{$product->id}}">
										<a class="thumb" href="#"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" class="attachment-shop_thumbnail" style="width: 70px; height: 70px"></a>
										<a href="#" class="title">{{$product->name}}</a>
										<span class="quantity">
											<span class="amount">Rs. {{$product->sell_price}}</span> x {{$quantity[$i]}}
										</span>
										<div class="rate">
											<?php
											$total  =$total + $product->sell_price * $quantity[$i]; 

											$rate = Review::where('product_id',$product->id)->avg('rating');
											$final_rate = floor($rate);
											if($final_rate != null)
												echo $final_rate;
											else
												echo 0;									
											?>

										</div>
										<a title="Remove this item" class="mini-cart-remove" onclick="deleteProduct({{$product->id}})"><span class="fa fa-times"></span></a>										
									</li>
									@endforeach								
								</ul><!--/.cart-list -->
								@endif									
								<div class="cart-list-subtotal">
									<strong class="txt fl">SubTotal:</strong>
									<strong class="currency fr" id="bill">Rs. </strong>
								</div><!--/.cart-list-subtotal -->
							</div><!--/.cart-list-content -->
							<a class="cart-list-bottom" href="<?php if(!empty($member->id)){ echo "/cart"; } else { echo "/en/login" ;} ?>">
								<span class="fl">Checkout</span>
								<span class="fr"><i class="fa fa-long-arrow-right"></i></span>
							</a><!--/.cart-list-bottom -->
						</div><!--/.cart-list-container -->
					</li>
				</ul><!--/.right-side -->
			</div><!--/.middle -->

			<div class="bottom">
				<div class="left-side">
						{{-- <button type="button" class="menumobile-toggle js__menu_toggle">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button> --}}
						<div class="main-menu-wrap js__auto_correct_sub_menu">
							<ul class="menu">
								{{-- <li class="current-menu-item"><a href="/index"><i class='fa fa-home' style='font-size:24px'></i></a></li> --}}
								<?php $count_main_cat = 0;
								$maincat = array();
								$maincat_cnt = 0; ?>
								@foreach($maincategory as $row)
								<?php $count_main_cat++;
								if($count_main_cat>6){
									$maincat[$maincat_cnt++] = $row->id;
									continue;
								} ?>
								<li class="menu-item-has-children mega-menu-wrap"><a href="/products/{{$row->slug}}">{{$row->Mcategory_name}}</a>
									<div class="mega-menu">
										<div class="row">
											<div class="col-md-12">
												<div class="row">
													<?php 
													$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
													?>
													@foreach($category as $rows)
													<?php $count_menu = 0; ?>
													<div class="col-md-3">
														<h3 class="title"><a href="/products/{{$rows->slug}}">{{$rows->Mcategory_name}}</a></h3>
														<ul class="sub-menu">
															<?php
															$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->limit(7)->get();
															
															?>
															@foreach($subcategory as $col)
															<li><a href="/products/{{$col->slug}}">{{$col->Mcategory_name}}</a></li>
															<?php $count_menu++;  ?>
															@endforeach

														</ul>
														@if($count_menu >= 7)
														<div class="row">
															<div class="col-sm-12" style="float: right;">
																<a href='/products/{{$rows->slug}}'>View More...</a>
															</div>
														</div>
														@endif
													</div><!--/col -->
													
													@endforeach

												</div><!--/.row -->
											</div><!--col -->
											<?php 
											$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
											?>
											@foreach($category as $rows)
											<?php
											$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
											?>											
											@endforeach
											
										</div><!--/.row -->
									</div><!--/.menu-mega -->
								</li>
								@endforeach
								<li><a href=" http://iseindia.ltd/" target="blank">Stocks & Investments</a></li>
								@if($count_main_cat>=6)
								<li class="menu-item-has-children mega-menu-wrap"><a style="cursor: pointer;">More...</a>
									<div class="mega-menu" style="width: 20%; margin-left: 80%">
										<div class="all-more-categories">
											<ul class="more-main-cat">
												@for($i = 0; $i < $maincat_cnt ; $i++)
												<?php $maincategory = MainCategory::where('id',$maincat[$i])->first(); ?>
												<li class="have-menu"><a href="/products/{{$maincategory->slug}}" style="font-weight: bold; color: #898c90">{{$maincategory->Mcategory_name}}</a><i class='fa fa-chevron-down' onclick="showCat({{$maincategory->id}});"></i>
													<?php $category = MainCategory::where('category_id',$maincategory->id)->get(); ?>
													@if($category != null)

													<ul class="more-cat" id="more-cat{{$maincategory->id}}">
														@foreach($category as $cate)
														<li class="have-sub-menu"><a href="/products/{{$cate->slug}}" style="color: #898c90">{{$cate->Mcategory_name}}</a> <i class='fa fa-chevron-down'onclick="showSubCat({{$cate->id}});"></i>
															<?php $subcategory = MainCategory::where('category_id',$cate->id)->get(); ?>
															@if($subcategory != null)

															<ul class="more-sub-cat" id="more-sub-cat{{$cate->id}}">
																@foreach($subcategory as $sub)
																<li class="no-sub-menu" >
																	<a href="/products/{{$sub->slug}}" style="color: #898c90">{{$sub->Mcategory_name}}</a>
																</li>
																@endforeach
															</ul>

															@endif
														</li>
														@endforeach
													</ul>								
													@endif
												</li>
												@endfor
											</ul>
										</div>
									</div><!--/.menu-mega -->
								</li>
								@endif

								{{-- <li><a href="about">About us</a></li> --}}
								{{-- <li><a href="#">Contact us</a></li> --}}
							</ul><!--/.menu -->
						</div><!--/.main-menu-wrap -->
					</div><!--/.left-side -->

					
				</div><!--/.bottom -->
			</div><!--/.container -->
		</header><!--/.header -->

		@yield('content')

		<style>
			@media screen and (max-width: 768px){
				.footer{ text-align: center !important; }
				.footer .widget .menu li a{ text-align: center !important; } 
				.widget .title { font-size: 20px }
			}
			.widget .menu li a { color: #fff; font-size: 13px }
			.widget .menu li { padding-bottom: 8px; }

		</style>

		<footer class="footer">
			<div class="container">
				<div class="top">
					<div class="row">
						<div class="col-md-9 col-sm-12">
							<div class="row">
								<div class="col-md-4 col-sm-4">
									<div class="widget ">
										<h3 class="title" style="color: #fff">Help</h3>
										<ul class="menu">
											{{-- <li><a href="#">Track order</a></li> --}}
											<li><a href="/faq">FAQs</a></li>
											<li><a href="/privacypolicy">Privacy policy</a></li>
											<li><a href="/tnc">Terms & Conditions</a></li>
											<li><a href="/returnpolicy">Refunds</a></li>
											{{-- <li><a href="#">Support Online</a></li> --}}
										</ul>
									</div>
								</div><!-- col -->

								<div class="col-md-4 col-sm-4">
									<div class="widget ">
										<h3 class="title" style="color: #fff">Account</h3>
										<ul class="menu">
											<li><a href="/en/member">My account</a></li>
											<li><a href="/wishlist">Wishlist</a></li>
											{{-- <li><a href="">Order history</a></li> --}}
											{{-- <li><a href="#">My Favorites</a></li> --}}
											{{-- <li><a href="#">Gift Vouchers</a></li> --}}
											{{-- <li><a href="#">Specials</a></li> --}}
										</ul>
									</div>
								</div><!-- col -->

								<div class="col-md-4 col-sm-4">
									<div class="widget ">
										<h3 class="title" style="color: #fff">Quick Links</h3>
										<ul class="menu">
											{{-- <li><a href="#">Best Sellers</a></li> --}}
											{{-- <li><a href="#">Featured Products</a></li> --}}
											{{-- <li><a href="#">Hot Products</a></li> --}}
											{{-- <li><a href="#">Top Rated</a></li> --}}
											{{-- <li><a href="#">Blog</a></li> --}}
											<li><a href="/about">About Us</a></li>
											<li><a href="/contact-us">Contact Us</a></li>						
										</ul>
									</div>
								</div><!-- col -->
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6">
									<br>
									<a href="http://bizbank.in/ " target="blank"><img src="/assetsss/images/AdminProduct/bizbanklogo.jpg" style="width: 150px; height: auto;"></a>
								</div>
								<div class="col-md-4 col-sm-6">
									<h3 class="title" style="color: #fff">Bank Detail</h3>
									<p style="margin-top: 5px">M-Biz Trading Pvt Ltd<br>
										ICICI Bank<br>
									A/C - 400305500145<br>
									IFSC - ICIC0004003</p>
								</div>

								<div class="col-md-4 col-sm-6">
									<h3 class="title" style="color: #fff">Company Detail</h3>
									<p style="margin-top: 5px">GST - AA070619047507U<br>
										CIN - U72900DL2018PTC340621<br>
										PAN - AAMCM1686P<br>
										TAN - DELM33184E<br>
									Licence No. - U72900DL/MBT/DL/45/19</p>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="widget widget_text">
								<h3 class="title" style="color: #fff">Contact Us</h3>

								<p>Please feel free to contact us if you have any question.</p>
								<ul class="contact-list">
									<li><a href="mailto:info@m-biz.in"><i class="fa fa-envelope-o"></i>info@m-biz.in</a></li>
									<li><a href="tel:1800 419 8447"><i class="fa fa-phone"></i>1800 419 8447</a></li>
									<li><i class="fa fa-map-marker"></i>M-Biz Trading Private Limited<br> A-51, 2nd Floor, New Dwarka Road, Dabri Exnt, New Delhi-110045</li>
								</ul>
							</div>
						</div><!-- col -->
					{{-- <div class="col-md-3 col-sm-6">
						<div class="widget widget_subscribe">
							<h3 class="title">Newsletter Signup</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
							<form action="#" class="join-form">
								<div class="form-controls">
									<input type="email" placeholder="Enter your email" class="inp-email">
									<input type="submit" value="Join" class="inp-submit">
									<i>We respect your privac</i>
								</div>
							</form>
							<ul class="social-list" style="text-align: center;">
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-facebook"></a></li>
								<li><a href="#" class="fa fa-google-plus"></a></li>
								<li><a href="#" class="fa fa-linkedin"></a></li>
								{{-- <li><a href="#" class="fa fa-vimeo"></a></li>
								<li><a href="#" class="fa fa-pinterest-p"></a></li>
							</ul>
						</div>
					</div><!-- col --> --}}
				</div><!-- .row -->
			</div><!-- .top -->
		</div><!-- .container -->
		<div class="bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="copyright">Copyright &copy; 2019 <a href="#">M-Biz Trading Private Limited.</a> All rights reserved.</a></div>
					</div><!-- col -->

					<div class="col-sm-4">
						<ul class="social-list" style="text-align: center;">
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="https://www.facebook.com/M-Biz-Trading-Private-Limited-2122159934768714/" class="fa fa-facebook" target="blank"></a></li>
							{{-- <li><a href="#" class="fa fa-google-plus"></a></li> --}}
							<li><a href="#" class="fa fa-linkedin"></a></li>
								{{-- <li><a href="#" class="fa fa-vimeo"></a></li>
								<li><a href="#" class="fa fa-pinterest-p"></a></li> --}}
							</ul>
						</div>

						<div class="col-sm-4">
							<ul class="payment-list">
								<li><a href="#"><img src="/assetsss/images/payment1.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment2.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment3.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment4.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment5.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment6.jpg" alt=""></a></li>
							</ul>
						</div><!-- col -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .bottom -->
		</footer><!--/.footer -->
	</div><!--/#wrapper -->

	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assetsss/script/html5shiv.min.js"></script>
	<script src="assetsss/script/respond.min.js"></script>
<![endif]-->
	<!-- 
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="/assetsss/scripts/jquery.min.js"></script>
		<!-- BEGIN Revolution Slider Scripts -->
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.themepunch.revolution.min.js"></script>
		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.video.min.js"></script>

		<!-- END Revolution Slider Scripts -->
		<script src="/assetsss/scripts/jquery.inview.min.js"></script>
		<script src="/assetsss/scripts/modernizr.min.js"></script>
		<script src="/assetsss/scripts/jquery.scrollTo.min.js"></script>
		<script src="/assetsss/plugin/select2/js/select2.min.js"></script>
		<script src="/assetsss/scripts/isotope.pkgd.min.js"></script>
		<script src="/assetsss/scripts/cells-by-row.min.js"></script>
		<script src="/assetsss/scripts/packery-mode.pkgd.min.js"></script>
		<script src="/assetsss/plugin/slick/slick.min.js"></script>
		<script src="/assetsss/scripts/jquery.parallax-1.1.3.min.js"></script>
		<script src="/assetsss/scripts/nouislider.min.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD84ST3FIRNNVS1CEm_IE9KoR-lAIw8OPo" type="text/javascript"></script>
		<script src="/assetsss/scripts/main.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.revolution.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css"></script>

		<script>			

			function showBar(){
				$('.menumobile-navbar').show();
			}

			function hideNavBar(){
				$('.menumobile-navbar').hide();
			}


			function showCart(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$.ajax({
					/* the route pointing to the post function */
					url: '/cart/show',
					type: 'POST',
					/* send the csrf-token and the input to the controller */					
					data: {_token: CSRF_TOKEN},
					success: function (data) {			
						$('#showminicart').html(data);
						$("#bill").text(data.bill);			
					}
				}); 
			}

			// var count_product=<?php echo session()->get('count'); ?>;
			function deleteProduct(id){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');			
				// var std = $("#show-total").html();
				// count_product--;			
				$('#listhide'+id).hide();
				$('#cart'+id).hide();
				$.ajax({
					/* the route pointing to the post function */
					url: '/cart/delete-product/id',
					type: 'POST',
					/* send the csrf-token and the input to the controller */				
					data: {_token: CSRF_TOKEN, id: id},
					success: function (data) { 
						$("#show-total").html(data.showcount);			
						$("#bill").html(data.bill);			
					}
				}); 
			}


			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
				if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
					document.getElementById("myBtn").style.display = "block";
				} else {
					document.getElementById("myBtn").style.display = "none";
				}
			}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}

function showDropDown(){
	$('#drop-down').show();
}

function hideDropDown(){
	$('#drop-down').hide();
}

function showCat(id){
	$('#more-cat'+id).toggle();
}

function showSubCat(id){
	$('#more-sub-cat'+id).toggle();
}

</script>

@yield('script')
</body>
</html>