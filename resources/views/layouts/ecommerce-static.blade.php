<?php
use App\MainCategory;
use App\Product;
use App\Review;
$bill = 0;
$i=0;
$sesseion_count = 0;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>M-Biz</title>
	<link rel="stylesheet" href="/assetsss/styles/style.min.css">
	<link rel="shortcut icon" href="{{ asset('assetsss/images/favicon.ico') }}">
	<style>
		.show-result { margin: 10px; padding: 10px; color: green; font-size: 20px; }
		#response-list{ text-align: right !important }

		.main-menu-wrap .menu>li>a { font-size: 13px !important; }			
		.header .middle .logo { top: 50px !important; }
		.header .middle .logo { position: absolute; left: 0; height: 85px !important; }
		.header .middle .logo img { top: 40%; max-height: 75px;	width: auto;}

		.main-menu-wrap .menu>li { display: inline-block; position: relative; margin-left: 20px; }
		.header .middle.middle-ver-2 { padding-left: 200px; margin-bottom: 10px; }


		@media screen and (max-width: 768px)
		{
			#response-icon{ text-align: center !important; }
			#response-list{ text-align: center !important }
			.header .middle.middle-ver-2 .logo { top: 10px !important; }
			.header .middle.middle-ver-2 .right-side { top: 90px !important; }
		}

		@media screen and (max-width: 399px)
		{
			.header .middle.middle-ver-2 .right-side { padding: 0 !important; }

			.header .middle.middle-ver-2 .right-side .user-wrap {
				position: relative !important;
				top: 0;
				left: 0px !important;
			}

		}


	</style>
</head>


<body>
	<div class="menumobile-navbar-wrapper">
		<nav class="menu-navbar menumobile-navbar js__menu_mobile">
			<div class="menumobile-close-btn js__menu_close">
				<span class="fa fa-times"></span> CLOSE
			</div>
			<div id="menu-mobile">
				<ul class="menu">
					<li class="current-menu-item"><a href="/index">Home</a></li>
					@foreach($maincategory as $row)
					<li class="menu-item-has-children"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a><span class="drop-down-icon js__menu_drop"></span>
						<ul class="sub-menu">
							<?php
							$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
							?>
							@foreach($category as $rows)
							<li class="menu-item-has-children">
								
								<a href="/products/{{$rows->id}}">{{$rows->Mcategory_name}}</a>
								<span class="drop-down-icon js__menu_drop"></span>
								<ul class="sub-menu">
									<?php
									$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
									?>
									@foreach($subcategory as $col)
									<li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a></li>
									@endforeach									
								</ul>
							</li>
							@endforeach							
						</ul>
					</li>
					@endforeach
					<li><a href="about">About us</a></li>
					{{-- <li><a href="#">Contact us</a></li> --}}
				</ul><!--/.menu -->
			</div><!--/#menu- -navbar -->
		</nav>
	</div><!--/.menu- -navbar-wrapper -->
	<div class="mobile-sticky js__menu_sticky">
		<div class="container">
		{{-- <div class="left-side">
			<a href="/index" class="logo"><img src="/assetsss/images/logo.png" alt=""></a>
		</div> --}}
		{{-- <div class="right-side">
			<button type="button" class="menumobile-toggle js__menu_toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div> --}}
	</div>
</div>
<div id="wrapper">
	<header class="header with-border">
		<div class="top">
			<div class="container">
				<div class="row" id="response-icon">
					<div class="col-sm-2 col-xs-12" >
						<ul class="list-inline" style="margin-top: 5px;margin-bottom: 0">
							<li class="list-inline-item"><a href="#" class="fa fa-twitter" ></a></li>
							<li class="list-inline-item"><a href="#" class="fa fa-facebook"></a></li>
							<li class="list-inline-item"><a href="#" class="fa fa-google-plus"></a></li>
							<li class="list-inline-item"><a href="#" class="fa fa-linkedin"></a></li>		<button type="button" class="menumobile-toggle js__menu_toggle" style="float: right; top: -10px; ">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>				
						</ul>					

					</div><!--/.left-side -->
					
					<div class="col-sm-10 col-xs-12" id="response-list">

						<ul class="menu">
							@if (isset($member->id))
							{{-- <li><a href="/en/member">Panel</a></li> --}}
							<li><a href="/en/member">  My Account</a></li>
							<li><a href="/wishlist">My Wishlist</a></li>
							@if(session()->get('cart') != null)
							<li><a href="/cart">Checkout</a></li>
							@endif
							<li><a href="/en/logout">Logout</a></li>
							@else
							<li><a href="/en/login">Login</a></li>
							@endif
							<li><strong><a style="color: #f60e23">TOLL FREE&nbsp;&nbsp;: &nbsp;&nbsp;</a><a style="font-size: 20px;color: #f60e23" href="tel:1800-419-8447">1800-419-8447</a></strong></li>
						</ul>

					</div><!--/.right-side -->
				</div>
			</div><!--/.container -->
		</div><!--/.top -->
		<div class="container">
			<div class="middle middle-ver-2">
				<a href="/index" class="logo"><img src="/assetsss/images/mbizlogo.png" alt="" style="width: 140px; height: 200px"></a>
				<div class="main-menu-wrap-2">
					<div class="main-menu-wrap js__auto_correct_sub_menu">
						<ul class="menu">
							<li class="current-menu-item"><a href="/index"><i class='fa fa-home' style='font-size:22px'></i></a></li>
							@foreach($maincategory as $row)
							<li class="menu-item-has-children mega-menu-wrap"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a>
								<div class="mega-menu">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<?php 
												$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
												?>
												@foreach($category as $rows)
												<div class="col-md-4">
													<h3 class="title"><a href="/products/{{$rows->id}}">{{$rows->Mcategory_name}}</a></h3>
													<ul class="sub-menu">
														<?php
														$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
														?>
														@foreach($subcategory as $col)
														<li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a></li>
														@endforeach
														
													</ul>
												</div><!--/col -->
												@endforeach
												
											</div><!--/.row -->
										</div><!--col -->
										<?php 
										$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
										?>
										@foreach($category as $rows)
										<?php
										$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
										?>
										@foreach($subcategory as $col)

										@endforeach
										@endforeach
										
									</div><!--/.row -->
								</div><!--/.menu-mega -->
							</li>
							@endforeach
							
							{{-- <li><a href="#">About us</a></li> --}}
							{{-- <li><a href="#">Contact us</a></li> --}}
						</ul><!--/.menu -->
					</div><!--/.main-menu-wrap -->
				</div><!--/.main-menu-wrap-2 -->
				<ul class="right-side">
					{{-- <li class="js__drop_down search-toggle">
						<a href="#" class="fa fa-search js__drop_down_button"></a>
						<div class="search-container">
							<form action="#">
								<input type="search" class="inp-search" placeholder="Enter keyword...">
								<button type="submit" class="btn-submit"><i class="fa fa-search"></i></button>
							</form>
						</div>
					</li> --}}
					<li class="user-wrap"><a href="/en/member" class="fa fa-user"></a></li>
					<li class="js__drop_down" >
						<a onclick="showCart()" class="fa fa-shopping-cart js__drop_down_button">
							
							<div id="total-check">
								<?php													
								$show=session()->get('count');
								?>									
							</div>
							@if(session()->get('count') != null)
							<span class="num" id="show-total">{{$show}}</span>
							@else
							<span class="num" id="show-total">0</span>
							@endif
						</a>
						<div class="cart-list-container">
							<div class="cart-list-content" id="showminicart">
								@if(session()->get('cart') != null)

								<?php

								$ids = array();
								$cat_id = array();					
								$quantities = array();

								foreach(session()->get('cart') as $data)
								{
									$ids[$i] = $data->product_id;								
									$quantities[$i] = $data->quantity;
									$i++;
								}						
								?>
								<ul class="cart-list" style="height: 350px; overflow: auto">
									@for($j=0 ; $j<$i ; $j++ )
									<?php
									$product = Product::where('id',$ids[$j])->first(); 
									if($product != null)
									{
										$cat_id[$j] = $product->category_id;
									}
									else
									{
										$cat_id[$j] = 0;
									}
									?>
									<li id="listhide{{$product->id}}">
										<a class="thumb" href="#"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" class="attachment-shop_thumbnail" style="width: 70px; height: 70px"></a>
										<a href="#" class="title">{{$product->name}}</a>
										<span class="quantity">
											<span class="amount">Rs. {{$product->sell_price}}</span> x {{$quantities[$j]}}
										</span>
										<div class="star-rating">
											<span class="js__width" data-width="100%"><strong class="rating">4.00</strong> out of 5</span>
										</div>
										<a title="Remove this item" class="mini-cart-remove" onclick="deleteProduct({{$ids[$j]}})"><span class="fa fa-times"></span></a>
										<?php
										$total = $quantities[$j]*$product->sell_price;
										$bill =$bill +  $total;
										?>
									</li>
									@endfor								
								</ul><!--/.cart-list -->
								@endif
								<div class="cart-list-subtotal">
									<strong class="txt fl">SubTotal:</strong>
									<strong class="currency fr">Rs. {{$bill}}</strong>
								</div><!--/.cart-list-subtotal -->
							</div><!--/.cart-list-content -->
							<a class="cart-list-bottom" href="/checkout">
								<span class="fl">Checkout</span>
								<span class="fr"><i class="fa fa-long-arrow-right"></i></span>
							</a><!--/.cart-list-bottom -->
						</div><!--/.cart-list-container -->
					</li>
					{{-- <li class="menumobile-toggle-wrap">
						<button type="button" class="menumobile-toggle js__menu_toggle">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</li> --}}
				</ul><!--/.right-side -->
			</div><!--/.middle -->
		</div><!--/.container -->
	</header><!--/.header -->

	@yield('content')


	<style>
		.widget_nav_menu .title , .widget_text .title{ color: #fff !important; }

		@media screen and (max-width: 768px){
			.footer{ text-align: center !important; }
			.footer .widget .menu li a{ text-align: center !important; } 
			.widget .title { font-size: 20px }
		}
		.widget .menu li a { color: #fff; font-size: 13px }
		.widget .menu li { padding-bottom: 8px; }

	</style>

	<footer class="footer">
		<div class="container">
			<div class="top">
				<div class="row">
					<div class="col-md-3 col-sm-4">
						<div class="widget ">
							<h3 class="title">Help</h3>
							<ul class="menu">
								{{-- <li><a href="#">Track order</a></li> --}}
								<li><a href="faq">FAQs</a></li>
								<li><a href="privacypolicy">Privacy policy</a></li>
								<li><a href="tnc">Terms & Conditions</a></li>
								<li><a href="returnpolicy">Refunds</a></li>
								<li><a href="#">Support Online</a></li>
							</ul>
						</div>
					</div><!-- col -->
					<div class="col-md-3 col-sm-4">
						<div class="widget ">
							<h3 class="title">Account</h3>
							<ul class="menu">
								<li><a href="/en/member">My account</a></li>
								<li><a href="/wishlist">Wishlist</a></li>
								{{-- <li><a href="">Order history</a></li> --}}
								{{-- <li><a href="#">My Favorites</a></li> --}}
								{{-- <li><a href="#">Gift Vouchers</a></li> --}}
								{{-- <li><a href="#">Specials</a></li> --}}
							</ul>
						</div>
					</div><!-- col -->
					<div class="col-md-3 col-sm-4">
						<div class="widget ">
							<h3 class="title">Quick Links</h3>
							<ul class="menu">
								{{-- <li><a href="#">Best Sellers</a></li> --}}
								{{-- <li><a href="#">Featured Products</a></li> --}}
								{{-- <li><a href="#">Hot Products</a></li> --}}
								{{-- <li><a href="#">Top Rated</a></li> --}}
								{{-- <li><a href="#">Blog</a></li> --}}
								<li><a href="about">About Us</a></li>
								<li><a href="contact-us">Contact Us</a></li>
							</ul>
						</div>
					</div><!-- col -->
					<div class="col-md-3 col-sm-6">
						<div class="widget widget_text">
							<h3 class="title">Contact Us</h3>

							<p>Please feel free to contact us if you have any question.</p>
							<ul class="contact-list">
								<li><a href="mailto:info@m-biz.in"><i class="fa fa-envelope-o"></i>info@m-biz.in</a></li>
								<li><a href="tel:1800 419 8447"><i class="fa fa-phone"></i>1800 419 8447</a></li>
								<li><i class="fa fa-map-marker"></i>M-Biz Trading Private Limited, A-51, 2nd Floor, New Dwarka Road, Dabri Exnt, New Delhi-110045</li>
							</ul>
						</div>
					</div><!-- col -->
					{{-- <div class="col-md-3 col-sm-6">
						<div class="widget widget_subscribe">
							<h3 class="title">Newsletter Signup</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
							<form action="#" class="join-form">
								<div class="form-controls">
									<input type="email" placeholder="Enter your email" class="inp-email">
									<input type="submit" value="Join" class="inp-submit">
									<i>We respect your privac</i>
								</div>
							</form>
							<ul class="social-list" style="text-align: center;">
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-facebook"></a></li>
								<li><a href="#" class="fa fa-google-plus"></a></li>
								<li><a href="#" class="fa fa-linkedin"></a></li>
								{{-- <li><a href="#" class="fa fa-vimeo"></a></li>
								<li><a href="#" class="fa fa-pinterest-p"></a></li>
							</ul>
						</div>
					</div><!-- col --> --}}
				</div><!-- .row -->
			</div><!-- .top -->
		</div><!-- .container -->
		<div class="bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="copyright">Copyright &copy; 2019 <a href="#">M-Biz.</a> All rights reserved.</a></div>
					</div><!-- col -->

					<div class="col-sm-4">
						<ul class="social-list" style="text-align: center;">
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-facebook"></a></li>
							<li><a href="#" class="fa fa-google-plus"></a></li>
							<li><a href="#" class="fa fa-linkedin"></a></li>
								{{-- <li><a href="#" class="fa fa-vimeo"></a></li>
								<li><a href="#" class="fa fa-pinterest-p"></a></li> --}}
							</ul>
						</div>

						<div class="col-sm-4">
							<ul class="payment-list">
								<li><a href="#"><img src="/assetsss/images/payment1.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment2.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment3.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment4.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment5.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment6.jpg" alt=""></a></li>
							</ul>
						</div><!-- col -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .bottom -->
		</footer><!--/.footer -->
	</div><!--/#wrapper -->
	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assetsss/script/html5shiv.min.js"></script>
	<script src="assetsss/script/respond.min.js"></script>
<![endif]-->
	<!-- 
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="/assetsss/scripts/jquery.min.js"></script>
		<!-- BEGIN Revolution Slider Scripts -->
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.themepunch.revolution.min.js"></script>
		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.video.min.js"></script>

		<!-- END Revolution Slider Scripts -->
		<script src="/assetsss/scripts/jquery.inview.min.js"></script>
		<script src="/assetsss/scripts/modernizr.min.js"></script>
		<script src="/assetsss/scripts/jquery.scrollTo.min.js"></script>
		<script src="/assetsss/plugin/select2/js/select2.min.js"></script>
		<script src="/assetsss/scripts/isotope.pkgd.min.js"></script>
		<script src="/assetsss/scripts/cells-by-row.min.js"></script>
		<script src="/assetsss/scripts/packery-mode.pkgd.min.js"></script>
		<script src="/assetsss/plugin/slick/slick.min.js"></script>
		<script src="/assetsss/scripts/jquery.parallax-1.1.3.min.js"></script>
		<script src="/assetsss/scripts/nouislider.min.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD84ST3FIRNNVS1CEm_IE9KoR-lAIw8OPo" type="text/javascript"></script>
		<script src="/assetsss/scripts/main.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.revolution.min.js"></script>

		@yield('script')
		{{-- Modal pop up Script --}}
		<script>
			var count_product=<?php echo session()->get('count'); ?>;
			function deleteProduct(id){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');	
				
				count_product--;			
				$('#listhide'+id).hide();
				
				$.ajax({
					/* the route pointing to the post function */
					url: '/cart/delete-product/id',
					type: 'POST',
					/* send the csrf-token and the input to the controller */
					dataType: 'JSON',
					data: {_token: CSRF_TOKEN, id: id},
					success: function (data) { 
						$("#show-total").html(count_product);

					// alert('data deleted successfully');				
				}
			}); 
			}

			
		</script>

	</body>
	</html>