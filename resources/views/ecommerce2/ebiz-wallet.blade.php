<?php
use App\Models\Epoint;
use App\MainCategory;
$wallet = Epoint::where('member_id',$member->id)->orderBy('created_at','DESC')->first();
//echo $user_info;
//die;

$txnid = 'TXN'.rand(10000,99999999);
$email = $user_info->email;
$price = $checkorder->price;
$name = $user_info->username;
$phone = $user_info->phone;
$product_info = "Product_Info";
$udf5 = "BOLT_KIT_PHP7";
$hash=hash('sha512', $txnid.'|'.$price.'|'.$product_info.'|'.$name.'|'.$email.'|||||'.$udf5.'||||||');

//echo $hash;


//die;
?>

<html>
<head>
	<title> M-Biz Trading Private Limited</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		#bg-image{
    background:url(../assets/img/login.png) no-repeat center center fixed !important; height: 100%;
    filter: brightness(10%); 
  }
		.wallet-body{ border: 10px solid #fff; background-color: #fff; border-radius: 20px ; position : absolute; top: 100px;left: 25%; }
		.wallet-body .card h2 { text-align: center; color: #999; font-size: 28px;  }
		.card { width: 100%; margin-top: -30px}	
		.set-response{ margin-top: 3% }
		.set-response h3 { color: #1f2f3f; font-weight: bold; font-size: 16px ; margin-left: 20%}
	</style>
</head>
<body>
	<div id="bg-image"></div>
	<br><br>
	<div class="container">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 wallet-body">
				<form action="/pay/{{$member->id}}" method="POST" id="payment_form">
					{{-- <form action="/pay/{{$member->id}}"  id="payment_form"> --}}
						<input type="hidden" id="txnid" name="txnid" placeholder="Transaction ID" value="{{$txnid}}" />
						<input type="hidden" id="amount" name="amount" placeholder="Amount" value="{{$price}}" />    
						<input type="hidden" id="pinfo" name="pinfo" placeholder="Product Info" value="{{$product_info}}" />
						<input type="hidden" id="fname" name="fname" placeholder="First Name" value="{{$name}}" />
						<input type="hidden" id="email" name="email" placeholder="Email ID" value="{{$email}}" />
						<input type="hidden" id="mobile" name="mobile" placeholder="Mobile/Cell Number" value="{{$phone}}" />
						<input type="hidden" id="hash" name="hash" placeholder="Hash" value="{{$hash}}" />

						<div class="card">
							<div class="row set-response" style="border-bottom: 1px solid red">
								<h2><img src="/assetsss/images/mbizlogo.png" alt="" style="width: auto; height: auto"></h2>
							</div>
							<div class="row set-response">
								<div class="col-sm-6"><h3>Total Biz Coins </h3></div>
								<div class="col-sm-6"><h3>: {{$wallet->epoint}}</h3></div>
							</div>
							<div class="row set-response">
								<div class="col-sm-6"><h3>To Pay</h3></div>
								<div class="col-sm-6"><h3>: {{$price}}.00</h3></div>
							</div>

							<div class="row set-response">
								<div class="col-sm-6"><h3>Remaining Coins </h3></div>
								<div class="col-sm-6"><h3>: 
									<?php $balance = $wallet->epoint-$price;?>
									{{$balance}}</h3>
								</div>
							</div>
							<div class="row set-response"></div>
							<div class="row set-response" style="text-align: center;">
								<button type="submit" class="btn btn-primary">Click To Pay</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>

	</body>
	</html>