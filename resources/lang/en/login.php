<?php

return [
'title'		=>	'Login',
'savetitle'	=>	'Register',
'subtitle' => 'Welcome | MEMBER AREA',
'username' => 'Username',
'password' => 'Password',
'forgotPassword' => 'Forgot Password',
'remember' => 'Remember Me'
];