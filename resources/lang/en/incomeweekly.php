<?php

return [
'title'	=>	'Weekly Income',
'subtitle' => 'Your Weekly members registration history',
'subtitleback' => 'Weekly members registration history',
'username' => 'Username',
'password' => 'Password',
'remember' => 'Remember Me'
];