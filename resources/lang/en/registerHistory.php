<?php

return [
	'title' => 'Registration History',
	'subTitle' => 'Your direct members registration history.',
	'join' => 'Join Date',
	'username' => 'Username',
	'member_id' => 'Member ID',
	'package' => 'Package',
	'action' => 'Action'
];
