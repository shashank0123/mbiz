<?php

return [
'title'	=>	'Direct Income',
'title1'	=>	'Bonus Income',
'subtitle' => 'Your direct members registration history',
'subtitleback' => 'Direct members registration history',
'subtitle1' => 'Your Bonus Income history',
'username' => 'Username',
'password' => 'Password',
'remember' => 'Remember Me'
];